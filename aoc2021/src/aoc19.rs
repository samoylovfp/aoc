use std::{
    collections::{HashMap, HashSet},
    fs::read_to_string,
};

use itertools::{iproduct, Itertools};

#[derive(Debug)]
struct Orientation {
    offset: [i32; 3],
    order: [usize; 3],
    signs: [i32; 3],
}

fn main() {
    let input = read_to_string("input19.txt").unwrap();

    let scanners: Vec<Scanner> = parse_scanners(input);
    dbg!(part_one_and_two(&scanners));
}

fn parse_scanners(input: String) -> Vec<Scanner> {
    input.split("\n\n").map(Scanner::parse).collect()
}

type NeighbourSquareDistances = HashMap<Point, HashSet<u32>>;

fn part_one_and_two(scanners: &[Scanner]) -> (usize, u32) {
    let mut scanners_distances: HashMap<usize, NeighbourSquareDistances> = HashMap::new();
    for scanner in scanners {
        let mut points_to_distances = HashMap::new();
        for point in &scanner.points {
            let mut distances = HashSet::new();
            for point2 in &scanner.points {
                if point2 != point {
                    distances.insert(point2.sq_distance(point));
                }
            }
            points_to_distances.insert(point.clone(), distances);
        }
        scanners_distances.insert(scanner.n, points_to_distances);
    }

    let mut global_points = NeighbourSquareDistances::new();

    let base_scanner = scanners[0].n;
    let mut processed_scanners = HashSet::from([base_scanner]);
    global_points.extend(scanners_distances[&base_scanner].clone());
    let mut all_offsets: Vec<[i32; 3]> = vec![];
    while processed_scanners.len() < scanners_distances.len() {
        for (new_scanner_n, new_scanner_points) in scanners_distances.iter() {
            let mut common_points = vec![];
            for (new_scanner_point, new_scanner_point_distances) in new_scanner_points {
                for (global_point, global_point_distances) in &global_points {
                    if global_point_distances
                        .intersection(new_scanner_point_distances)
                        .count()
                        >= 11
                    {
                        common_points.push((global_point, new_scanner_point));
                    }
                }
            }
            if common_points.len() < 5 {
                continue;
            }
            if let Some(orientation) = calculate_orientation(&common_points) {
                all_offsets.push(orientation.offset);
                for (p, distances) in new_scanner_points {
                    let p = orientation.reverse(p);
                    if global_points.contains_key(&p) {
                        continue;
                    }
                    let mut new_distances = distances.clone();
                    for (gp, gp_distances) in global_points.iter_mut() {
                        let new_distance = p.sq_distance(gp);
                        new_distances.insert(new_distance);
                        gp_distances.insert(new_distance);
                    }
                    global_points.insert(p, new_distances);
                }
                processed_scanners.insert(*new_scanner_n);
            }
        }
    }
    let max_distance = all_offsets
        .into_iter()
        .combinations(2)
        .map(|pair| {
            pair[0]
                .into_iter()
                .zip(pair[1])
                .map(|(p1, p2)| (p1 - p2).abs() as u32)
                .sum()
        })
        .max()
        .unwrap();

    (global_points.len(), max_distance)
}

fn calculate_orientation(common_points: &[(&Point, &Point)]) -> Option<Orientation> {
    let (p1_global, p1_new) = common_points[0];
    let (p2_global, p2_new) = common_points[1];

    let signs = [1, -1];
    for (orders, x_sign, y_sign, z_sign) in
        iproduct!([0, 1, 2].into_iter().permutations(3), signs, signs, signs)
    {
        let mut potential_orientation = Orientation {
            offset: [0, 0, 0],
            order: orders.try_into().unwrap(),
            signs: [x_sign, y_sign, z_sign],
        };

        let p1_transformed = potential_orientation.reverse(p1_new);
        let offset = p1_global
            .0
            .into_iter()
            .zip(p1_transformed.0)
            .map(|(g, t)| g - t)
            .collect::<Vec<i32>>();

        potential_orientation.offset = offset.try_into().unwrap();

        assert_eq!(&potential_orientation.reverse(p1_new), p1_global);

        if &potential_orientation.reverse(p2_new) == p2_global {
            return Some(potential_orientation);
        }
    }

    None
}

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
struct Point([i32; 3]);

#[derive(Debug)]
struct Scanner {
    n: usize,
    points: Vec<Point>,
}

impl Scanner {
    fn parse(input: &str) -> Scanner {
        let mut lines = input.trim().lines();
        let mut words = lines.next().unwrap().split_whitespace();
        assert_eq!(words.next(), Some("---"));
        assert_eq!(words.next(), Some("scanner"));
        let scanner_n = words.next().unwrap().parse().unwrap();
        Scanner {
            n: scanner_n,
            points: lines.map(Point::parse).collect(),
        }
    }
}
impl Point {
    pub(crate) fn parse(line: &str) -> Point {
        let mut coordinates = line.split(',');
        let mut next_number = || coordinates.next().unwrap().parse().unwrap();
        let x = next_number();
        let y = next_number();
        let z = next_number();
        Point([x, y, z])
    }

    pub(crate) fn sq_distance(&self, p: &Point) -> u32 {
        self.0
            .into_iter()
            .zip(p.0)
            .map(|(p1, p2)| (p1 - p2).pow(2) as u32)
            .sum()
    }
}

impl Orientation {
    pub(crate) fn reverse(&self, point: &Point) -> Point {
        let new_coords = point.0;
        Point([
            new_coords[self.order[0]] * self.signs[0] + self.offset[0],
            new_coords[self.order[1]] * self.signs[1] + self.offset[1],
            new_coords[self.order[2]] * self.signs[2] + self.offset[2],
        ])
    }
}

#[test]
fn test_provided() {
    let (p1, p2) = part_one_and_two(&parse_scanners(read_to_string("sample19.txt").unwrap()));
    assert_eq!(p1, 79);
    assert_eq!(p2, 3621);
}

#[test]
fn test_find_orientation() {
    let orientation = calculate_orientation(&[
        (&Point([-618, -824, -621]), &Point([686, 422, 578])),
        (&Point([-537, -823, -458]), &Point([605, 423, 415])),
        (&Point([-447, -329, 318]), &Point([515, 917, -361])),
    ]);

    assert_eq!(
        orientation.unwrap().reverse(&Point([-336, 658, 858])),
        Point([404, -588, -901])
    )
}
