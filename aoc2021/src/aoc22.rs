use std::fs::read_to_string;
use std::ops::RangeInclusive;

use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::one_of,
    combinator::{opt, recognize},
    multi::{many1, many_m_n},
    sequence::{pair, separated_pair, terminated},
};

#[derive(Debug)]
struct CubeInstruction {
    mode: bool,
    ranges: [RangeInclusive<i32>; 3],
}

fn main() {
    let input = read_to_string("input22.txt").unwrap();
    let cube_instructions: Vec<CubeInstruction> = input
        .trim()
        .lines()
        .map(|l| parse_cube(l).unwrap().1)
        .collect();
    println!("{:?}", cube_instructions);
    // let mut volume_state = vec![];
}

fn parse_cube(i: &str) -> nom::IResult<&str, CubeInstruction> {
    let (i, mode) = alt((tag("on"), tag("off")))(i)?;
    let (i, _) = tag(" ")(i)?;
    let dim_name = alt((tag("x"), tag("y"), tag("z")));
    let range = separated_pair(parse_number, tag(".."), parse_number);

    let dimension = separated_pair(dim_name, tag("="), range);
    let (i, dimensions) = many_m_n(3, 3, terminated(dimension, opt(tag(","))))(i)?;
    println!("{:?}", dimensions);

    let ci = CubeInstruction {
        mode: match mode {
            "on" => true,
            "off" => false,
            _ => panic!(),
        },
        ranges: dimensions
            .into_iter()
            .map(|(_name, dim)| dim.0..=dim.1)
            .collect_vec()
            .try_into()
            .unwrap(),
    };
    Ok((i, ci))
}

fn parse_number(input: &str) -> nom::IResult<&str, i32> {
    let (i, num) = recognize(pair(opt(tag("-")), many1(one_of("0123456789"))))(input)?;
    let num = num.parse().unwrap();

    Ok((i, num))
}
