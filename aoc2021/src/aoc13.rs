use std::{collections::HashSet, fs::read_to_string, mem::swap};

fn main() {
    let input = read_to_string("input13.txt").unwrap();
    let (mut paper, actions) = parse_paper_and_actions(&input);
    paper.apply(&actions[0]);
    println!("First part is {}", paper.count_visible());

    actions[1..].iter().for_each(|a| paper.apply(a));
    let max_x = paper.0.iter().map(|p|p.0).max().unwrap();
    let max_y = paper.0.iter().map(|p|p.1).max().unwrap();
    for y in 0..=max_y {
        for x in 0..=max_x {
            if paper.0.contains(&(x, y)) {
                print!("#")
            } else {
                print!(".")
            }
        }
        println!();
    }
}

struct Paper(HashSet<(i32, i32)>);

impl Paper {
    fn parse(input: &str) -> Paper {
        let map = input
            .trim()
            .lines()
            .map(|l| {
                let (x, y) = l.trim().split_once(",").unwrap();
                (x.parse().unwrap(), y.parse().unwrap())
            })
            .collect();
        Paper(map)
    }

    fn apply(&mut self, action: &FoldAction) {
        let mut folded_paper = self.0.clone();
        self.0.iter().for_each(|&(x, y)| {
            let folded = match action.axis {
                Axis::X => (x > action.coord).then(|| (2 * action.coord - x, y)),
                Axis::Y => (y > action.coord).then(|| (x, 2 * action.coord - y)),
            };
            if let Some(new_point) = folded {
                folded_paper.remove(&(x, y));
                folded_paper.insert(new_point);
            }
        });
        swap(&mut self.0, &mut folded_paper);
    }

    fn count_visible(&self) -> usize {
        self.0.len()
    }
}

enum Axis {
    X,
    Y,
}

struct FoldAction {
    axis: Axis,
    coord: i32,
}

impl FoldAction {
    fn parse(input: &str) -> FoldAction {
        let axis_coord = input.split_once("fold along ").unwrap().1;
        let (axis, coord) = axis_coord.split_once("=").unwrap();
        FoldAction {
            axis: match axis {
                "x" => Axis::X,
                "y" => Axis::Y,
                _ => panic!(),
            },
            coord: coord.parse().unwrap(),
        }
    }
}

fn parse_paper_and_actions(input: &str) -> (Paper, Vec<FoldAction>) {
    let (paper_str, actions) = input.split_once("\n\n").unwrap();

    (
        Paper::parse(paper_str),
        actions.trim().lines().map(FoldAction::parse).collect(),
    )
}

#[test]
fn test_provided() {
    let (mut paper, actions) = parse_paper_and_actions(
        r#"
            6,10
            0,14
            9,10
            0,3
            10,4
            4,11
            6,0
            6,12
            4,1
            0,13
            10,12
            3,4
            3,0
            8,4
            1,10
            2,14
            8,10
            9,0

            fold along y=7
            fold along x=5

    "#,
    );

    assert_eq!(paper.count_visible(), 18);
    paper.apply(&actions[0]);

    assert_eq!(paper.count_visible(), 17);
}
