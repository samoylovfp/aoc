use std::{error::Error, fs::read_to_string};

fn main() -> Result<(), Box<dyn Error>> {
    let data = read_to_string("../day4_full.txt")?;
    // y, x
    let grid: Vec<Vec<char>> = data.lines().map(|l| l.chars().collect()).collect();

    // part 1
    let mut part1_res = 0;
    for x in 0..grid[0].len() {
        for y in 0..grid.len() {
            for dx in -1..=1 {
                for dy in -1..=1 {
                    if is_xmas(&grid, [x, y], [dx, dy], "XMAS") {
                        part1_res += 1;
                    }
                }
            }
        }
    }
    dbg!(part1_res);

    //part 2
    let mut part2_res = 0;
    for x in 0..(grid[0].len() - 2) {
        for y in 0..(grid.len() - 2) {
            let sam_or_mas = |pos, offset| {
                is_xmas(&grid, pos, offset, "MAS") || is_xmas(&grid, pos, offset, "SAM")
            };
            if sam_or_mas([x, y], [1, 1]) && sam_or_mas([x + 2, y], [-1, 1]) {
                part2_res += 1;
            }
        }
    }
    dbg!(part2_res);
    Ok(())
}

fn is_xmas(grid: &[Vec<char>], pos: [usize; 2], dir: [isize; 2], pattern: &str) -> bool {
    for (i, target_char) in pattern.chars().enumerate() {
        let i = i as isize;
        let Some(x) = pos[0].checked_add_signed(dir[0] * i) else {
            return false;
        };
        let Some(y) = pos[1].checked_add_signed(dir[1] * i) else {
            return false;
        };

        let Some(char) = grid.get(y).and_then(|l| l.get(x)) else {
            return false;
        };
        // println!("expert {target_char:?} got {char:?} at {pos:?}");

        if *char != target_char {
            return false;
        }
    }
    true
}
