use std::collections::{BTreeMap, BTreeSet};

fn main() -> anyhow::Result<()> {
    let input = std::fs::read_to_string("../day5.txt")?;
    dbg!(part1(&input));
    dbg!(part2(&input));
    Ok(())
}

#[derive(Default)]
struct Rules {
    after: BTreeMap<u64, BTreeSet<u64>>,
    pre: BTreeMap<u64, BTreeSet<u64>>,
}

fn parse_rules(input: &[&str]) -> Rules {
    let rules_list = input.iter().map(|r| {
        let (prev, next) = r
            .split_once("|")
            .unwrap_or_else(|| panic!("Rule {r:?} needs to have a '|'"));
        let prev = prev.trim().parse().unwrap();
        let next = next.trim().parse().unwrap();
        (prev, next)
    });
    let mut rules = Rules::default();
    for (prev, next) in rules_list {
        rules.after.entry(prev).or_default().insert(next);
        rules.pre.entry(next).or_default().insert(prev);
    }
    rules
}

fn part1(input: &str) -> u64 {
    let lines: Vec<&str> = input.trim().lines().collect();
    dbg!(&lines);
    let mut parts = lines.split(|s| s.trim().is_empty());
    let rules = parse_rules(parts.next().unwrap());
    let updates: Vec<Vec<u64>> = parts
        .next()
        .unwrap()
        .iter()
        .map(|u| u.split(',').map(|un| un.trim().parse().unwrap()).collect())
        .collect();

    let correct_updates = updates.iter().filter(|u| is_update_correct(u, &rules));

    correct_updates.map(|u| u[u.len() / 2]).sum()
}

fn part2(input: &str) -> u64 {
    let lines: Vec<&str> = input.trim().lines().collect();
    let mut parts = lines.split(|s| s.is_empty());
    let rules = parse_rules(parts.next().unwrap());
    let mut updates: Vec<Vec<u64>> = parts
        .next()
        .unwrap()
        .iter()
        .map(|u| u.split(',').map(|un| un.trim().parse().unwrap()).collect())
        .collect();
    let incorrect_updates = updates.iter_mut().filter(|u| !is_update_correct(u, &rules));

    incorrect_updates
        .map(|iu| {
            while !do_correct_update(iu, &rules) {}
            iu
        })
        .inspect(|u| assert!(is_update_correct(u, &rules)))
        .map(|u| u[u.len() / 2])
        .sum()
}

fn do_correct_update(u: &mut [u64], all_rules: &Rules) -> bool {
    for (i, un) in u.iter().enumerate() {
        if let Some(rules_after) = all_rules.after.get(un) {
            for (i2, un2) in u[..i].iter().enumerate() {
                if rules_after.contains(un2) {
                    dbg!("swapping", i, i2);
                    u.swap(i, i2);
                    return false;
                }
            }
        }
        if let Some(rules_before) = all_rules.pre.get(un) {
            for (i2, un2) in u[(i + 1)..].iter().enumerate() {
                let i2 = i2 + i + 1;
                if rules_before.contains(un2) {
                    u.swap(i, i2);
                    dbg!("swapping", i, i2);
                    return false;
                }
            }
        }
    }
    true
}

fn is_update_correct(u: &[u64], all_rules: &Rules) -> bool {
    for (i, un) in u.iter().enumerate() {
        if let Some(rules_after) = all_rules.after.get(un) {
            for un2 in &u[..i] {
                if rules_after.contains(un2) {
                    return false;
                }
            }
        }
        if let Some(rules_before) = all_rules.pre.get(un) {
            for un2 in &u[(i + 1)..] {
                if rules_before.contains(un2) {
                    return false;
                }
            }
        }
    }
    true
}
#[test]
fn provided_part1() {
    let rules = parse_rules(
        &r#"
        47|53
        97|13
        97|61
        97|47
        75|29
        61|13
        75|53
        29|13
        97|29
        53|29
        61|53
        97|53
        61|29
        47|13
        75|47
        97|75
        47|61
        75|61
        47|29
        75|13
        53|13"#
            .trim()
            .lines()
            .collect::<Vec<_>>(),
    );

    assert!(is_update_correct(&[75, 47, 61, 53, 29], &rules));
    assert!(is_update_correct(&[97, 61, 53, 29, 13], &rules));
    assert!(is_update_correct(&[75, 29, 13], &rules));
    // incorrect:
    assert!(!is_update_correct(&[75, 97, 47, 61, 53], &rules));
    assert!(!is_update_correct(&[61, 13, 29], &rules));
    assert!(!is_update_correct(&[97, 13, 75, 29, 47], &rules));

    let input = r#"
    47|53
    97|13
    97|61
    97|47
    75|29
    61|13
    75|53
    29|13
    97|29
    53|29
    61|53
    97|53
    61|29
    47|13
    75|47
    97|75
    47|61
    75|61
    47|29
    75|13
    53|13

    75,47,61,53,29
    97,61,53,29,13
    75,29,13
    75,97,47,61,53
    61,13,29
    97,13,75,29,47
            "#;
    assert_eq!(part1(input), 143);
}

#[test]
fn provided_part2() {
    let input = r#"
        47|53
        97|13
        97|61
        97|47
        75|29
        61|13
        75|53
        29|13
        97|29
        53|29
        61|53
        97|53
        61|29
        47|13
        75|47
        97|75
        47|61
        75|61
        47|29
        75|13
        53|13

        75,47,61,53,29
        97,61,53,29,13
        75,29,13
        75,97,47,61,53
        61,13,29
        97,13,75,29,47
                "#;
    assert_eq!(part2(input), 123);
}
