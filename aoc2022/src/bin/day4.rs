use std::fs::read_to_string;

fn main() {
    let input = read_to_string("input4.txt").unwrap();
    let elfs = input.trim().lines().map(|line| {
        let (elf1, elf2) = line.split_once(',').unwrap();
        (parse_interval(elf1), parse_interval(elf2))
    });
    let part1 = elfs
        .clone()
        .filter(|(e1, e2)| (e1.0 <= e2.0 && e1.1 >= e2.1) || (e1.0 >= e2.0 && e1.1 <= e2.1))
        .count();
    dbg!(part1);

    let part2 = elfs
        .filter(|(e1, e2)| {
            let overlaps_fully = (e1.0 <= e2.0 && e1.1 >= e2.1) || (e1.0 >= e2.0 && e1.1 <= e2.1);
            let overlaps_a_bit = (e1.0 <= e2.1 && e1.0 >= e2.0) || (e1.1 >= e2.0 && e1.1 <= e2.1);
            overlaps_a_bit || overlaps_fully
        })
        .count();
    dbg!(part2);
}

fn parse_interval(s: &str) -> (u32, u32) {
    let (left, right) = s.split_once('-').unwrap();
    (left.parse().unwrap(), right.parse().unwrap())
}
