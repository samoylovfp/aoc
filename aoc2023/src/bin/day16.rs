use std::{
    collections::{HashMap, HashSet},
    fs::read_to_string,
};

fn main() {
    // let input = read_to_string("sample16.txt").unwrap();
    let input = read_to_string("input16.txt").unwrap();
    let cells = input
        .lines()
        .enumerate()
        .flat_map(|(y, line)| {
            line.chars()
                .enumerate()
                .map(move |(x, c)| ((x as _, y as _), c))
        })
        .collect();

    let table = Table {
        width: input.lines().next().unwrap().len(),
        height: input.lines().count(),
        cells,
    };
    dbg!(part1(&table));
    dbg!(part2(&table));
}

fn part1(table: &Table) -> usize {
    count_energized(
        LightRay {
            pos: (0, 0),
            dir: LightDirection::Right,
        },
        table,
    )
}

fn part2(table: &Table) -> usize {
    (0..table.width)
        .flat_map(|x| {
            [
                LightRay {
                    pos: (x as isize, 0),
                    dir: LightDirection::Down,
                },
                LightRay {
                    pos: (x as isize, table.height as isize - 1),
                    dir: LightDirection::Up,
                },
            ]
        })
        .chain((0..table.height).flat_map(|y| {
            [
                LightRay {
                    pos: (0, y as isize),
                    dir: LightDirection::Right,
                },
                LightRay {
                    pos: (table.width as isize - 1, y as isize),
                    dir: LightDirection::Left,
                },
            ]
        }))
        .map(|r| count_energized(r, table))
        .max()
        .unwrap()
}

#[derive(Hash, PartialEq, Eq, Clone)]
enum LightDirection {
    Right,
    Up,
    Left,
    Down,
}

#[derive(Hash, PartialEq, Eq, Clone)]
struct LightRay {
    pos: (isize, isize),
    dir: LightDirection,
}

struct Table {
    width: usize,
    height: usize,
    cells: HashMap<(isize, isize), char>,
}

fn count_energized(start_ray: LightRay, table: &Table) -> usize {
    use LightDirection::*;
    let mut visited_light_positions = HashSet::new();
    let mut energized_cells = HashSet::new();
    let mut light_rays = vec![start_ray];
    while !light_rays.is_empty() {
        let mut spawned_rays = vec![];
        for ray in &mut light_rays {
            visited_light_positions.insert(ray.clone());
            energized_cells.insert(ray.pos);
            // mirror
            if let Some('/') = table.cells.get(&ray.pos) {
                match ray.dir {
                    Right => ray.dir = Up,
                    Up => ray.dir = Right,
                    Left => ray.dir = Down,
                    Down => ray.dir = Left,
                }
            }
            if let Some('\\') = table.cells.get(&ray.pos) {
                match ray.dir {
                    Right => ray.dir = Down,
                    Up => ray.dir = Left,
                    Left => ray.dir = Up,
                    Down => ray.dir = Right,
                }
            }
            // split
            if let Some('|') = table.cells.get(&ray.pos) {
                if let Left | Right = ray.dir {
                    ray.dir = Up;
                    spawned_rays.push(LightRay {
                        pos: ray.pos,
                        dir: Down,
                    });
                }
            }
            if let Some('-') = table.cells.get(&ray.pos) {
                if let Up | Down = ray.dir {
                    ray.dir = Left;
                    spawned_rays.push(LightRay {
                        pos: ray.pos,
                        dir: Right,
                    });
                }
            }
            // propagate
            match ray.dir {
                Right => ray.pos.0 += 1,
                Up => ray.pos.1 -= 1,
                Left => ray.pos.0 -= 1,
                Down => ray.pos.1 += 1,
            }
        }
        light_rays.extend(spawned_rays);
        light_rays.retain(|r| {
            let x = r.pos.0 as usize;
            let y = r.pos.1 as usize;

            (r.pos.0 >= 0)
                && (x < table.width)
                && r.pos.1 >= 0
                && y < table.height
                && !visited_light_positions.contains(r)
        });
    }
    // for y in 0..table.height {
    //     for x in 0..table.width {
    //         if visited_light_positions.contains(&LightRay {
    //             pos: (x as isize, y as isize),
    //             dir: Up,
    //         }) {
    //             print!("^")
    //         } else if visited_light_positions.contains(&LightRay {
    //             pos: (x as isize, y as isize),
    //             dir: Right,
    //         }) {
    //             print!(">")
    //         } else if visited_light_positions.contains(&LightRay {
    //             pos: (x as isize, y as isize),
    //             dir: Left,
    //         }) {
    //             print!("<")
    //         } else if visited_light_positions.contains(&LightRay {
    //             pos: (x as isize, y as isize),
    //             dir: Down,
    //         }) {
    //             print!("v")
    //         } else {
    //             print!(".")
    //         }
    //     }
    //     println!();
    // }

    energized_cells.len()
}
