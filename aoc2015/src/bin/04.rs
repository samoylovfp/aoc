fn main() {
    for nonce in 0.. {
        let val = format!("yzbqklnj{nonce}");
        let digest = md5::compute(&val);
        let hash = format!("{:x}", digest);
        if hash.starts_with("000000") {
            dbg!(nonce);
            break;
        }
    }
}
