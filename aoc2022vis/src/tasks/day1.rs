use std::collections::BinaryHeap;

use eframe::egui::{Grid, ScrollArea};

use super::AocSolver;

pub struct Day1;

impl AocSolver for Day1 {
    fn title(&self) -> &str {
        "Day1: Calories"
    }
    fn show(&mut self, input: &str, ui: &mut eframe::egui::Ui) {
        let mut current_elf_total = 0;
        let mut maximums = BinaryHeap::new();
        let mut grid = vec![];
        for line in input.lines() {
            let snack_str = if line.is_empty() {
                maximums.push(current_elf_total);
                current_elf_total = 0;
                "".to_string()
            } else {
                match line.parse::<u32>() {
                    Err(e) => format!("Error parsing number: {e}"),
                    Ok(n) => {
                        current_elf_total += n;
                        line.to_string()
                    }
                }
            };
            grid.push((snack_str, current_elf_total.to_string()));
        }
        maximums.push(current_elf_total);

        ui.group(|ui| {
            ui.heading("Top values");
            let mut total = 0;
            for snack_value in maximums.into_sorted_vec().into_iter().rev().take(3) {
                ui.label(snack_value.to_string());
                total += snack_value;
            }
            ui.heading("Total");
            ui.label(total.to_string());
            ui.heading("Breakdown");
            ScrollArea::vertical().show(ui, |ui| {
                Grid::new("day1").show(ui, |ui| {
                    ui.label("Snack value");
                    ui.label("Total value of snacks of the current elf");
                    ui.end_row();
                    for (snack, elf) in grid {
                        ui.label(snack);
                        ui.label(elf);
                        ui.end_row();
                    }
                });
            });
        });
    }
}
