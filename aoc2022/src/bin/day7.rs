use std::{collections::HashMap, fs::read_to_string};

use itertools::Itertools;

fn main() {
    let input = read_to_string("input7.txt").unwrap();

    let mut tree: HashMap<Vec<String>, u64> = HashMap::new();
    let mut cur_path = vec![];

    for line in input.trim().lines() {
        let words = line.split_ascii_whitespace().collect_vec();
        // dbg!(line, &cur_path);
        match words.as_slice() {
            ["$", "cd", ".."] => {
                cur_path.pop();
            }
            ["$", "cd", dir] => cur_path.push(dir.to_string()),
            ["$", "ls"] => {}
            ["dir", _dir_name] => {}
            [size, _filename] => {
                for i in 0..cur_path.len() {
                    *tree.entry(cur_path[0..=i].to_vec()).or_default() +=
                        size.parse::<u64>().unwrap();
                }
            }
            _ => panic!(),
        }
    }
    dbg!(tree
        .values()
        .copied()
        .filter(|v| *v <= 100_000)
        .sum::<u64>());

    let required_free = 30_000_000;
    let current_free = 70_000_000 - tree[&vec!["/".to_string()]];
    let need_to_free = required_free - current_free;
    dbg!(tree
        .values()
        .sorted()
        .find(|&&v| v >= need_to_free)
        .unwrap());
}
