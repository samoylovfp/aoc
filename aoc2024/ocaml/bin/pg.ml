let rec inner (rep : int list) increasing =
  match rep with e1 :: e2 -> inner [] true | e1 -> false

let is_safe (rep : int list) =
  match rep with
  | e1 :: e2 :: rest -> inner (e1 :: e2 :: rest) (e1 < e2)
  | _ -> true

let part1 reports = List.filter is_safe reports |> List.length
