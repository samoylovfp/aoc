use std::fs::read_to_string;

fn main() {
    let data: Vec<u32> = read_to_string("input1.txt")
        .unwrap()
        .lines()
        .map(|l| l.parse().unwrap())
        .collect();

    println!(
        "First part {}",
        data.windows(2).filter(|w| w[1] > w[0]).count()
    );

    let sums3: Vec<u32> = data.windows(3).map(|w| w.iter().sum()).collect();

    println!(
        "Second part {}",
        sums3.windows(2).filter(|w| w[1] > w[0]).count()
    );
}
