use std::time::Duration;

use glam::IVec2;

fn main() {
    let input = std::fs::read_to_string("day14.txt").unwrap();
    let mut robots = vec![];
    for line in input.lines() {
        let (x, y, vx, vy) =
            scan_fmt::scan_fmt!(line, "p={d},{d} v={d},{d}", i32, i32, i32, i32).unwrap();
        robots.push((IVec2::new(x, y), IVec2::new(vx, vy)));
    }
    let mut robots_p2 = robots.clone();
    let w = 101;
    let h = 103;
    let size = IVec2::new(w, h);
    for _ in 0..100 {
        for r in &mut robots {
            r.0 += r.1;
            r.0 = (r.0 + size) % size;
        }
    }
    let mut res = 1;
    for qx in 0..2 {
        for qy in 0..2 {
            let mut sum = 0;
            for dx in 0..w / 2 {
                for dy in 0..h / 2 {
                    let x = qx * (w / 2 + 1) + dx;
                    let y = qy * (h / 2 + 1) + dy;
                    sum += robots.iter().filter(|r| r.0 == IVec2::new(x, y)).count();
                }
            }
            res *= sum;
        }
    }
    dbg!(res);
    let state_at = |i, robots: &Vec<(IVec2, IVec2)>, res: &mut Vec<IVec2>| {
        for ((pos, vel), res) in robots.iter().zip(res) {
            *res = ((pos + vel * i) % size + size) % size;
        }
    };
    let mut res = vec![IVec2::ZERO; robots_p2.len()];
    for s in 0..100000 {
        state_at(s, &robots_p2, &mut res);
        let mut long_verticals = vec![];
        for x in 0..w {
            let mut max_run = 0;
            let mut prev = false;
            let mut current_run = 0;
            for y in 0..h {
                if res.contains(&IVec2::new(x, y)) {
                    if prev {
                        current_run += 1;
                    } else {
                        prev = true;
                        current_run = 1;
                    }
                    max_run = std::cmp::max(current_run, max_run);
                } else {
                    prev = false;
                    current_run = 0;
                }
            }
            if max_run > 8 {
                long_verticals.push(max_run);
            }
        }

        if long_verticals.len() > 0 {
            println!("====== {s} =======");
            for y in 0..h {
                for x in 0..w {
                    let count = res.iter().filter(|r| **r == IVec2::new(x, y)).count();
                    if count == 0 {
                        print!(" ")
                    } else {
                        print!("{count}",)
                    }
                }
                println!();
            }
            std::thread::sleep(Duration::from_millis(100));
        }
    }
}
