use std::{collections::HashMap, fs::read_to_string};

fn main() {
    let input = read_to_string("input03.txt").unwrap();
    let mut gears = HashMap::<(usize, usize), Vec<u32>>::new();

    for (y, line) in input.lines().enumerate() {
        for (x, symb) in line.chars().enumerate() {
            if symb == '*' {
                gears.entry((x, y)).or_default();
            }
        }
    }

    for (y, line) in input.lines().enumerate() {
        let mut number_digits = vec![];
        let mut digit_start_x = None;
        for (x, symb) in line.chars().enumerate() {
            if let Some(n) = symb.to_digit(10) {
                number_digits.push(n);
                if digit_start_x.is_none() {
                    digit_start_x = Some(x)
                }
            }
            // end of the number
            if symb.to_digit(10).is_none() || x == line.chars().count() - 1 {
                if !number_digits.is_empty() {
                    let number = number_digits.iter().copied().fold(0, |acc, n| acc * 10 + n);

                    if let Some(xs) = digit_start_x {
                        for gx in xs.saturating_sub(1)..=x {
                            for yoff in -1..=1 {
                                let gy = y.saturating_add_signed(yoff);
                                if let Some(g) = gears.get_mut(&(gx, gy)) {
                                    g.push(number)
                                }
                            }
                        }
                    }
                }

                number_digits.clear();
                digit_start_x = None;
            }
        }
    }
    for (k,v) in &gears {
        println!("{k:?} {v:?}");
    }

    let result: u64 = gears
        .values()
        .filter(|v| v.len() == 2)
        .map(|v| (v[0] * v[1]) as u64)
        .sum();
    println!("{result}");
}
