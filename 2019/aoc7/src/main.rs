#![allow(dead_code)]
use std::collections::{VecDeque};
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread::JoinHandle;


#[derive(Clone)]
struct Mem {
    layout: Vec<i32>,
    instructions_pointer: usize
}

impl Mem {
    fn new(layout: Vec<i32>) -> Mem {
        Mem {
            layout, instructions_pointer: 0
        }
    }
    fn next(&mut self) -> i32 {
        let res = self.layout[self.instructions_pointer];
        self.instructions_pointer += 1;
        res
    }
    fn get_val(&self, addr: usize) -> i32 {
        self.layout[addr]
    }
    fn set_val(&mut self, addr: usize, val: i32) {
        self.layout[addr] = val
    }
    fn jump(&mut self, val: usize) {
        self.instructions_pointer = val;
    }
}

fn run_till_completion(
    mut mem: Mem,
    input: Receiver<i32>,
    output: Sender<i32>
) {
    loop {
        let op_encoded = mem.next();
        let op = op_encoded % 100;
        let first_operand_mode = op_encoded / 100 % 10;
        let second_operand_mode = op_encoded / 1000 % 10;
        match op {
            99 => break,
            1 | 2 | 7 | 8 => {
                let mut lhs = mem.next();
                let mut rhs = mem.next();
                let target = mem.next();
                if first_operand_mode == 0 {
                    lhs = mem.get_val(lhs as usize);
                }
                if second_operand_mode == 0 {
                    rhs = mem.get_val(rhs as usize);
                }
                let val = match op {
                    1 => lhs + rhs,
                    2 => lhs * rhs,
                    7 => if lhs < rhs {1} else {0},
                    8 => if lhs == rhs {1} else {0},
                    _ => unreachable!()
                };
                mem.set_val(target as usize, val)
            },
            3 => {
                let pos = mem.next();
                mem.set_val(pos as usize, input.recv().expect("input"))
            },
            4 => {
                let mut val = mem.next();
                if first_operand_mode == 0 {
                    val = mem.get_val(val as usize);
                }
                output.send(val).unwrap();
            },
            5 | 6 => {
                let mut op1 = mem.next();
                let mut op2 = mem.next();
                if first_operand_mode == 0 {
                    op1 = mem.get_val(op1 as usize);
                }
                if second_operand_mode == 0 {
                    op2 = mem.get_val(op2 as usize);
                }
                match op {
                    5 => if op1 != 0 {mem.jump(op2 as usize)},
                    6 => if op1 == 0 {mem.jump(op2 as usize)},
                    _ => unreachable!()
                }
            }
            o => panic!("Unknwon operation {}", o)
        }
    }
}

fn recalculate(mem: Mem, phase: i32, input: i32) -> i32 {
    let (in_tx, in_rx) = channel();
    let (out_tx, out_rx) = channel();
    in_tx.send(phase).unwrap();
    in_tx.send(input).unwrap();
    run_till_completion(mem, in_rx, out_tx);
    out_rx.recv().expect("an output from the programm")
}

#[derive(Debug)]
enum PermutatorState {
    // adding the element before sub-iterator
    Before,
    // adding elements after sub-iterator
    After,
    // done
    End
}


trait Peekable: Iterator {
    fn peek(&mut self) -> Option<Self::Item>;
}

impl<T> Peekable for std::iter::Peekable<T> 
    where T: Iterator,
    T::Item: Clone
     {

    fn peek(&mut self) -> Option<Self::Item> {
        self.peek().cloned()
    }
}

type Permutation<T> = VecDeque<T>;
type DynPeekable<T> = Box<dyn Peekable<Item=Permutation<T>>>;

// KILLME
struct Permutator<T> {
    // len is always > 1
    elements: Vec<T>,
    elements_idx: usize,
    sub_permutator: DynPeekable<T>,
    state: PermutatorState,
}

impl<T: 'static + Copy> Peekable for Permutator<T> {
    fn peek(&mut self) -> Option<Self::Item> {
        self.produce()
    }
}

impl<T: 'static + Copy> Permutator<T> {
    fn new(elements: Vec<T>) -> Box<dyn Peekable<Item=VecDeque<T>>> {
        if elements.len() == 1 {
            return Box::new(std::iter::once(elements.into()).peekable())
        }
        let mut less_elements = elements.clone();
        less_elements.pop();
        let p = Permutator {
            elements_idx: elements.len() - 1,
            elements: elements,
            sub_permutator: Permutator::new(less_elements),
            state: PermutatorState::Before
        };
        Box::new(p)
    }
    fn current_element(&self) -> T {
        self.elements[self.elements_idx]
    }
    fn produce(&mut self) -> Option<VecDeque<T>> {
        use PermutatorState::*;
        match self.state {
            End => None,
            Before | After => {
                let element = self.sub_permutator.peek();

                match element {
                    None => {
                        self.advance();
                        self.produce()
                    },
                    Some(mut e) => {
                        match self.state {
                            Before => {
                                e.push_back(self.current_element());
                                Some(e.clone())
                            },
                            After => {
                                e.push_front(self.current_element());
                                Some(e.clone())
                            },
                            End => None
                        }
                    }
                }
            }
        }
    }

    fn reset_sub_iterator(&mut self) {
        // Optimization: combine cloning and element removal
        let mut vector_without_element = self.elements.clone();
        vector_without_element.remove(self.elements_idx);
        self.sub_permutator = Permutator::new(vector_without_element);
    }

    fn advance(&mut self) {
        use PermutatorState::*;
        self.state = match self.state {
            End => return,
            Before => {
                self.sub_permutator.next();
                match self.sub_permutator.peek() {
                    None => if self.elements_idx == 0 {
                            End
                        } else {
                            self.elements_idx -= 1;
                            self.reset_sub_iterator();
                            Before
                        },
                    Some(_) => Before
                }
            },
            _ => unreachable!()
        }
    }
}

impl<T: 'static + Copy> Iterator for Permutator<T> {
    type Item=VecDeque<T>;
    fn next(&mut self) -> Option<Self::Item> {
        let res = self.produce();
        self.advance();
        res
    }
}

fn get_max_output(instructions: Vec<i32>) -> i32 {
    let mem = Mem::new(instructions);
    let mut max_output = None;
    for phase_setup in Permutator::new(vec![0,1,2,3,4]) {
        let mut output = 0;
        for phase in phase_setup.iter() {
            let result = recalculate(mem.clone(), *phase, output);
            output = result;
        }
        max_output = Some(output).max(max_output);
    }
    max_output.expect("phases")
}

fn recalculate_parallel(
    mem: Mem,
    input: Receiver<i32>,
    output: Sender<i32>) -> JoinHandle<()> {

    std::thread::spawn(move || {
        run_till_completion(mem,
            input, output)
    })
}

fn run_duplucator(
    last_stage_out: Receiver<i32>,
    first_stage_in: Sender<i32>
) -> JoinHandle<i32> {
        std::thread::spawn(move || {
            let mut last_value = None;
            for value in last_stage_out {
                last_value = Some(value);
                if let Err(_) = first_stage_in.send(value) {
                    // accepting amplifier already terminated
                    // No action required
                }
            }
            return last_value.unwrap();
        })
}

fn get_max_output_with_feedback(instructions: Vec<i32>) -> i32 {
    // build channels between amplifiers
    // last channel should duplicate data into a channel 
    // that goes to thrusters
    // each channel should receive phase setting first
    // first channel should also receive 0
    let mem = Mem::new(instructions);
    let mut max_thruster_output = None;

    for phase_setup in Permutator::new(vec![5,6,7,8,9]) {
        let mut thread_handlers = Vec::new();

        let mut input_receivers = Vec::new();
        let mut output_senders = Vec::new();
        let mut send_zero = true;
        for phase in phase_setup {
            let (input_sender, input_receiver) = channel();
            input_sender.send(phase).unwrap();
            if send_zero {
                input_sender.send(0).unwrap();
                send_zero = false;
            }
            input_receivers.push(input_receiver);
            // output of one amplifier is the input of another
            output_senders.push(input_sender);
        }
        let (dup_sender, dup_receiver) = channel();
        input_receivers.push(dup_receiver);
        output_senders.push(dup_sender);

        // this makes the first sender to be the input of the second amp
        // and so on
        output_senders.rotate_left(1);
        let amplifier_inputs_len = input_receivers.len() - 1;
        let mut zipped_channels = input_receivers
            .into_iter()
            .zip(output_senders)
            .collect::<Vec<_>>();
        let dup_channels = zipped_channels.split_off(amplifier_inputs_len);
        for (input, output) in zipped_channels {
            thread_handlers.push(recalculate_parallel(mem.clone(), input, output));
        }
        let (dup_input, dup_output) = dup_channels.into_iter().next().unwrap();
        let result = run_duplucator(dup_input, dup_output).join().unwrap();
        max_thruster_output = max_thruster_output.max(Some(result));
    }
    max_thruster_output.expect("phases")
    // join all threads
}



fn main() {
    let instructions = std::fs::read_to_string("input.txt").expect("input file")
    .trim().split(",").map(|s|s.parse().expect("valid number")
    ).collect::<Vec<i32>>();
    println!("First output is {}", get_max_output(instructions.clone()));
    println!("Second output is {}", get_max_output_with_feedback(instructions));
}

#[test]
fn test_provided1() {
    assert_eq!(get_max_output(vec![3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0]), 43210);
}
#[test]
fn test_provided2() {
    assert_eq!(get_max_output(vec![3,23,3,24,1002,24,10,24,1002,23,-1,23,
        101,5,23,23,1,24,23,23,4,23,99,0,0]), 54321);
}

#[test]
fn test_provided3() {
    assert_eq!(get_max_output(vec![3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
        1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0]), 65210);
}


#[test]
fn test_simple_perms() {
    assert_eq!(
        Permutator::new(vec![1,2])
        .map(|vd|Vec::from(vd))
        .collect::<Vec<Vec<i32>>>(),
        vec![
            vec![1,2],
            vec![2,1],
        ]
    );
}

#[test]
fn test_perms() {
    assert_eq!(
        Permutator::new(vec![0,1,2])
        .map(|vd|Vec::from(vd))
        .collect::<Vec<Vec<i32>>>(),
            vec![
                [0, 1, 2],
                [1, 0, 2],
                [0, 2, 1],
                [2, 0, 1],
                [1, 2, 0],
                [2, 1, 0]
            ]
    );
}

#[test]
fn test_provided_p2_1() {
    assert_eq!(
        get_max_output_with_feedback(vec![
            3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,
            27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5        
        ]),
        139629729
    )
}

#[test]
fn test_provided_p2_2() {
    assert_eq!(
        get_max_output_with_feedback(vec![
            3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,
            -5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,
            53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10      
        ]),
        18216
    )
}
