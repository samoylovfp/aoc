use std::{collections::VecDeque, fs::read_to_string};

use itertools::Itertools;

fn main() {
    let input = read_to_string("input6.txt").unwrap();
    dbg!(get_position_of_distinct_chars(&input, 4));
    dbg!(get_position_of_distinct_chars(&input, 14));
}
fn get_position_of_distinct_chars(input: &str, required_distinct_count: usize) -> usize {
    let mut last_chars = VecDeque::new();
    for (i, char) in input.trim().chars().enumerate() {
        while last_chars.len() > required_distinct_count {
            last_chars.pop_front();
        }
        if last_chars.len() == required_distinct_count
        // having OrderedHashMap<char, count> is more effective
            && last_chars.iter().sorted().dedup().count() == required_distinct_count
        {
            return i;
        }
        last_chars.push_back(char)
    }
    if last_chars.iter().sorted().dedup().count() == required_distinct_count {
        return input.chars().count();
    } else {
        panic!("not found")
    }
}
