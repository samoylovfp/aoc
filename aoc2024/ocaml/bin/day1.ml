open Printf
open String

let find_distance (l1 : int list) (l2 : int list) : int =
  List.sort Int.compare l1
  |> List.combine (List.sort Int.compare l2)
  |> List.fold_left (fun acc (v1, v2) -> acc + (abs @@ (v1 - v2))) 0

let count_multiples (l1 : int list) (l2 : int list) : int =
  let count v l = List.length @@ List.filter (fun v2 -> v2 == v) l in
  List.fold_left (fun acc v1 -> acc + (v1 * count v1 l2)) 0 l1

let parse_line (l1, l2) line =
  let nums =
    line |> String.split_on_char ' '
    |> List.filter (fun x -> x <> "")
    |> List.map int_of_string
  in
  match nums with n1 :: n2 :: rest -> (n1 :: l1, n2 :: l2) | _ -> (l1, l2)

let () =
  "day1.txt" |> open_in
  |> In_channel.fold_lines parse_line ([], [])
  |> (fun (l1, l2) -> find_distance l1 l2)
  |> printf "Part 1: %d\n";

  "day1.txt" |> open_in
  |> In_channel.fold_lines parse_line ([], [])
  |> (fun (l1, l2) -> count_multiples l1 l2)
  |> printf "Part 2: %d\n";
  flush stdout
