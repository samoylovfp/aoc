use std::fs::read_to_string;

use itertools::Itertools;

fn main() -> anyhow::Result<()> {
    let input = read_to_string("input1.txt").unwrap();
    let elfs_str_iter = input.trim().lines().group_by(|l| !l.is_empty());

    let elf_snacks = elfs_str_iter
        .into_iter()
        .filter_map(|(is_not_empty, elf_lines)| is_not_empty.then(|| parse_elf(elf_lines)))
        .collect_vec();

    dbg!(elf_snacks.iter().max().unwrap());
    dbg!(elf_snacks.iter().sorted().rev().take(3).sum::<u32>());
    Ok(())
}

fn parse_elf<'a>(lines: impl Iterator<Item = &'a str>) -> u32 {
    lines.map(|l| l.parse::<u32>().unwrap()).sum()
}
