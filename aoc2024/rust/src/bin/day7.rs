use itertools::Itertools;
use nom::{
    bytes::complete::{tag, take_while},
    multi::separated_list1,
    sequence::separated_pair,
    Parser,
};

fn main() {
    // let input = std::fs::read_to_string("../day7_ex.txt").unwrap();
    let input = std::fs::read_to_string("../day7_full.txt").unwrap();
    dbg!(part1(&input));
    dbg!(part2(&input));
}

fn part1(input: &str) -> String {
    let equations = input.lines().map(|line| parse_eq(line).unwrap().1);

    equations
        .filter(|(res, vals)| can_be_made_true(*res, &vals, false))
        .map(|(res, _vals)| res)
        .sum::<u64>()
        .to_string()
}

fn part2(input: &str) -> String {
    let equations = input.lines().map(|line| parse_eq(line).unwrap().1);

    equations
        .filter(|(res, vals)| can_be_made_true(*res, &vals, true))
        .map(|(res, _vals)| res)
        .sum::<u64>()
        .to_string()
}

#[derive(Debug, Clone, Copy)]
enum Op {
    Mul,
    Plus,
    Concat,
}

impl Op {
    fn apply(&self, v1: u64, v2: u64) -> u64 {
        match self {
            Op::Mul => v1 * v2,
            Op::Plus => v1 + v2,
            Op::Concat => v1 * 10_u64.pow(v2.ilog10() + 1) + v2,
        }
    }
}

fn can_be_made_true(expected: u64, vals: &[u64], part2: bool) -> bool {
    let ops = if part2 {
        [Op::Plus, Op::Mul, Op::Concat].as_slice()
    } else {
        [Op::Plus, Op::Mul].as_slice()
    };

    for operators in itertools::repeat_n(ops, vals.len() - 1).multi_cartesian_product() {
        let init = operators[0].apply(vals[0], vals[1]);

        let res = vals
            .iter()
            .copied()
            .skip(2)
            .zip(&operators[1..])
            .fold(init, |acc, (val, op)| op.apply(acc, val));
        // println!("{operators:?}, {res}");
        if res == expected {
            return true;
        }
    }

    false
}

fn parse_eq(i: &str) -> nom::IResult<&str, (u64, Vec<u64>)> {
    let decimal = || take_while(|c: char| c.is_digit(10)).map(|v: &str| v.parse().unwrap());

    separated_pair(decimal(), tag(": "), separated_list1(tag(" "), decimal())).parse(i)
}
#[test]
fn test_292() {
    assert!(can_be_made_true(292, &[11, 6, 16, 20], false))
}
