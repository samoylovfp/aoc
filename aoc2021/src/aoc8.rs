use std::{collections::HashMap, fs::read_to_string};

fn main() {
    let input = read_to_string("input8.txt").unwrap();
    let displays: Vec<(Vec<String>, Vec<String>)> = parse_displays(&input);
    dbg!(part_one(&displays));
    dbg!(part_two(&displays));
}

fn parse_displays(input: &str) -> Vec<(Vec<String>, Vec<String>)> {
    input
        .trim()
        .lines()
        .map(|l| {
            let (before, after) = l.split_once("|").unwrap();
            (
                before
                    .split_whitespace()
                    .map(|s| s.trim().to_string())
                    .collect::<Vec<String>>(),
                after
                    .split_whitespace()
                    .map(|s| s.trim().to_string())
                    .collect::<Vec<String>>(),
            )
        })
        .collect()
}

fn part_one(displays: &[(Vec<String>, Vec<String>)]) -> u32 {
    displays
        .iter()
        .map(|(_before, after)| {
            after
                .iter()
                .filter(|d| matches!(d.len(), 2 | 3 | 4 | 7))
                .count()
        })
        .sum::<usize>() as u32
}

static DISPLAY_SEGMENTS: &[&str] = &[
    "abcefg", "cf", "acdeg", "acdfg", "bcdf", "abdfg", "abdefg", "acf", "abcdefg", "abcdfg",
];

static WIRES: &[char] = &['a', 'b', 'c', 'd', 'e', 'f', 'g'];

fn part_two(displays: &[(Vec<String>, Vec<String>)]) -> u32 {
    dbg!(WIRES.iter().copied().permutations(7).count());
    use itertools::Itertools;

    let mut sum = 0;

    'patterns: for (patterns, values) in displays {
        for possible_combination in WIRES.iter().copied().permutations(7) {
            if is_correct_combination(&possible_combination, patterns) {
                sum += decode(&possible_combination, values);
                continue 'patterns;
            }
        }
        panic!("No valid combinations for {:?}", patterns);
    }
    sum
}

fn decode(possible_combination: &[char], values: &[String]) -> u32 {
    let mut sum = 0;
    let wiring: HashMap<char, char> = WIRES
        .iter()
        .copied()
        .zip(possible_combination.iter().copied())
        .collect();
    let mut digits_with_this_wiring: Vec<Vec<char>> = DISPLAY_SEGMENTS
        .iter()
        .map(|ds| {
            ds.chars()
                .map(|c| wiring.get(&c).unwrap())
                .copied()
                .collect()
        })
        .collect();
    digits_with_this_wiring
        .iter_mut()
        .for_each(|d| d.sort_unstable());

    for (i, number) in values.iter().rev().enumerate() {
        let mut number: Vec<char> = number.chars().collect();
        number.sort_unstable();
        sum += (digits_with_this_wiring
            .iter()
            .position(|d| d == &number)
            .unwrap()
            * 10_usize.pow(i as u32)) as u32;
    }
    sum
}

fn is_correct_combination(possible_combination: &[char], patterns: &[String]) -> bool {
    let wiring: HashMap<char, char> = WIRES
        .iter()
        .copied()
        .zip(possible_combination.iter().copied())
        .collect();
    let mut digits_with_this_wiring: Vec<Vec<char>> = DISPLAY_SEGMENTS
        .iter()
        .map(|ds| {
            ds.chars()
                .map(|c| wiring.get(&c).unwrap())
                .copied()
                .collect()
        })
        .collect();
    digits_with_this_wiring
        .iter_mut()
        .for_each(|d| d.sort_unstable());
    patterns.iter().all(|p| {
        let mut p_chars: Vec<char> = p.chars().collect();
        p_chars.sort_unstable();
        digits_with_this_wiring.contains(&p_chars)
    })
}

#[test]
fn test_provided() {
    let displays = parse_displays(&read_to_string("sample8.txt").unwrap());
    assert_eq!(part_one(&displays), 26);
    assert_eq!(part_two(&displays), 61229)
}

#[test]
fn test_is_correct_combination() {
    let str_vec: Vec<String> = ["acedgfb", "cdfbe", "gcdfa", "fbcad", "dab", "cefabd", "ab"]
        .into_iter()
        .map(|s| s.to_string())
        .collect();
    assert!(is_correct_combination(
        &['d', 'e', 'a', 'f', 'g', 'b', 'c'],
        &str_vec
    ));
    assert!(!is_correct_combination(
        &['d', 'e', 'a', 'f', 'g', 'c', 'b'],
        &str_vec
    ))
}

#[test]
fn test_decode() {
    let str_vec: Vec<String> = ["ab", "cagedb"]
        .into_iter()
        .map(|s| s.to_string())
        .collect();

    assert_eq!(decode(&['d', 'e', 'a', 'f', 'g', 'b', 'c'], &str_vec), 10)
}
