use std::{collections::HashSet, fs::read_to_string};

fn main() {
    let input = read_to_string("input9.txt").unwrap();
    // +X - right
    // +Y - up
    dbg!(solve(&input, 2));
    dbg!(solve(&input, 10));
}

fn solve(input: &str, rope_len: usize) -> usize {
    let mut visited_cells_by_tail: HashSet<(i32, i32)> = HashSet::new();
    let mut rope = vec![(0, 0); rope_len];

    visited_cells_by_tail.insert(*rope.last().unwrap());

    for instruction in input.lines() {
        let (direction, steps) = instruction.split_once(' ').unwrap();
        let direction = match direction {
            "R" => (1, 0),
            "U" => (0, 1),
            "L" => (-1, 0),
            "D" => (0, -1),
            _ => panic!(),
        };
        for _ in 0..steps.parse().unwrap() {
            let head_pos = rope.first_mut().unwrap();
            head_pos.0 += direction.0;
            head_pos.1 += direction.1;
            for i in 1..rope.len() {
                let diff = (rope[i - 1].0 - rope[i].0, rope[i - 1].1 - rope[i].1);
                if diff.0.abs() > 1 || diff.1.abs() > 1 {
                    rope[i].0 += diff.0.signum();
                    rope[i].1 += diff.1.signum();
                }
            }

            visited_cells_by_tail.insert(*rope.last().unwrap());
        }
    }
    // println!();
    // for y in 0..5 {
    //     for x in 0..5 {
    //         if visited_cells_by_tail.contains(&(x, y)) {
    //             print!("#")
    //         } else {
    //             print!(".")
    //         }
    //     }
    //     println!()
    // }
    visited_cells_by_tail.len()
}
