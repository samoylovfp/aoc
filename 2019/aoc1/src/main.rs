fn get_fuel_cost(mass: u32) -> u32 {
    (mass / 3).saturating_sub(2)
}

fn get_recursive_fuel_cost(mut mass: u32) -> u32 {
    let mut result = 0;
    loop {
        mass = get_fuel_cost(mass);
        if mass == 0 {
            break;
        }
        result += mass;
    }
    result
}

fn main() {
    let masses: Vec<u32> = std::fs::read_to_string("input.txt")
        .expect("input")
        .split_whitespace()
        .map(std::str::FromStr::from_str)
        .filter_map(Result::ok)
        .collect();

    println!(
        "Part 1 is {}",
        masses.iter().copied().map(get_fuel_cost).sum::<u32>()
    );
    println!(
        "Part 2 is {}",
        masses
            .iter()
            .copied()
            .map(get_recursive_fuel_cost)
            .sum::<u32>()
    )
}

#[test]
fn test_fuel_cost() {
    assert_eq!(get_fuel_cost(12), 2);
    assert_eq!(get_fuel_cost(14), 2);
    assert_eq!(get_fuel_cost(1969), 654);
    assert_eq!(get_fuel_cost(100756), 33583);
}

#[test]
fn test_fuel_recur_cost() {
    assert_eq!(get_recursive_fuel_cost(14), 2);
}
