use std::{fmt::Display, fs::read_to_string, iter::Peekable, ops::Add};

fn main() {
    let input = read_to_string("input18.txt").unwrap();
    dbg!(part_one(&input));
    dbg!(part_two(&input));
}

fn part_two(input: &str) -> u32 {
    let nums = input
        .trim()
        .lines()
        .map(|l| SnailNumber::parse_str(l.trim()))
        .collect::<Vec<_>>();

    nums.iter()
        .map(|n| {
            nums.iter()
                .filter(|&n2| n2 != n)
                .map(|n2| {
                    let mut added = n.clone() + n2.clone();
                    added.reduce();
                    added.magnitude()
                })
                .max()
                .unwrap()
        })
        .max()
        .unwrap()
}

fn sum(input: &str) -> SnailNumber {
    let mut lines = input.trim().lines();
    let mut result = SnailNumber::parse_str(lines.next().unwrap());
    for line in lines {
        result = result + SnailNumber::parse_str(line.trim());
        result.reduce();
    }
    result
}

fn part_one(input: &str) -> u32 {
    sum(input).magnitude()
}

#[derive(Debug, Clone, PartialEq)]
enum IntOrSubNumber {
    Int(u32),
    SubNumber(Box<SnailNumber>),
}

impl IntOrSubNumber {
    fn parse(input: &mut Peekable<impl Iterator<Item = char>>) -> IntOrSubNumber {
        if input.peek() == Some(&'[') {
            IntOrSubNumber::SubNumber(Box::new(SnailNumber::parse(input)))
        } else {
            let mut digits = String::new();
            while input.peek().map(|c| c.is_ascii_digit()).unwrap_or(false) {
                digits.push(input.next().unwrap())
            }
            IntOrSubNumber::Int(digits.parse().unwrap())
        }
    }

    fn explode(&mut self, level: usize) -> Option<Explosion> {
        if let IntOrSubNumber::SubNumber(sn) = self {
            let maybe_pair_of_ints = sn.get_pair_of_ints();
            if level >= 4 && maybe_pair_of_ints.is_some() {
                let (left, right) = maybe_pair_of_ints.unwrap();
                *self = IntOrSubNumber::Int(0);
                return Some(Explosion {
                    left: Some(left),
                    right: Some(right),
                });
            } else {
                return sn.explode_from_level(level + 1);
            }
        }
        None
    }

    fn add_left(&mut self, additive: u32) {
        match self {
            IntOrSubNumber::Int(int) => *int += additive,
            IntOrSubNumber::SubNumber(sn) => sn.add_left(additive),
        }
    }

    fn add_right(&mut self, additive: u32) {
        match self {
            IntOrSubNumber::Int(int) => *int += additive,
            IntOrSubNumber::SubNumber(sn) => sn.add_right(additive),
        }
    }

    fn maybe_split(&mut self) -> bool {
        match self {
            IntOrSubNumber::Int(int) => {
                let int = *int;
                if int >= 10 {
                    *self = IntOrSubNumber::SubNumber(Box::new(SnailNumber {
                        left: IntOrSubNumber::Int(int / 2),
                        right: IntOrSubNumber::Int(int / 2 + int % 2),
                    }));
                    return true;
                }
            }
            IntOrSubNumber::SubNumber(sn) => return sn.maybe_split(),
        }
        false
    }

    fn magnitude(&self) -> u32 {
        match self {
            IntOrSubNumber::Int(n) => *n,
            IntOrSubNumber::SubNumber(sn) => sn.magnitude(),
        }
    }
}

impl Display for IntOrSubNumber {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            IntOrSubNumber::Int(n) => n.fmt(f)?,
            IntOrSubNumber::SubNumber(sn) => sn.fmt(f)?,
        }
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq)]
struct SnailNumber {
    left: IntOrSubNumber,
    right: IntOrSubNumber,
}

#[derive(Debug)]
struct Explosion {
    left: Option<u32>,
    right: Option<u32>,
}

impl SnailNumber {
    fn parse(input: &mut Peekable<impl Iterator<Item = char>>) -> SnailNumber {
        assert_eq!(input.next(), Some('['));
        let left = IntOrSubNumber::parse(input);
        assert_eq!(input.next(), Some(','));
        let right = IntOrSubNumber::parse(input);
        assert_eq!(input.next(), Some(']'));
        SnailNumber { left, right }
    }
    fn parse_str(input: &str) -> SnailNumber {
        SnailNumber::parse(&mut input.chars().peekable())
    }

    fn add_left(&mut self, additive: u32) {
        self.left.add_left(additive);
    }

    fn add_right(&mut self, additive: u32) {
        self.right.add_right(additive);
    }

    fn reduce(&mut self) {
        loop {
            if !(self.maybe_explode() || self.maybe_split()) {
                break;
            }
        }
    }

    /// Should be called on the root number
    fn maybe_explode(&mut self) -> bool {
        self.explode_from_level(1).is_some()
    }

    fn explode_from_level(&mut self, level: usize) -> Option<Explosion> {
        if let Some(mut explosion) = self.left.explode(level) {
            if let Some(er) = explosion.right.take() {
                self.right.add_left(er);
            }
            return Some(explosion);
        }

        if let Some(mut explosion) = self.right.explode(level) {
            if let Some(el) = explosion.left.take() {
                self.left.add_right(el);
            }
            return Some(explosion);
        }

        None
    }

    fn get_pair_of_ints(&self) -> Option<(u32, u32)> {
        if let (IntOrSubNumber::Int(l), IntOrSubNumber::Int(r)) = (&self.left, &self.right) {
            Some((*l, *r))
        } else {
            None
        }
    }

    fn maybe_split(&mut self) -> bool {
        self.left.maybe_split() || self.right.maybe_split()
    }

    fn magnitude(&self) -> u32 {
        self.left.magnitude() * 3 + self.right.magnitude() * 2
    }
}

impl Add for SnailNumber {
    type Output = SnailNumber;

    fn add(self, rhs: Self) -> Self::Output {
        SnailNumber {
            left: IntOrSubNumber::SubNumber(Box::new(self)),
            right: IntOrSubNumber::SubNumber(Box::new(rhs)),
        }
    }
}

impl Display for SnailNumber {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{},{}]", self.left, self.right)?;
        Ok(())
    }
}

#[test]
fn test_parse() {
    use IntOrSubNumber::*;
    assert_eq!(
        SnailNumber::parse_str("[1,2]"),
        SnailNumber {
            left: Int(1),
            right: Int(2)
        }
    );
    assert_eq!(
        SnailNumber::parse_str("[1,[2,3]]"),
        SnailNumber {
            left: Int(1),
            right: SubNumber(Box::new(SnailNumber {
                left: Int(2),
                right: Int(3)
            }))
        }
    )
}

#[test]
fn test_addition() {
    assert_eq!(
        SnailNumber::parse_str("[1,2]") + SnailNumber::parse_str("[3,4]"),
        SnailNumber::parse_str("[[1,2],[3,4]]")
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use hamcrest2::prelude::*;
    use test_case::test_case;

    #[test_case("[1,2]", "[1,2]" ; "no_explosion")]
    #[test_case("[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]" ; "explode_right")]
    #[test_case("[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]" ; "explode_left")]
    #[test_case("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]" ; "explode_middle")]
    #[test_case("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]" ; "explode_middle_2")]
    #[test_case("[[[[4,0],[5,0]],[[[4,5],[2,6]],[9,5]]],[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]]",
                "[[[[4,0],[5,4]],[[0,[7,6]],[9,5]]],[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]]" ; "explode_larger_2")]
    fn test_explosion_detection(before_explosion: &str, after_explosion: &str) {
        let mut n = SnailNumber::parse_str(before_explosion);
        n.maybe_explode();
        assert_eq!(n, SnailNumber::parse_str(after_explosion), "actual {}", n)
    }

    #[test_case("[1,2]", "[1,2]" ; "no_split")]
    #[test_case("[11,2]", "[[5,6],2]" ; "single_split")]
    fn test_split(before_split: &str, after_split: &str) {
        let mut n = SnailNumber::parse_str(before_split);
        n.maybe_split();
        assert_eq!(n, SnailNumber::parse_str(after_split), "actual {}", n)
    }

    #[ignore = "fails"]
    #[test]
    fn test_provided() {
        let sn_sum = sum(r#"
        [[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
        [7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
        [[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
        [[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
        [7,[5,[[3,8],[1,4]]]]
        [[2,[2,2]],[8,[8,1]]]
        [2,9]
        [1,[[[9,3],9],[[9,0],[0,7]]]]
        [[[5,[7,4]],7],1]
        [[[[4,2],2],6],[8,7]]
                    "#);

        assert_eq!(
            sn_sum,
            SnailNumber::parse_str("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"),
            "actual {}",
            sn_sum
        );
    }

    #[test]
    fn test_magnitude() {
        assert_eq!(SnailNumber::parse_str("[[1,2],[[3,4],5]]").magnitude(), 143);
        assert_eq!(
            SnailNumber::parse_str("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]")
                .magnitude(),
            3488
        );
    }

    #[test_case(&["[1,1]","[2,2]", "[3,3]", "[4,4]"] 
                => is eq("[[[[1,1],[2,2]],[3,3]],[4,4]]") ; "no_conversion")]
    #[test_case(&["[1,1]","[2,2]", "[3,3]", "[4,4]", "[5,5]"] 
                => is eq("[[[[3,0],[5,3]],[4,4]],[5,5]]") ; "single_exp")]
    #[test_case(&["[1,1]","[2,2]", "[3,3]", "[4,4]", "[5,5]", "[6,6]"] 
                => is eq("[[[[5,0],[7,4]],[5,5]],[6,6]]") ; "single_exp_and_split")]
    #[test_case(&["[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]","[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]"] 
                => is eq("[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]") ; "larger_1")]
    fn test_sum(lines: &[&str]) -> String {
        format!("{}", sum(&lines.join("\n")))
    }
}
