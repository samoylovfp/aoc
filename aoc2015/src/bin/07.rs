use std::collections::HashMap;

use winnow::{
    combinator::{alt, preceded, rest, separated_pair},
    token::{literal, take_while},
    PResult, Parser,
};

fn main() {
    let input = std::fs::read_to_string("day07.txt").unwrap();
    let mut wire_to_source = HashMap::new();
    for instr in input.lines() {
        let parsed = parse_instr.parse(instr).unwrap();
        wire_to_source.insert(parsed.target.clone(), parsed);
    }

    let mut vals_cache = HashMap::new();
    let a = dbg!(get_reg_val(&wire_to_source, &mut vals_cache, "a"));
    vals_cache.clear();
    vals_cache.insert("b".to_string(), a);
    dbg!(get_reg_val(&wire_to_source, &mut vals_cache, "a"));
}

fn get_reg_val(
    regs: &HashMap<String, Instruction>,
    vals: &mut HashMap<String, u16>,
    reg: &str,
) -> u16 {
    if let Some(v) = vals.get(reg) {
        return *v;
    }
    let mut src = |s: &Source| match s {
        Source::Const(c) => *c,
        Source::Reg(r) => get_reg_val(regs, vals, &r),
    };
    let res = match &regs[reg].source {
        DataSource::Straight(s) => src(s),
        DataSource::And(s1, s2) => src(s1) & src(s2),
        DataSource::Or(s1, s2) => src(s1) | src(s2),
        DataSource::Not(s) => !src(s),
        DataSource::LShift(s1, s2) => src(s1) << src(s2),
        DataSource::RShift(s1, s2) => src(s1) >> src(s2),
    };
    vals.insert(reg.to_string(), res);
    res
}

fn parse_instr(mut instr: &mut &str) -> PResult<Instruction> {
    let decimal = || take_while(1.., |c: char| c.is_digit(10));
    let reg_id = || take_while(1.., |c: char| c.is_alphabetic());
    let source = || {
        alt((
            decimal().map(|v: &str| Source::Const(v.parse().unwrap())),
            reg_id().map(|v: &str| Source::Reg(v.to_string())),
        ))
    };

    let parse_source = alt((
        preceded("NOT ", source()).map(DataSource::Not),
        separated_pair(source(), " AND ", source()).map(|(s1, s2)| DataSource::And(s1, s2)),
        separated_pair(source(), " OR ", source()).map(|(s1, s2)| DataSource::Or(s1, s2)),
        separated_pair(source(), " LSHIFT ", source()).map(|(s1, s2)| DataSource::LShift(s1, s2)),
        separated_pair(source(), " RSHIFT ", source()).map(|(s1, s2)| DataSource::RShift(s1, s2)),
        source().map(|s: Source| DataSource::Straight(s)),
    ));

    separated_pair(parse_source, literal(" -> "), reg_id())
        .map(|(source, target): (DataSource, &str)| Instruction {
            target: target.to_string(),
            source,
        })
        .parse_next(instr)
}

#[derive(Debug)]
struct Instruction {
    target: String,
    source: DataSource,
}

#[derive(Debug)]
enum DataSource {
    Straight(Source),
    And(Source, Source),
    Or(Source, Source),
    Not(Source),
    LShift(Source, Source),
    RShift(Source, Source),
}

#[derive(Debug)]
enum Source {
    Const(u16),
    Reg(String),
}
