use std::{collections::HashSet, fs::read_to_string};

use itertools::Itertools;

fn main() {
    let map = parse_map(&read_to_string("input9.txt").unwrap());
    dbg!(part_one(&map));
    dbg!(part_two(&map));
}

fn parse_map(arg: &str) -> Vec<Vec<u8>> {
    arg.trim()
        .lines()
        .map(|l| l.chars().map(|c| c.to_digit(10).unwrap() as u8).collect())
        .collect()
}

fn part_one(map: &[Vec<u8>]) -> u32 {
    let y_max = map.len() - 1;
    let x_max = map[0].len() - 1;
    let mut sum = 0;
    (0..=y_max).for_each(|y| {
        (0..=x_max).for_each(|x| {
            if is_lowest_point((x, y), map) {
                let current = map[y][x];
                sum += current as u32 + 1
            }
        })
    });

    sum
}

fn iter_neighbours(
    (x, y): (usize, usize),
    map: &[Vec<u8>],
) -> impl Iterator<Item = (usize, usize)> {
    let y_max = map.len() - 1;
    let x_max = map[0].len() - 1;
    let mut neighbours = vec![];
    if x > 0 {
        neighbours.push((x - 1, y))
    }
    if y > 0 {
        neighbours.push((x, y - 1))
    }
    if x < x_max {
        neighbours.push((x + 1, y))
    }
    if y < y_max {
        neighbours.push((x, y + 1))
    }
    neighbours.into_iter()
}

fn is_lowest_point((x, y): (usize, usize), map: &[Vec<u8>]) -> bool {
    let current = map[y][x];
    iter_neighbours((x, y), map).all(|(nx, ny)| map[ny][nx] > current)
}

fn part_two(map: &[Vec<u8>]) -> u32 {
    map
        .iter()
        .enumerate()
        .flat_map(|(y, line)| {
            line.iter().enumerate().filter_map(move |(x, _current)| {
                is_lowest_point((x, y), map).then(move || {
                    let mut basin_points = HashSet::new();
                    calculate_sub_basin_size((x, y), map, &mut basin_points);
                    basin_points.len() as u32
                })
            })
        })
        .sorted()
        .rev()
        .take(3)
        .product()
}

fn calculate_sub_basin_size(
    (x, y): (usize, usize),
    map: &[Vec<u8>],
    points: &mut HashSet<(usize, usize)>,
) {
    let current = map[y][x];
    if current == 9 {
        return;
    }
    points.insert((x, y));
    iter_neighbours((x, y), map).for_each(|(nx, ny)|{
        if map[ny][nx] > current {
            calculate_sub_basin_size((nx, ny), map, points);
        }
    });
}

#[test]
fn test_provided() {
    let map = parse_map(
        r#"    
2199943210
3987894921
9856789892
8767896789
9899965678
    "#,
    );
    assert_eq!(part_one(&map), 15);
    assert_eq!(part_two(&map), 1134);
}
