use std::collections::{BTreeSet, HashMap, HashSet};

use aoc_rust::{Grid, IVec2, UVec2};
use itertools::Itertools;

#[derive(Default, Debug)]
struct Island {
    cells: HashSet<IVec2>,
    perimeter: usize,
    sides_count: Option<usize>,
}

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
enum Dir {
    Right,
    Left,
    Up,
    Down,
}

fn main() {
    let g = Grid::from_str_chars(&std::fs::read_to_string("../day12_full.txt").unwrap());
    // gather region names
    let mut region_types = HashMap::<char, HashSet<IVec2>>::new();
    for (pos, val) in g.iter_cells() {
        region_types.entry(*val).or_default().insert(pos);
    }

    // split regions names into islands
    let mut islands = HashMap::<(char, IVec2), Island>::new();
    for (region_name, region) in &mut region_types {
        while !region.is_empty() {
            let start = region.iter().min_by_key(|v| v.to_array()).copied().unwrap();
            let mut island = Island::default();
            let mut contiguous_front = HashSet::from([start]);
            while let Some(&coord) = contiguous_front.iter().next() {
                contiguous_front.remove(&coord);
                region.remove(&coord);
                island.cells.insert(coord);
                for (n_coord, n_name) in g.cross_neighbours(coord) {
                    if !island.cells.contains(&n_coord) {
                        if Some(region_name) == n_name {
                            contiguous_front.insert(n_coord);
                        } else {
                            island.perimeter += 1;
                        }
                    }
                }
            }
            assert!(islands.insert((*region_name, start), island).is_none());
        }
    }

    dbg!(islands
        .values()
        .map(|i| i.perimeter * i.cells.len())
        .sum::<usize>());

    for ((island_name, _start), island) in islands.iter_mut() {
        let mut edges = HashSet::new();
        for point in &island.cells {
            for (n_point, n_name) in g.cross_neighbours(*point) {
                if n_name != Some(island_name) {
                    edges.insert((
                        *point,
                        if n_point.x == point.x {
                            if n_point.y > point.y {
                                Dir::Down
                            } else {
                                Dir::Up
                            }
                        } else {
                            if n_point.x > point.x {
                                Dir::Right
                            } else {
                                Dir::Left
                            }
                        },
                    ));
                }
            }
        }

        let mut sides_count = 0;
        while let Some((p, dir)) = edges.iter().next().copied() {
            sides_count += 1;
            let dir_v_pos = match dir {
                Dir::Up | Dir::Down => IVec2::new(1, 0),
                Dir::Left | Dir::Right => IVec2::new(0, 1),
            };
            edges.remove(&(p, dir));
            for dir_v in [dir_v_pos, -dir_v_pos] {
                let mut cursor = p;
                while edges.contains(&(cursor + dir_v, dir)) {
                    edges.remove(&(cursor + dir_v, dir));
                    cursor += dir_v;
                }
            }
        }

        island.sides_count = Some(sides_count);
    }

    dbg!(islands
        .values()
        .map(|i| i.cells.len() * i.sides_count.unwrap())
        .sum::<usize>());
}
