use std::collections::HashSet;

use glam::IVec2;

fn main() {
    let input = std::fs::read_to_string("day03_ex.txt").unwrap();
    let mut visited = HashSet::new();
    let mut pos = IVec2::new(0, 0);
    let mut pos2 = IVec2::new(0, 0);
    visited.insert(pos);
    let mut is_robo = false;
    for c in input.trim().chars() {
        let d: IVec2 = match c {
            '^' => [0, 1],
            'v' => [0, -1],
            '>' => [1, 0],
            '<' => [-1, 0],
            c => panic!("invalid char {c}"),
        }
        .into();
        let p = if is_robo { &mut pos } else { &mut pos2 };
        is_robo = !is_robo;
        *p += d;
        visited.insert(pos);
        visited.insert(pos2);
    }
    dbg!(visited.len());
}
