use std::collections::{BTreeMap, BTreeSet, HashSet};

use glam::{IVec2, UVec2};

fn main() {
    let grid: Vec<Vec<u8>> = std::fs::read_to_string("../day10_ex.txt")
        .unwrap()
        .lines()
        .map(|l| l.chars().map(|c| c.to_digit(10).unwrap() as u8).collect())
        .collect();
    let mut res1 = 0;
    for (y, line) in grid.iter().enumerate() {
        for (x, v) in line.iter().enumerate() {
            if *v == 0 {
                res1 += find_nine(&grid, UVec2::new(x as u32, y as u32), 1).len();
            }
        }
    }
    dbg!(res1);

    let mut res2 = 0;
    for (y, line) in grid.iter().enumerate() {
        for (x, v) in line.iter().enumerate() {
            if *v == 0 {
                res2 += find_paths_to_nine(
                    &grid,
                    UVec2::new(x as u32, y as u32),
                    1,
                    BTreeSet::from_iter(Some([x as u32, y as u32])),
                )
                .len();
            }
        }
    }

    dbg!(res2);
}

fn find_nine(grid: &[Vec<u8>], start: UVec2, step: u8) -> HashSet<UVec2> {
    let mut result = HashSet::new();
    for dc in [[0, 1], [0, -1], [1, 0], [-1, 0]].map(IVec2::from) {
        let new_coord = start.wrapping_add_signed(dc);
        if let Some(val) = grid
            .get(new_coord.y as usize)
            .and_then(|row| row.get(new_coord.x as usize))
        {
            if *val == step {
                if *val == 9 {
                    result.insert(new_coord);
                } else {
                    result.extend(find_nine(grid, new_coord, step + 1));
                }
            }
        }
    }
    result
}

fn find_paths_to_nine(
    grid: &[Vec<u8>],
    start: UVec2,
    step: u8,
    current_path_so_far: BTreeSet<[u32; 2]>,
) -> BTreeSet<BTreeSet<[u32; 2]>> {
    let mut result = BTreeSet::new();
    for dc in [[0, 1], [0, -1], [1, 0], [-1, 0]].map(IVec2::from) {
        let new_coord = start.wrapping_add_signed(dc);
        if let Some(val) = grid
            .get(new_coord.y as usize)
            .and_then(|row| row.get(new_coord.x as usize))
        {
            if *val == step {
                let mut new_path = current_path_so_far.clone();
                new_path.insert(new_coord.into());
                if *val == 9 {
                    result.insert(new_path);
                } else {
                    result.extend(find_paths_to_nine(grid, new_coord, step + 1, new_path));
                }
            }
        }
    }
    result
}
