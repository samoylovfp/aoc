use std::collections::{
    HashMap,
    HashSet
};

#[derive(Debug)]
struct OrbitObject {
    name: String,
    children: Vec<String>
}

#[derive(Debug)]
struct OrbitTree {
    name_to_node: HashMap<String, OrbitObject>
}

#[derive(Clone)]
struct TreePath(Vec<String>);

impl TreePath {
    fn depth(&self) -> usize {
        self.0.len()
    }
}

impl std::ops::Add for TreePath {
    type Output = TreePath;
    fn add(mut self, rhs: TreePath) -> TreePath {
        self.0.extend(rhs.0);
        self
    }
}

impl Into<HashSet<String>> for TreePath {
    fn into(self) -> HashSet<String>{
        self.0.into_iter().collect()
    }
}

impl OrbitTree {
    fn from(input: &str) -> OrbitTree {
        let mut tree = OrbitTree {
            name_to_node: HashMap::new()
        };
        for pair in input.trim().split('\n') {
            let split = pair.trim().split(')').collect::<Vec<&str>>();
            let parent = tree.get_node(split[0]);
            parent.children.push(split[1].to_string());
        }
        tree
    }

    fn get_node(&mut self, name: &str) -> &mut OrbitObject {
        let name = name.to_string();
        self.name_to_node.entry(name.clone()).or_insert(OrbitObject {
            name: name,
            children: vec![]
        })
    }

    fn get_node_children_sum_length(
        &self,
        child_name: &str,
        current_length: u32
    )-> u32 {
        // if node is not in the map 
        // then this node is a leaf, just return current_length for it
        // if node is there, then add lengths of all the children

        current_length + self.name_to_node.get(child_name).map(
            |n|
            n.children
            .iter()
            .map(|c|
                self.get_node_children_sum_length(c, current_length + 1)
            ).sum()
        ).unwrap_or(0)
    }

    fn get_path_to(&self, current_node: &str, target: &str) -> TreePath {
        if current_node == target {
            return TreePath(vec![current_node.to_string()])
        } 
        if let Some(n) = self.name_to_node.get(current_node) {
            for c in n.children.clone() {
                let p = self.get_path_to(&c, target);
                if p.depth() != 0 {
                    return TreePath(vec![current_node.to_string()]) + p
                }
            }
        }
        return TreePath(vec![])
    }
}


fn total_orbits(setup: &str) -> u32 {
    let tree = OrbitTree::from(setup);
    tree.get_node_children_sum_length("COM", 0)
}


fn distance(setup: &str, n1: &str, n2: &str) -> usize {
    // find paths to both nodes
    // turn one path into a set
    // iterate over the second path end-to-start
    // until the node is in the first path
    // return the sum from common ancestor to both nodes
    let tree = OrbitTree::from(setup);
    let p1 = tree.get_path_to("COM", n1);
    let p2 = tree.get_path_to("COM", n2);
    let common_path = p1.0
        .iter()
        .zip(p2.0.iter())
        .take_while(|(n1, n2)| n1 == n2).count();
    dbg!(common_path);
    (p1.depth() - common_path) + (p2.depth() - common_path) - 2
}


fn main() {
    let tree_setup = std::fs::read_to_string("input.txt")
        .expect("input file");

    println!("First part is {}", total_orbits(
        &tree_setup
    ));
    println!("Second part is {}", distance(
        &tree_setup, "YOU", "SAN"))
}


mod test{
    use crate::*;
    #[test]
    fn test_simple() {
        assert_eq!(total_orbits(r#"
        COM)A
        "#), 1);
        assert_eq!(total_orbits(r#"
        COM)A
        A)B
        "#), 3);
    }


    #[test]
    fn test_provided_setup() {
        assert_eq!(total_orbits(r#"
        COM)B
        B)C
        C)D
        D)E
        E)F
        B)G
        G)H
        D)I
        E)J
        J)K
        K)L
        "#), 42);
    }

    #[test]
    fn test_provided_second_part() {
        assert_eq!(distance(r#"
            COM)B
            B)C
            C)D
            D)E
            E)F
            B)G
            G)H
            D)I
            E)J
            J)K
            K)L
            K)YOU
            I)SAN
        "#, "YOU", "SAN"), 4);
    }
}