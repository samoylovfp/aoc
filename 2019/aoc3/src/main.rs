#![allow(dead_code)]
use std::collections::{HashMap, HashSet};

type RenderedWire = HashMap<Coord, u32>;

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
struct Coord {
    x: i32,
    y: i32
}

impl Coord {
    fn manhattan(self) -> u32 {
        (self.x.abs() + self.y.abs()) as u32
    }
}

fn get_rendered_wire(instructions: Vec<String>) -> RenderedWire {
    let mut result = RenderedWire::new();
    let mut current = Coord {x: 0, y: 0};
    let mut wire_length = 0;
    instructions.iter().for_each(|ins|{
        let direction = ins.chars().next().expect("direction in instruction");
        let length = ins.chars().skip(1).collect::<String>().parse::<u32>().expect("number of steps");
        for _s in 0..length {
            match direction {
                'U' => current.y += 1,
                'D' => current.y -= 1,
                'L' => current.x -= 1,
                'R' => current.x += 1,
                d => panic!("Unknown direction {}", d)
            }
            wire_length += 1;
            result.insert(current, wire_length);
        }
    });
    result
}

fn get_closest_intersection(wire1: Vec<String>, wire2: Vec<String>) -> Coord {
    let rendered1 = get_rendered_wire(wire1);
    let rendered2 = get_rendered_wire(wire2);
    let rendered1_keys = rendered1.keys().copied().collect::<HashSet<_>>();
    let rendered2_keys = rendered2.keys().copied().collect::<HashSet<_>>();
    let intersection = rendered1_keys.intersection(&rendered2_keys);

    *intersection.min_by_key(|c|c.manhattan()).expect("an intersection")
}

fn get_closest_by_wire(wire1: Vec<String>, wire2: Vec<String>) -> u32 {
    let rendered1 = get_rendered_wire(wire1);
    let rendered2 = get_rendered_wire(wire2);
    let rendered1_keys = rendered1.keys().copied().collect::<HashSet<_>>();
    let rendered2_keys = rendered2.keys().copied().collect::<HashSet<_>>();
    let intersection = rendered1_keys.intersection(&rendered2_keys);

    intersection.map(
        |c| rendered1[c] + rendered2[c]
    ).min().expect("an intersection")
}

fn parse_instr(i: impl Into<String>) -> Vec<String> {
    i.into().split(",").map(Into::into).collect()
}

fn main() {
    let data = std::fs::read_to_string("input.txt").expect("input file");
    let lines = data.lines().collect::<Vec<_>>();

    println!("First part is {}", get_closest_intersection(
        parse_instr(lines[0]),
        parse_instr(lines[1])
    ).manhattan());

    println!("Second part is {}", get_closest_by_wire(
        parse_instr(lines[0]),
        parse_instr(lines[1])
    ));    
}


#[test]
fn test_provided() {
    assert_eq!(
        get_closest_intersection(
            parse_instr("R75,D30,R83,U83,L12,D49,R71,U7,L72"),
            parse_instr("U62,R66,U55,R34,D71,R55,D58,R83"),
        ).manhattan(),
        159
    )
}