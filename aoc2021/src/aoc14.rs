use std::{collections::HashMap, fs::read_to_string, mem::swap};

fn main() {
    let input = read_to_string("input14.txt").unwrap();
    let (starting_state, instructions) = input.split_once("\n\n").unwrap();
    let instructions: HashMap<(char, char), char> = instructions
        .trim()
        .lines()
        .map(|l| l.trim().split_once(" -> ").unwrap())
        .map(|(k, v)| {
            let mut chars = k.chars();
            (
                (chars.next().unwrap(), chars.next().unwrap()),
                v.chars().next().unwrap(),
            )
        })
        .collect();

    let starting_state: Vec<char> = starting_state.chars().collect();
    let mut state: HashMap<(char, char), u128> = HashMap::new();
    let mut char_counts: HashMap<char, u128> = HashMap::new();
    for char_pair in starting_state.windows(2) {
        *state.entry((char_pair[0], char_pair[1])).or_default() += 1;
    }
    for char in starting_state {
        *char_counts.entry(char).or_default() += 1;
    }

    for step in 1..=40 {
        let mut new_state = HashMap::new();
        for (pair, num) in state.iter() {
            let new_char = *instructions.get(pair).unwrap();
            *char_counts.entry(new_char).or_default() += num;

            for new_pair in [(pair.0, new_char), (new_char, pair.1)] {
                (*new_state.entry(new_pair).or_default()) += num;
            }
        }
        swap(&mut new_state, &mut state);

        // println!("{}", step);
        // println!("{:?}", state);
        // println!("{:?}", char_counts);
        if step == 10 {
            println!(
                "First part is {}",
                char_counts.values().max().unwrap() - char_counts.values().min().unwrap()
            );
        }
    }
    println!(
        "Second part is {}",
        char_counts.values().max().unwrap() - char_counts.values().min().unwrap()
    );
}
