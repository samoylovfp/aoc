use std::{cmp::max, collections::HashSet, fs::read_to_string, ops::RangeInclusive};

use itertools::Itertools;

#[derive(Debug)]
struct Sensor {
    center: (i32, i32),
    beacon: (i32, i32),
    radius: u32,
}

fn main() {
    let input = read_to_string("sample15.txt").unwrap();
    let sample_sensors = input.lines().map(parse_sensor).collect_vec();
    let input = read_to_string("input15.txt").unwrap();
    let actual_sensors = input.lines().map(parse_sensor).collect_vec();
    dbg!(part1(&sample_sensors, 10));
    dbg!(part1(&actual_sensors, 2000000));

    dbg!(part2(&sample_sensors, 0..=20));
    dbg!(part2(&actual_sensors, 0..=4_000_000));
}

fn part2(sensors: &[Sensor], coord_range: RangeInclusive<i32>) -> i64 {
    'y: for y in coord_range.clone() {
        let coverage = calculate_coverage(sensors, y);
        let mut left_limit = *coord_range.start() - 1;
        for cov in coverage {
            if *cov.start() > left_limit + 1 {
                return (left_limit as i64 + 1) * 4000000 + y as i64;
            }
            left_limit = left_limit.max(*cov.end());
            if left_limit > *coord_range.end() {
                continue 'y;
            }
        }
    }

    panic!("Not found");
}

fn part1(sensors: &[Sensor], y: i32) -> usize {
    let beacons: HashSet<(i32, i32)> = sensors
        .iter()
        .map(|s| s.beacon)
        .filter(|b| b.1 == y)
        .collect();

    let coverages = calculate_coverage(sensors, y);
    let mut coverage_total = 0;
    let mut last_end = None;
    for cov in coverages {
        let mut unoverlapped_coverage = cov.clone();

        if let Some(last_end) = last_end {
            let min_new_start = last_end + 1;
            unoverlapped_coverage =
                max(min_new_start, *unoverlapped_coverage.start())..=*unoverlapped_coverage.end();
        }
        last_end = max(last_end, Some(*unoverlapped_coverage.end()));
        coverage_total += unoverlapped_coverage.clone().count()
            - beacons
                .iter()
                .filter(|b| unoverlapped_coverage.contains(&b.0))
                .count();
    }

    coverage_total as usize
}

fn calculate_coverage(sensors: &[Sensor], y: i32) -> Vec<RangeInclusive<i32>> {
    let mut coverage_intervals = Vec::with_capacity(sensors.len());
    for sensors in sensors {
        let distance_to_center = sensors.center.1.abs_diff(y);
        if distance_to_center <= sensors.radius {
            let overlap = sensors.radius - distance_to_center;
            let range =
                (sensors.center.0 - (overlap as i32))..=(sensors.center.0 + (overlap as i32));
            coverage_intervals.push(range);
        }
    }
    coverage_intervals.sort_by_key(|r| *r.start());
    coverage_intervals
}

fn parse_sensor(l: &str) -> Sensor {
    fn parse_coords(c: &str) -> (i32, i32) {
        let (x, y) = c.split_once(", ").unwrap();
        let x = x.strip_prefix("x=").unwrap().parse().unwrap();
        let y = y.strip_prefix("y=").unwrap().parse().unwrap();
        (x, y)
    }
    let (sensor_center, beacon) = l.split_once(": closest beacon is at ").unwrap();
    let sensor_center = parse_coords(sensor_center.strip_prefix("Sensor at ").unwrap());
    let beacon = parse_coords(beacon);

    Sensor {
        center: sensor_center,
        beacon,
        radius: distance(sensor_center, beacon),
    }
}

fn distance(sensor_center: (i32, i32), beacon: (i32, i32)) -> u32 {
    sensor_center.0.abs_diff(beacon.0) + sensor_center.1.abs_diff(beacon.1)
}
