use std::{collections::VecDeque, fs::read_to_string};

use itertools::Itertools;
use nom::{character::complete::multispace1, multi::separated_list1, Finish};

fn main() {
    let input = read_to_string("input11.txt").unwrap();
    let (_i, mut monkeys) = separated_list1(multispace1, parse_monkey)(&input)
        .finish()
        .unwrap();
    let common_multiplier: u64 = monkeys.iter().map(|m| m.test_num).product();

    for _round in 0..10000 {
        for monkey_i in 0..monkeys.len() {
            loop {
                let (item, target_monkey) = {
                    let monkey = monkeys.get_mut(monkey_i).unwrap();

                    let Some(item) = monkey.items.pop_front() else {break};
                    let item = monkey.operation.apply(item) % common_multiplier;
                    monkey.inspects += 1;
                    let target = if (item % monkey.test_num) == 0 {
                        monkey.if_true
                    } else {
                        monkey.if_false
                    };
                    (item, target)
                };
                monkeys
                    .get_mut(target_monkey)
                    .unwrap()
                    .items
                    .push_back(item);
            }
        }
    }

    let part1: usize = monkeys
        .iter()
        .map(|m| m.inspects)
        .sorted()
        .rev()
        .take(2)
        .product();
    dbg!(part1);
}

#[derive(Debug)]
struct MonkeyInstructions {
    items: VecDeque<u64>,
    operation: Op,
    test_num: u64,
    if_true: usize,
    if_false: usize,
    inspects: usize,
}

#[derive(Debug)]
enum Op {
    Mul(u64),
    Add(u64),
    MulOld,
}
impl Op {
    fn apply(&self, worry: u64) -> u64 {
        match self {
            Op::Mul(n) => worry * n,
            Op::Add(n) => worry + n,
            Op::MulOld => worry * worry,
        }
    }
}

fn parse_monkey(i: &str) -> nom::IResult<&str, MonkeyInstructions> {
    use nom::{
        branch::alt,
        bytes::complete::{tag, take_while},
        character::complete::digit1,
        combinator::map,
        sequence::tuple,
    };

    let digit_str = take_while(|c: char| c.is_ascii_digit());
    let digit = map(&digit_str, |s: &str| s.parse::<u64>().unwrap());

    let (i, _) = tag("Monkey ")(i)?;
    let (i, _monkey_num) = digit1(i)?;
    let (i, _) = tag(":")(i)?;

    let (i, _) = tuple((multispace1, tag("Starting items: ")))(i)?;
    let (i, starting_items) = separated_list1(tag(", "), digit)(i)?;

    let (i, _) = tuple((multispace1, tag("Operation: new = old ")))(i)?;
    let (i, op_type) = alt((tag("*"), tag("+")))(i)?;
    let (i, _) = multispace1(i)?;
    let (i, op_rhs) = alt((tag("old"), &digit_str))(i)?;

    let (i, _) = tuple((multispace1, tag("Test: divisible by ")))(i)?;
    let (i, test) = digit_str(i)?;

    let (i, _) = tuple((multispace1, tag("If true: throw to monkey ")))(i)?;
    let (i, if_true) = digit_str(i)?;

    let (i, _) = tuple((multispace1, tag("If false: throw to monkey ")))(i)?;
    let (i, if_false) = digit_str(i)?;

    let op = match op_type {
        "*" => match op_rhs {
            "old" => Op::MulOld,
            _ => Op::Mul(op_rhs.parse().unwrap()),
        },
        "+" => Op::Add(op_rhs.parse().unwrap()),
        _ => panic!(),
    };

    Ok((
        i,
        MonkeyInstructions {
            items: starting_items.into(),
            operation: op,
            test_num: test.parse().unwrap(),
            if_true: if_true.parse().unwrap(),
            if_false: if_false.parse().unwrap(),
            inspects: 0,
        },
    ))
}
