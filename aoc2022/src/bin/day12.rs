use std::{
    collections::{HashMap, HashSet},
    fs::read_to_string,
};

use itertools::Itertools;
use ndarray::{Array2, Axis};

#[derive(Debug, Clone, Copy)]
enum Cell {
    Start,
    End,
    Height(u8),
}

impl Cell {
    fn height(&self) -> u8 {
        match self {
            Cell::Start => 0,
            Cell::End => 'z' as u8 - 'a' as u8,
            Cell::Height(h) => *h,
        }
    }
}

fn main() -> anyhow::Result<()> {
    let input = read_to_string("input12.txt")?;
    // order: y, x
    let field = input
        .trim()
        .lines()
        .map(|l| {
            l.chars()
                .map(|c| match c {
                    'S' => Cell::Start,
                    'E' => Cell::End,
                    _ => Cell::Height(c as u8 - 'a' as u8),
                })
                .collect_vec()
        })
        .collect_vec();
    let field = Array2::from_shape_fn((field[0].len(), field.len()), |(x, y)| field[y][x]);

    dbg!(part1(&field));
    dbg!(part2(&field));

    Ok(())
}

fn find_shortest_route_from(start: (usize, usize), field: &Array2<Cell>) -> u32 {
    let end = field
        .indexed_iter()
        .find(|(_idx, cell)| matches!(cell, Cell::End))
        .unwrap()
        .0;

    let mut shortest_paths = HashMap::new();
    let mut current_path = 0;
    let mut path_front = HashSet::from([start]);
    let mut next_path_front = HashSet::new();
    const NEIGHBOURS: &[(isize, isize)] = &[(0, 1), (0, -1), (1, 0), (-1, 0)];

    loop {
        for coord in path_front.drain() {
            let Some(current_cell) = field.get(coord) else {
                continue;
            };
            for (dx, dy) in NEIGHBOURS {
                // FIXME: there should be a better way to do this
                let neighbour_coords = (
                    (coord.0 as isize + dx) as usize,
                    (coord.1 as isize + dy) as usize,
                );

                // No nehgbour
                let Some(neighbor) = field
                    .get(neighbour_coords)
                     else {
                        continue;
                    };

                // Cannot access
                if neighbor.height() > current_cell.height() + 1 {
                    continue;
                }
                let path_to_neighbour = current_path + 1;

                let shorter_route_is_known = matches!(shortest_paths.get(&neighbour_coords), Some(path) if *path < path_to_neighbour);
                // There is a shorter route to this neighbour
                if !shorter_route_is_known {
                    shortest_paths.insert(neighbour_coords, path_to_neighbour);
                    next_path_front.insert(neighbour_coords);
                }
            }
        }
        if next_path_front.is_empty() {
            break;
        }
        std::mem::swap(&mut path_front, &mut next_path_front);
        current_path += 1;
    }

    shortest_paths.get(&end).copied().unwrap_or(u32::MAX)
}

fn part1(field: &Array2<Cell>) -> u32 {
    let start = field
        .indexed_iter()
        .find(|(_idx, cell)| matches!(cell, Cell::Start))
        .unwrap()
        .0;
    find_shortest_route_from(start, &field)
}

fn part2(field: &Array2<Cell>) -> u32 {
    field
        .indexed_iter()
        .filter(|(_coords, cell)| matches!(cell, Cell::Start) || matches!(cell, Cell::Height(0)))
        .map(|(coords, _cell)| find_shortest_route_from(coords, &field))
        .min()
        .unwrap()
}
