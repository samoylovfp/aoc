use std::{fs::read_to_string, time::Instant};

use rand::{distributions::WeightedIndex, prelude::*, SeedableRng};

fn main() {
    let input = read_to_string("input15.txt").unwrap();
    let cave = parse_cave(&input);
    dbg!(find_shortest(&cave, 50_000_000, 0));
}

fn parse_cave(input: &str) -> Vec<Vec<u8>> {
    input
        .trim()
        .lines()
        .map(|l| {
            l.trim()
                .chars()
                .map(|c| c.to_digit(10).unwrap() as u8)
                .collect()
        })
        .collect()
}

type Cell = (i32, i32);

const COST_DIFF_BASE: f32 = 1.1;

const MIN_INTEREST: f32 = 0.02;
const ROUTE_COST_INTEREST: f32 = 5.0;
const CELL_COST_INTEREST: f32 = 0.1;
const GOAL_INTEREST: f32 = 0.3;

fn find_shortest(cave: &[Vec<u8>], iterations: usize, seed: u64) -> usize {
    let height = cave.len();
    let width = cave[0].len();
    let max_y = (height - 1) as i32;
    let max_x = (width - 1) as i32;

    let goal = (max_x, max_y);
    let mut smallest_route_cost_through_cell = vec![vec![usize::MAX; cave.len()]; cave[0].len()];
    let mut smallest_route_cost: Option<usize> = None;
    // let mut shortest_route = vec![];
    let mut rng = StdRng::seed_from_u64(seed);
    let get_cell_cost = |c: Cell| cave[c.1 as usize][c.0 as usize];
    let mut time = Instant::now();
    let mut dead_ends = 0;
    let mut finishes = 0;

    for ant in 0..iterations {
        if time.elapsed().as_millis() > 1000 {
            println!(
                "{:.0}%, smallest_route: {:?}, dead_ends: {}, finishes: {}",
                ant as f32 / iterations as f32 * 100.0,
                smallest_route_cost,
                dead_ends,
                finishes
            );
            print_weights(&smallest_route_cost_through_cell);
            time = Instant::now();
            dead_ends = 0;
            finishes = 0;
        }

        let increasing_interest = ant as f32 / iterations as f32;
        let reducing_interest = 1.0 - increasing_interest;

        let mut visited_cells = vec![false; cave.len() * cave[0].len()];
        let mut route_cost = 0;
        let mut ant_position: Cell = (0, 0);
        // let mut route = vec![ant_position];
        visited_cells[0] = true;

        loop {
            let places_to_go = [(-1, 0), (1, 0), (0, -1), (0, 1)]
                .into_iter()
                .map(|(x, y): (i32, i32)| (ant_position.0 + x, ant_position.1 + y))
                // Is within board
                .filter(|(x, y)| (0..=max_x).contains(x) && (0..=max_y).contains(y))
                // Is not yet visited
                .filter(|&(x, y)| !visited_cells[y as usize * width + x as usize])
                // add score
                .map(|(x, y)| (x, y))
                .collect::<Vec<Cell>>();

            if places_to_go.is_empty() {
                // Dead-end
                dead_ends += 1;
                break;
            }

            let supposed_cost = smallest_route_cost_through_cell[ant_position.1 as usize]
                [ant_position.0 as usize] as f32;

            if route_cost as f32 > supposed_cost * 1.5 {
                dead_ends += 1;
                break;
            }

            let current_distance = ((goal.0 - ant_position.0).pow(2) as f32
                + (goal.1 - ant_position.1).pow(2) as f32)
                .sqrt();

            let weights: Vec<f32> = places_to_go
                .iter()
                .map(|&(x, y)| {
                    let new_distance = (((goal.0 - x).pow(2) + (goal.1 - y).pow(2)) as f32).sqrt();
                    let distance_change = current_distance - new_distance;
                    let smallest_cell_cost =
                        smallest_route_cost_through_cell[y as usize][x as usize] as f32;
                    let smallest_route_cost = smallest_route_cost.unwrap_or(2000) as f32;
                    let additional_cost = smallest_route_cost - smallest_cell_cost;

                    MIN_INTEREST
                        + ROUTE_COST_INTEREST * COST_DIFF_BASE.powf(-additional_cost).min(0.0)
                        + GOAL_INTEREST * reducing_interest * (1.0 + distance_change)
                        + CELL_COST_INTEREST * (9.0 - get_cell_cost((x, y)) as f32) / 9.0
                })
                .collect();
            // println!("{:?}", &places_to_go);
            // println!("{:?}", &weights);
            let dist = WeightedIndex::new(&weights).unwrap();
            let next_step = places_to_go[dist.sample(&mut rng)];

            ant_position = next_step;
            visited_cells[ant_position.1 as usize * width + ant_position.0 as usize] = true;
            route_cost += get_cell_cost(ant_position) as usize;

            if ant_position == goal {
                finishes += 1;
                if smallest_route_cost.is_none()
                    || matches!(smallest_route_cost, Some(p) if route_cost < p )
                {
                    smallest_route_cost = Some(route_cost);
                    // shortest_route = route;
                }

                visited_cells
                    .iter()
                    .enumerate()
                    .filter(|&(_i, v)| *v)
                    .for_each(|(i, _v)| {
                        let x = i % width;
                        let y = i / width;

                        smallest_route_cost_through_cell[y][x] =
                            route_cost.min(smallest_route_cost_through_cell[y][x])
                    });

                smallest_route_cost =
                    smallest_route_cost.map_or(Some(route_cost), |c| Some(c.min(route_cost)));
                break;
            }
        }
    }
    // print_route(&shortest_route);
    print_weights(&smallest_route_cost_through_cell);
    smallest_route_cost.unwrap()
}

// fn get_avg_cost(map: &[Vec<usize>], point: (i32, i32)) -> f32 {
//     let (sx, sy) = (point.0 as usize, point.1 as usize);
//     let iter = (sx.saturating_sub(SEARCH_RADIUS)..=(sx + SEARCH_RADIUS)).flat_map(|x| {
//         (sy.saturating_sub(SEARCH_RADIUS)..=(sy + SEARCH_RADIUS))
//             .filter_map(move |y| map.get(y).and_then(|row| row.get(x).copied()))
//     });
//     let mode = "avg";
//     match mode {
//         "max" => iter.max().unwrap() as f32,
//         "avg" => iter.clone().sum::<usize>() as f32 / iter.count() as f32,
//         _ => panic!(),
//     }
// }

const GREYSCALE: &str = " .:-=+*#%@";

#[allow(clippy::needless_range_loop)]
fn print_weights(costs: &[Vec<usize>]) {
    let max_greyscale = GREYSCALE.chars().count() - 1;
    let min_value = *costs.iter().map(|l| l.iter().min().unwrap()).min().unwrap();
    let max_x = costs[0].len();
    let max_y = costs.len();
    for y in 0..max_y {
        for x in 0..max_x {
            let char_index = (costs[y][x] - min_value) / 5;
            print!(
                "{}",
                GREYSCALE
                    .chars()
                    .rev()
                    .nth(char_index.min(max_greyscale))
                    .unwrap()
            )
        }
        println!();
    }
}

// fn max_pheromone_value(pheromones: &[Vec<f32>]) -> f32 {
//     let max_value = pheromones
//         .iter()
//         .flat_map(|l| l.iter())
//         .map(|&v| (v * 1000.0) as u32)
//         .max()
//         .unwrap() as f32
//         / 1000.0;
//     max_value
// }

// fn print_route(route: &[Cell]) {
//     let max_x = *route.iter().map(|(x, _y)| x).max().unwrap();
//     let max_y = *route.iter().map(|(_x, y)| y).max().unwrap();
//     for y in 0..=max_y {
//         for x in 0..=max_x {
//             if route.contains(&(x, y)) {
//                 print!("#")
//             } else {
//                 print!(".")
//             }
//         }
//         println!()
//     }
// }

#[test]
fn test_provided() {
    let cave = parse_cave(
        r#"
        1163751742
        1381373672
        2136511328
        3694931569
        7463417111
        1319128137
        1359912421
        3125421639
        1293138521
        2311944581    
    "#,
    );
    let runs = 5;
    let scores: Vec<usize> = (0..runs)
        .map(|seed| find_shortest(&cave, 100_000, seed))
        .collect();
    assert_eq!(scores, vec![40; runs as usize]);
}
