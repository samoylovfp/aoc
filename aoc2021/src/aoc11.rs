use std::{
    collections::{HashMap, HashSet},
    fs::read_to_string,
    ops::Add,
    sync::mpsc::channel,
};

fn main() {
    let mut cavern = Cavern::parse(&read_to_string("input11.txt").unwrap(), 10);
    let first_part: usize = (0..100).map(|_| cavern.step()).sum();
    dbg!(first_part);

    let mut cavern = Cavern::parse(&read_to_string("input11.txt").unwrap(), 10);
    let second_part = (1..).find(|_| cavern.step() == 100).unwrap();
    dbg!(second_part);
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct Coord(i8, i8);

#[derive(Debug, Clone, PartialEq)]
struct Octopus {
    energy: u8,
}

impl Octopus {
    fn energize(&mut self) {
        self.energy = self.energy.saturating_add(1);
    }

    fn is_glowing(&self) -> bool {
        self.energy > 9
    }

    fn reset(&mut self) {
        if self.is_glowing() {
            self.energy = 0;
        }
    }
}

impl Add<(i8, i8)> for &Coord {
    type Output = Coord;

    fn add(self, (x, y): (i8, i8)) -> Self::Output {
        Coord(self.0 + x, self.1 + y)
    }
}

#[rustfmt::skip]
const NEIGHBOURS: [(i8, i8); 8] = [
    (-1, -1), (0, -1), (1, -1),
    (-1,  0),          (1,  0),
    (-1,  1), (0,  1), (1,  1)
];

#[derive(Debug, Clone, PartialEq)]
struct Cavern {
    map: HashMap<Coord, Octopus>,
    size: usize,
}

impl Cavern {
    pub(crate) fn parse(arg: &str, size: usize) -> Self {
        let map = arg
            .trim()
            .lines()
            .enumerate()
            .flat_map(|(y, line)| {
                line.trim().chars().enumerate().map(move |(x, char)| {
                    (
                        Coord(x as i8, y as i8),
                        Octopus {
                            energy: char.to_digit(10).unwrap() as u8,
                        },
                    )
                })
            })
            .collect();

        Cavern { map, size }
    }

    fn step(&mut self) -> usize {
        let mut flashed = HashSet::new();
        let (energizer_tx, energizer_rx) = channel();

        self.map.keys().for_each(|coord| {
            energizer_tx.send(coord.clone()).unwrap();
        });

        while let Ok(c) = energizer_rx.try_recv() {
            if flashed.contains(&c) {
                continue;
            }
            let octopus = self.map.get_mut(&c).unwrap();
            octopus.energize();
            if octopus.is_glowing() {
                flashed.insert(c.clone());
                self.get_neighbours(&c)
                    .into_iter()
                    .for_each(|n| energizer_tx.send(n).unwrap())
            }
        }

        self.map.values_mut().for_each(|v| {
            v.reset();
        });
        flashed.len()
    }

    fn get_neighbours(&self, coord: &Coord) -> Vec<Coord> {
        NEIGHBOURS
            .into_iter()
            .map(|(dx, dy)| coord + (dx, dy))
            .filter(|c| self.map.contains_key(c))
            .collect()
    }
}

#[test]
fn test_provided() {
    let mut cavern = Cavern::parse(
        r#"
        5483143223
        2745854711
        5264556173
        6141336146
        6357385478
        4167524645
        2176841721
        6882881134
        4846848554
        5283751526
    "#,
        10,
    );
    cavern.step();

    assert_eq!(
        &cavern,
        &Cavern::parse(
            r#"
            6594254334
            3856965822
            6375667284
            7252447257
            7468496589
            5278635756
            3287952832
            7993992245
            5957959665
            6394862637

        "#,
            10
        )
    );

    cavern.step();

    assert_eq!(
        &cavern,
        &Cavern::parse(
            r#"
            8807476555
            5089087054
            8597889608
            8485769600
            8700908800
            6600088989
            6800005943
            0000007456
            9000000876
            8700006848
        "#,
            10
        )
    );
}

#[test]
fn test_neighbours() {
    let c = Cavern::parse(
        r#"
    12345
    12345
    12345
    12345
    12345
    "#,
        5,
    );
    let n = c.get_neighbours(&Coord(3, 3));
    assert_eq!(
        n,
        vec![
            Coord(2, 2),
            Coord(3, 2),
            Coord(4, 2),
            Coord(2, 3),
            Coord(4, 3),
            Coord(2, 4),
            Coord(3, 4),
            Coord(4, 4)
        ]
    );

    let n = c.get_neighbours(&Coord(0, 3));
    assert_eq!(
        n,
        vec![
            Coord(0, 2),
            Coord(1, 2),
            Coord(1, 3),
            Coord(0, 4),
            Coord(1, 4)
        ]
    );
    let n = c.get_neighbours(&Coord(4, 3));
    assert_eq!(
        n,
        vec![
            Coord(3, 2),
            Coord(4, 2),
            Coord(3, 3),
            Coord(3, 4),
            Coord(4, 4)
        ]
    )
}
