fn main() {
    let input = std::fs::read_to_string("day05_full.txt").unwrap();

    dbg!(input.trim().lines().filter(|l| is_nice(l)).count());
    dbg!(input.trim().lines().filter(|l| is_nice2(l)).count());
}

fn is_nice(l: &str) -> bool {
    let l_chars: Vec<char> = l.chars().collect();

    const VOWELS: &str = "aeiou";
    let vowel_count = l_chars.iter().filter(|&&c| VOWELS.contains(c)).count();
    if vowel_count < 3 {
        return false;
    }

    let has_pair = 'has_pair: {
        for w in l_chars.windows(2) {
            if w[0] == w[1] {
                break 'has_pair true;
            }
        }
        false
    };
    if !has_pair {
        return false;
    }

    const FORBIDDEN: &[&str] = &["ab", "cd", "pq", "xy"];
    if FORBIDDEN.iter().any(|f| l.contains(f)) {
        return false;
    }

    true
}

fn is_nice2(l: &str) -> bool {
    let chars: Vec<char> = l.chars().collect();
    let has_triple = 'has_triple: {
        for w in chars.windows(3) {
            if w[0] == w[2] && w[0] != w[1] {
                break 'has_triple true;
            }
        }
        false
    };
    if !has_triple {
        return false;
    }
    let has_double_double = 'dd: {
        for (i, w) in chars[..chars.len() - 2].windows(2).enumerate() {
            for w2 in chars[i + 2..].windows(2) {
                if w == w2 {
                    break 'dd true;
                }
            }
        }
        false
    };
    has_double_double
}
