use std::fs::read_to_string;

fn main() {
    let input = read_to_string("input10.txt").unwrap();
    dbg!(part_one(&input));
    dbg!(part_two(&input));
}

fn part_two(input: &str) -> u64 {
    let mut scores = vec![];
    for line in input.trim().lines() {
        if let LineStatus::Incomplete(remainder) = validate(line) {
            scores.push(get_score_for_incomplete_line(remainder));
        }
    }
    scores.sort_unstable();
    scores[scores.len() / 2]
}

fn get_score_for_incomplete_line(remainder: Vec<char>) -> u64 {
    let mut score = 0;
    for c in remainder.iter().rev() {
        score *= 5;
        score += match c {
            '(' => 1,
            '[' => 2,
            '{' => 3,
            '<' => 4,
            _ => panic!(),
        }
    }
    score
}

#[derive(Debug)]
enum LineStatus {
    Ok,
    Corrupted(char),
    Extra,
    Incomplete(Vec<char>),
}

fn part_one(input: &str) -> u32 {
    let mut score = 0;
    for line in input.trim().lines() {
        if let LineStatus::Corrupted(c) = validate(line) {
            score += get_score_for_corrupted_line(c)
        }
    }

    score
}

fn get_score_for_corrupted_line(c: char) -> u32 {
    match c {
        ')' => 3,
        ']' => 57,
        '}' => 1197,
        '>' => 25137,
        _ => panic!("Invalid closing char {}", c),
    }
}

fn validate(line: &str) -> LineStatus {
    let mut open_braces_stack = vec![];
    for c in line.trim().chars() {
        match c {
            '<' | '[' | '(' | '{' => open_braces_stack.push(c),
            '>' | ']' | ')' | '}' => match open_braces_stack.pop() {
                None => return LineStatus::Extra,
                Some(opening_c) if !char_matches(opening_c, c) => return LineStatus::Corrupted(c),
                _ => {}
            },
            _ => panic!("Invalid char"),
        }
    }

    if let Some(_c) = open_braces_stack.last() {
        LineStatus::Incomplete(open_braces_stack)
    } else {
        LineStatus::Ok
    }
}

fn char_matches(opening_c: char, closing_c: char) -> bool {
    matches!(
        (opening_c, closing_c),
        ('<', '>') | ('{', '}') | ('[', ']') | ('(', ')')
    )
}

#[test]
fn test_provided() {
    let input = r#"
        [({(<(())[]>[[{[]{<()<>>
        [(()[<>])]({[<{<<[]>>(
        {([(<{}[<>[]}>{[]{[(<()>
        (((({<>}<{<{<>}{[]{[]{}
        [[<[([]))<([[{}[[()]]]
        [{[{({}]{}}([{[{{{}}([]
        {<[[]]>}<{[{[{[]{()[[[]
        [<(<(<(<{}))><([]([]()
        <{([([[(<>()){}]>(<<{{
        <{([{{}}[<[[[<>{}]]]>[]]
    "#;
    assert_eq!(part_one(input), 26397);
    assert_eq!(part_two(input), 288957);
}

#[test]
fn test_validator() {
    let result = validate("[({(<(())[]>[[{[]{<()<>>");
    let missing: Vec<char> = "[({([[{{".chars().collect();
    assert!(
        matches!(result, LineStatus::Incomplete(ref m) if m == &missing),
        "{:?}",
        result
    )
}

#[test]
fn test_incomplete_score() {
    let result = "[({([[{{".chars().collect();
    assert_eq!(get_score_for_incomplete_line(result), 288957);
}
