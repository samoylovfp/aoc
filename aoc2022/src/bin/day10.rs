use std::fs::read_to_string;

fn main() -> anyhow::Result<()> {
    let input = read_to_string("input10.txt")?;
    let mut x = 1;
    let mut cycle_counter = 0;
    let mut part1_result = 0;
    let mut crt = vec![];
    let check_value = |cycle: &i32, reg: &i32, res: &mut i32| match cycle {
        20 | 60 | 100 | 140 | 180 | 220 => *res += cycle * reg,
        _ => {}
    };
    let draw = |reg: &i32, cycle: &i32, crt: &mut Vec<bool> | {
        let crt_position = cycle % 40;
        let crt_value = (crt_position - reg).abs() <= 1;
        // dbg!(reg, cycle, crt_value);
        // if *cycle > 30 {
        //     todo!()
        // }
        crt.push(crt_value);
    };
    for instr in input.trim().lines() {
        let mut words = instr.split_whitespace();
        match words.next().unwrap() {
            "noop" => {
                draw(&x, &cycle_counter, &mut crt);
                cycle_counter += 1;
                check_value(&cycle_counter, &x, &mut part1_result);
                
            }
            "addx" => {
                for _ in 0..2 {
                    draw(&x, &cycle_counter, &mut crt);
                    cycle_counter += 1;
                    check_value(&cycle_counter, &x, &mut part1_result);
                    // draw(&x, &cycle_counter, &mut crt);
                }
                x += words.next().unwrap().parse::<i32>()?;
            }
            _ => panic!(),
        }
    }
    dbg!(part1_result);

    for (i, value) in crt.into_iter().enumerate() {
        if (i % 40) ==0 {
            println!();
        }
        print!("{}", if value {'#'} else {' '});
    }

    Ok(())
}
