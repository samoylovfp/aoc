fn main() {
    let mut total_passwords_p1 = 0;
    let mut total_passwords_p2 = 0;
    let input = std::fs::read_to_string("input.txt")
    .expect("input")
    .trim()
    .split('-')
    .map(|l|l.parse().expect("number"))
    .collect::<Vec<u32>>();

    for i in input[0]..input[1] {
        if matches_part1(i) {
            total_passwords_p1 += 1;
        }
        if matches_part2(i) {
            total_passwords_p2 += 1
        }
    }
    println!("Stage 1 answer is {}", total_passwords_p1);
    println!("Stage 2 answer is {}", total_passwords_p2);
}


fn get_numbers(input: u32) -> [u8; 7] {
    let mut result = [0; 7];
    for i in 0..=6 {
        result[i as usize] = (input / 10u32.pow(6 - i) % 10) as u8;
    }
    result
}

fn matches_part1(i: u32) -> bool {
    let numbers = get_numbers(i);
    let starting_point = numbers.iter().position(|n| n != &0).expect("not 0 number");
    let mut has_repetition: bool = false;
    for i in starting_point..numbers.len() - 1 {
        if numbers[i+1] < numbers[i] {
            return false
        }
        if numbers[i+1] == numbers[i] {
            has_repetition = true
        }
    }
    has_repetition
}


fn matches_part2(i: u32) -> bool {
    let numbers = get_numbers(i);
    let starting_point = numbers.iter().position(|n| n != &0).expect("nonzero number");
    let mut repeating_number = numbers[starting_point];
    let mut unique_numbers = 1;
    let mut has_repetition = false;

    for i in starting_point..numbers.len() - 1 {
        //dbg!(i, unique_numbers, repeating_number, has_repetition, numbers);
        if numbers[i+1] < numbers[i] {
            return false
        }
        if numbers[i+1] == repeating_number {
            unique_numbers += 1
        } else {
            if unique_numbers == 2 {
                has_repetition = true
            }
            repeating_number = numbers[i+1];
            unique_numbers = 1;
        }
    }
    has_repetition || unique_numbers == 2
}

#[test]
fn test_numbers() {
    assert_eq!(get_numbers(123123), [0,1,2,3,1,2,3]);
    assert_eq!(get_numbers(12312), [0,0,1,2,3,1,2]);
}

#[test]
fn test_p1() {
    assert!(  matches_part1(1233));
    assert!(  matches_part1(1139));
    assert!(! matches_part1(1234));
    assert!(! matches_part1(1132));
}

#[test]
fn test_p2() {
    assert!(  matches_part2(1233));
    assert!(  matches_part2(1139));
    assert!(  matches_part2(11122));
    assert!(! matches_part2(1234));
    assert!(! matches_part2(1132));
    assert!(! matches_part2(11123));
    assert!(! matches_part2(111134));
}
