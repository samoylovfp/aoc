use std::{collections::BTreeSet, time::Instant};

fn main() {
    let input = std::fs::read_to_string("../day6.txt").unwrap();
    let mut orig_guard_pos = None;
    let grid: Vec<Vec<bool>> = input
        .lines()
        .map(|l| l.chars().map(|c| c == '#').collect())
        .collect();
    let grid_size = [grid[0].len(), grid.len()];
    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            if c == '^' {
                orig_guard_pos = Some([x, y]);
            }
        }
    }
    let mut guard_pos = orig_guard_pos.expect("^ should be in the input");
    let mut guard_dir = [0, -1];

    let mut visited = BTreeSet::new();
    'travel: loop {
        visited.insert(guard_pos);
        let mut next_coord = [0, 0];
        for coord in [0, 1] {
            match guard_pos[coord].checked_add_signed(guard_dir[coord]) {
                Some(v) if v < grid_size[coord] => {
                    next_coord[coord] = v;
                }
                _ => break 'travel,
            }
        }

        if grid[next_coord[1]][next_coord[0]] {
            let tmp = guard_dir[0];
            // 0,-1 becomes 1,0
            // 1,0 becomes 0,1
            guard_dir[0] = -guard_dir[1];
            guard_dir[1] = tmp;
        } else {
            guard_pos = next_coord;
        }
    }
    dbg!(visited.len());

    // part2
    let mut grid = grid;
    guard_pos = orig_guard_pos.unwrap();

    // {
    //     grid[8][5] = true;
    //     dbg!(does_guard_loop(&grid, guard_pos));
    //     grid[8][5] = false;
    //     panic!();
    // }

    let mut looping = BTreeSet::new();
    for ox in 0..grid_size[0] {
        for oy in 0..grid_size[1] {
            if (!grid[oy][ox]) && [ox, oy] != guard_pos {
                grid[oy][ox] = true;
                if does_guard_loop(&grid, guard_pos) {
                    looping.insert([ox, oy]);
                }
                grid[oy][ox] = false;
            }
        }
    }
    dbg!(looping.len());
}

fn does_guard_loop(grid: &Vec<Vec<bool>>, start_pos: [usize; 2]) -> bool {
    let grid_size = [grid[0].len(), grid.len()];
    let mut guard_pos = start_pos;
    let mut guard_dir = [0, -1];

    let mut faster_guard_pos = start_pos;
    let mut faster_guard_dir = guard_dir;

    let advance = |pos: &mut [usize; 2], dir: &mut [isize; 2]| -> bool {
        let mut next_coord = [0, 0];
        let mut moved = false;
        while !moved {
            for coord in [0, 1] {
                match pos[coord].checked_add_signed(dir[coord]) {
                    Some(v) if v < grid_size[coord] => {
                        next_coord[coord] = v;
                    }
                    _ => return false,
                }
            }

            if grid[next_coord[1]][next_coord[0]] {
                let tmp = dir[0];
                // 0,-1 becomes 1,0
                // 1,0 becomes 0,1
                dir[0] = -dir[1];
                dir[1] = tmp;
            } else {
                *pos = next_coord;
                moved = true;
            }
        }
        true
    };

    loop {
        if !advance(&mut guard_pos, &mut guard_dir) {
            return false;
        }
        // println!("G  {guard_pos:?}");
        for _try in 0..2 {
            if !advance(&mut faster_guard_pos, &mut faster_guard_dir) {
                return false;
            }
            // println!("FG {faster_guard_pos:?}");
        }

        if guard_pos == faster_guard_pos && guard_dir == faster_guard_dir {
            return true;
        }
    }
}
