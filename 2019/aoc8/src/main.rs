fn main() {
    let data = std::fs::read_to_string("input.txt").unwrap();
    println!("First part answer is {}", get_count(
        data.trim(),
        25,
        6
    ));
    let img = Image::from(data.trim(), 25, 6);
    img.render();
}

#[derive(Debug)]
struct Layer {
    pixels: Vec<u8>
}

#[derive(Debug)]
struct Image {
    layers: Vec<Layer>,
    width: usize,
    height: usize
}

impl Image {
    fn from(data: &str, width: usize, height: usize) -> Image {
        let chars = data.chars().collect::<Vec<char>>();
        Image {
            layers: chars.chunks(width * height)
                .map(|c| Layer{ pixels: c.iter().map(|c|c.to_digit(10).unwrap() as u8).collect() })
                .collect(),
            width,
            height
        }
    }
    fn render(&self) {
        for y in 0..self.height {
            for x in 0..self.width {
                print!("{}", self.get_pixel(x, y));
            }
            println!();
        }
    }
    fn get_pixel(&self, x: usize, y: usize) -> char {
        for l in &self.layers {
            let p = l.pixels[y * self.width + x]; 
            if p == 0 {
                return ' '
            } else if p == 1 {
                return '#'
            }
        }
        return '?'
    }
}

fn get_count(d: &str, width: usize, height: usize) -> u32 {
    let image = Image::from(d, width, height);
    let mut max_zeroes = usize::max_value();
    let mut best_layer_number = 0;
    for layer in image.layers {
        assert_eq!(layer.pixels.len(), width * height);
        let count_numbers = |n| layer.pixels.iter().filter(|&&c|c == n).count();
        let this_layer_max_zeroes = count_numbers(0);
        if this_layer_max_zeroes < max_zeroes {
            max_zeroes = this_layer_max_zeroes;
            best_layer_number = count_numbers(1) * count_numbers(2);
        }
    }
    best_layer_number as u32
}

#[test]
fn test_crafted() {
    let d = r#"123456002112"#;
    assert_eq!(get_count(d, 2, 3), 4);
}