use std::{collections::HashSet, fs::read_to_string};

use itertools::Itertools;

fn main() {
    let input = read_to_string("input3.txt").unwrap();
    let part1: u32 = input
        .lines()
        .map(|line| {
            let items = line.chars().collect_vec();
            let (compart_one, compart_two) = items.split_at(items.len() / 2);
            let compart_one: HashSet<_> = compart_one.iter().collect();
            let compart_two: HashSet<_> = compart_two.iter().collect();
            compart_one
                .intersection(&compart_two)
                .map(|c| item_priority(**c) as u32)
                .sum::<u32>()
        })
        .sum();

    dbg!(part1);

    let part2: u32 = input
        .lines()
        .chunks(3)
        .into_iter()
        .map(|three_elfs| {
            let common_items = three_elfs
                .map(|elf| -> HashSet<_> { elf.chars().collect() })
                .reduce(|a, b| a.intersection(&b).copied().collect())
                .unwrap();
            common_items
                .into_iter()
                .map(|c| item_priority(c) as u32)
                .sum::<u32>()
        })
        .sum();

    dbg!(part2);
}

fn item_priority(item: char) -> u8 {
    match item {
        'a'..='z' => {
            let lowercase_start: u8 = 'a'.try_into().unwrap();
            let byte: u8 = item.try_into().unwrap();
            byte - lowercase_start + 1
        }
        'A'..='Z' => {
            let lowercase_start: u8 = 'A'.try_into().unwrap();
            let byte: u8 = item.try_into().unwrap();
            byte - lowercase_start + 27
        }
        _ => panic!(),
    }
}
