use scan_fmt::{scan_fmt, scan_fmt_help};

fn main() {
    let input = std::fs::read_to_string("day06.txt").unwrap();
    let mut lights = vec![vec![0_u32; 1000]; 1000];
    for instr in input.lines() {
        if let Ok((xs, ys, xe, ye)) = scan_fmt!(
            instr,
            "turn on {d},{d} through {d},{d}",
            usize,
            usize,
            usize,
            usize
        ) {
            for x in xs..=xe {
                for y in ys..=ye {
                    lights[y][x] = lights[y][x] + 1;
                }
            }
        } else if let Ok((xs, ys, xe, ye)) = scan_fmt!(
            instr,
            "turn off {d},{d} through {d},{d}",
            usize,
            usize,
            usize,
            usize
        ) {
            for x in xs..=xe {
                for y in ys..=ye {
                    lights[y][x] = lights[y][x].saturating_sub(1);
                }
            }
        } else if let Ok((xs, ys, xe, ye)) = scan_fmt!(
            instr,
            "toggle {d},{d} through {d},{d}",
            usize,
            usize,
            usize,
            usize
        ) {
            for x in xs..=xe {
                for y in ys..=ye {
                    lights[y][x] = lights[y][x] + 2;
                }
            }
        } else {
            panic!("cannot parse {instr:?}");
        }
    }
    dbg!(lights
        .iter()
        .map(|l| l.iter().map(|l| *l).sum::<u32>())
        .sum::<u32>());
}
