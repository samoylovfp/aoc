fn execute(mut instructions: Vec<u32>) -> Vec<u32> {
    for w in instructions.clone().chunks(4) {
        let op = w[0];
        if op == 99 {
            break
        }
        let lh = instructions[w[1] as usize];
        let rh = instructions[w[2] as usize];
        let target = &mut instructions[w[3] as usize];
        match op {
            1 => *target = lh + rh,
            2 => *target = lh * rh,
            o => panic!("Unknown opcode {}", o)
        }
    }
    instructions
}

fn main() {
    let mut instructions: Vec<u32> = std::fs::read_to_string("input.txt")
    .expect("input")
    .split(",")
    .filter_map(|n| n.parse().ok())
    .collect();

    instructions[1] = 12;
    instructions[2] = 2;
    println!("Part 1 is {}", execute(instructions.clone())[0]);

    let response_for_part2 = 19690720;
    for noun in 0..99 {
        for verb in 0..99 {
            let mut instructions_clone = instructions.clone();
            instructions_clone[1] = noun;
            instructions_clone[2] = verb;
            let result = execute(instructions_clone)[0];
            if result == response_for_part2 {
                println!("Response for part 2 is {}", noun * 100 + verb);
                break
            }
        }
    }
}


#[test]
fn test_simple() {
    assert_eq!(
        execute(vec![1, 0, 0, 0, 99]),
                vec![2, 0, 0, 0, 99]
    );
    assert_eq!(
        execute(vec![2, 3, 0, 3, 99]),
                vec![2, 3, 0, 6, 99]
    );
}