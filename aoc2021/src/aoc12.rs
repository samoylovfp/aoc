use std::{
    collections::{HashMap, HashSet},
    fs::read_to_string,
};

fn main() {
    let graph = Graph::parse(&read_to_string("input12.txt").unwrap());
    dbg!(graph.distinct_paths(false));
    dbg!(graph.distinct_paths(true));
}

#[derive(Debug)]
struct Graph(HashMap<String, Vec<String>>);

impl Graph {
    fn parse(input: &str) -> Graph {
        let mut map: HashMap<String, Vec<String>> = HashMap::new();
        input.trim().lines().for_each(|l| {
            let (from, to) = l.trim().split_once("-").unwrap();
            map.entry(from.to_string())
                .or_default()
                .push(to.to_string());
            map.entry(to.to_string())
                .or_default()
                .push(from.to_string());
        });

        Graph(map)
    }

    fn distinct_paths(&self, single_small_cave_can_be_visited_twice: bool) -> usize {
        let mut paths = HashSet::new();
        visit_connected_caves(
            self,
            "start",
            &[],
            &mut paths,
            single_small_cave_can_be_visited_twice,
        );
        paths.len()
    }
}

fn extended_vec<T: Clone>(v: &[T], e: T) -> Vec<T> {
    let mut v = Vec::from(v);
    v.push(e);
    v
}

fn is_small_cave(c: &str) -> bool {
    c.chars().all(|c| c.is_lowercase())
}

fn visit_connected_caves(
    graph: &Graph,
    current_cave: &str,
    current_path: &[String],
    complete_paths: &mut HashSet<Vec<String>>,
    single_small_cave_can_be_visited_twice: bool,
) {
    if is_small_cave(current_cave) {
        // This clause is super inefficient but
        // the task is still solved in ~500ms on my pc in --release mode
        if single_small_cave_can_be_visited_twice {
            let has_a_small_cave_been_visited_twice = {
                current_path.iter().any(|c| {
                    is_small_cave(c) && current_path.iter().filter(|&c2| c2 == c).count() >= 2
                })
            };
            if has_a_small_cave_been_visited_twice && current_path.iter().any(|c| c == current_cave)
            {
                return;
            }
        } else if current_path.iter().any(|c| c == current_cave) {
            return;
        }
    }

    let current_path = extended_vec(current_path, current_cave.to_string());
    let connected_caves = graph
        .0
        .get(current_cave)
        .unwrap_or_else(|| panic!("Nothing connected to {:?}", current_cave));

    // dbg!(&current_path, &connected_caves);
    if current_cave == "end" {
        complete_paths.insert(current_path);
        return;
    }

    for cave in connected_caves {
        if cave != "start" {
            visit_connected_caves(
                graph,
                cave,
                &current_path,
                complete_paths,
                single_small_cave_can_be_visited_twice,
            )
        }
    }
}

#[test]
fn test_provided() {
    let g = Graph::parse(
        r#"
        start-A
        start-b
        A-c
        A-b
        b-d
        A-end
        b-end
    "#,
    );
    assert_eq!(g.distinct_paths(false), 10);
    assert_eq!(g.distinct_paths(true), 36)
}
