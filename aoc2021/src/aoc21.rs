use std::{collections::HashMap, mem::swap};

fn main() {
    part_one();
    part_two();
}

fn part_one() {
    let mut die_rolls = 0;
    let mut die = || {
        let result = die_rolls % 100 + 1;
        die_rolls += 1;
        result
    };
    let mut p1_pos_minus_one = 3;
    let mut p2_pos_minus_one = 1;

    let mut p1_score = 0;
    let mut p2_score = 0;

    loop {
        p1_pos_minus_one = (p1_pos_minus_one + die() + die() + die()) % 10;
        p1_score += p1_pos_minus_one + 1;
        if p1_score >= 1000 {
            break;
        }

        p2_pos_minus_one = (p2_pos_minus_one + die() + die() + die()) % 10;
        p2_score += p2_pos_minus_one + 1;
        if p2_score >= 1000 {
            break;
        }
    }
    if p1_score >= 1000 {
        println!("Part 1 is {}", p2_score * die_rolls);
    } else {
        println!("Part 1 is {}", p1_score * die_rolls);
    }
}

#[derive(Hash, PartialEq, Eq, Clone)]
struct State {
    p1_pos_minus_one: u8,
    p2_pos_minus_one: u8,
    p1_score: u8,
    p2_score: u8,
}

fn part_two() {
    let mut games_in_progress: HashMap<State, u128> = HashMap::new();
    games_in_progress.insert(
        State {
            p1_pos_minus_one: 3,
            p2_pos_minus_one: 1,
            p1_score: 0,
            p2_score: 0,
        },
        1,
    );

    let mut p1_wins = 0;
    let mut p2_wins = 0;

    while !games_in_progress.is_empty() {
        for player_n in [1, 2] {
            for _die_trow in 0..3 {
                throw(&mut games_in_progress, player_n);
            }
            increase_score(&mut games_in_progress, player_n);
            let wins_counter = match player_n {
                1 => &mut p1_wins,
                2 => &mut p2_wins,
                _ => panic!(),
            };
            *wins_counter += extract_wins(&mut games_in_progress, player_n);
        }
    }

    println!("Part two {}", p1_wins.max(p2_wins))
}

fn throw(games_in_progress: &mut HashMap<State, u128>, player_n: i32) {
    let mut new_games = HashMap::new();

    for (state, count) in games_in_progress.iter() {
        for die_value in 1..=3 {
            let mut new_state = state.clone();

            let player_pos_to_change = match player_n {
                1 => &mut new_state.p1_pos_minus_one,
                2 => &mut new_state.p2_pos_minus_one,
                _ => panic!(),
            };
            *player_pos_to_change = (*player_pos_to_change + die_value) % 10;
            *new_games.entry(new_state).or_default() += *count;
        }
    }
    swap(&mut new_games, games_in_progress);
}

fn increase_score(games_in_progress: &mut HashMap<State, u128>, player_n: i32) {
    let mut new_games = HashMap::new();

    for (state, count) in games_in_progress.iter() {
        let mut new_state = state.clone();
        match player_n {
            1 => new_state.p1_score += new_state.p1_pos_minus_one + 1,
            2 => new_state.p2_score += new_state.p2_pos_minus_one + 1,
            _ => panic!(),
        };
        *new_games.entry(new_state).or_default() += *count;
    }
    swap(&mut new_games, games_in_progress);
}

fn extract_wins(games_in_progress: &mut HashMap<State, u128>, player_n: i32) -> u128 {
    let mut total = 0;
    games_in_progress.retain(|state, count| {
        let score = match player_n {
            1 => state.p1_score,
            2 => state.p2_score,
            _ => panic!(),
        };
        if score >= 21 {
            total += *count;
            false
        } else {
            true
        }
    });
    total
}
