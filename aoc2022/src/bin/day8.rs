use std::{collections::HashSet, fs::read_to_string};

use itertools::Itertools;

fn main() {
    let input = read_to_string("input8.txt").unwrap();
    let grid = input
        .lines()
        .map(|l| l.chars().map(|c| c.to_digit(10).unwrap()).collect_vec())
        .collect_vec();

    dbg!(part1(&grid));
    dbg!(part2(&grid));
}

fn part1(grid: &Vec<Vec<u32>>) -> usize {
    let width = grid[0].len();
    let height = grid.len();
    for v in grid {
        assert_eq!(v.len(), width as usize);
    }
    let mut visible = HashSet::new();

    for start_x in 0..width {
        for ray_coordinates in [
            Box::new(0..height) as Box<dyn Iterator<Item = usize>>,
            Box::new((0..height).rev()),
        ] {
            let mut max_visible = None;
            for coord in ray_coordinates {
                let tree = grid[coord][start_x];
                if max_visible.is_none() || max_visible.unwrap() < tree {
                    max_visible = Some(tree);
                    visible.insert((start_x, coord));
                }
            }
        }
    }

    // It is easier to compare to the previous block when it is written like that
    #[allow(clippy::needless_range_loop)]
    for start_y in 0..height {
        for ray_coordinates in [
            Box::new(0..width) as Box<dyn Iterator<Item = usize>>,
            Box::new((0..width).rev()),
        ] {
            let mut max_visible = None;
            for coord in ray_coordinates {
                let tree = grid[start_y][coord];
                if max_visible.is_none() || max_visible.unwrap() < tree {
                    max_visible = Some(tree);
                    visible.insert((coord, start_y));
                }
            }
        }
    }

    visible.len()
}

fn part2(grid: &[Vec<u32>]) -> u32 {
    grid.iter()
        .enumerate()
        .flat_map(|(y, line)| {
            line.iter().enumerate().map(move |(x, current_tree)| {
                let mut score = 1;
                for direction in [[0, 1], [0, -1], [1, 0], [-1, 0]] {
                    let mut current_direction_visible_trees = 0;
                    let mut point = [x as i32 + direction[0], y as i32 + direction[1]];
                    while let Some(tree) = grid
                        .get(point[1] as usize)
                        .and_then(|line| line.get(point[0] as usize))
                    {
                        current_direction_visible_trees += 1;
                        if tree >= current_tree {
                            break;
                        }
                        point[0] += direction[0];
                        point[1] += direction[1];
                    }
                    score *= current_direction_visible_trees;
                }
                score
            })
        })
        .max()
        .unwrap()
}

#[test]
fn test_part2() {
    assert_eq!(
        part2(&[
            vec![3, 0, 3, 7, 3],
            vec![2, 5, 5, 1, 2],
            vec![6, 5, 3, 3, 2],
            vec![3, 3, 5, 4, 9],
            vec![3, 5, 3, 9, 0]
        ]),
        8
    )
}
