use std::{fs::read_to_string, str::FromStr};

fn main() {
    let input = read_to_string("input2.txt").unwrap();
    let presents: Vec<Present> = input.lines().map(|l| l.parse().unwrap()).collect();
    println!(
        "First part is {}",
        presents
            .iter()
            .map(|p| p.get_needed_wrapping_paper_area())
            .sum::<u32>()
    );
    println!(
        "Second part is {}",
        presents
            .iter()
            .map(|p| p.get_needed_ribbon_length())
            .sum::<u32>()
    )
}

struct Present {
    l: u32,
    w: u32,
    h: u32,
}

impl FromStr for Present {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut dims = s.split("x");
        let mut get_dim = || {
            dims.next()
                .ok_or(format!("Cannot parse present, too little values"))
                .and_then(|d| {
                    d.parse()
                        .map_err(|e| format!("Cannot parse present: {:?}", e))
                })
        };
        Ok(Present {
            l: get_dim()?,
            w: get_dim()?,
            h: get_dim()?,
        })
    }
}

impl Present {
    fn get_needed_wrapping_paper_area(&self) -> u32 {
        let sides = [
            self.l * self.w * 2,
            self.w * self.h * 2,
            self.l * self.h * 2,
        ];
        let smallest_side = sides.into_iter().min().unwrap() / 2;

        sides
            .into_iter()
            .chain(std::iter::once(smallest_side))
            .sum()
    }
    fn get_needed_ribbon_length(&self) -> u32 {
        let face_half_perimeters = [self.l + self.w, self.l + self.h, self.w + self.h];
        let smallest_face_perimeter = face_half_perimeters.into_iter().min().unwrap() * 2;
        smallest_face_perimeter + self.l * self.w * self.h
    }
}
