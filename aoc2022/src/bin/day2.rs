use std::fs::read_to_string;

fn main() {
    let input = read_to_string("input2.txt").unwrap();
    let part1: u32 = input
        .trim()
        .lines()
        .map(|line| {
            let (opponent, you) = line.split_once(' ').unwrap();
            let opponent = Turn::from_str(opponent);
            let you = Turn::from_str(you);
            you.match_score(&opponent) as u32
        })
        .sum();
    dbg!(part1);

    let part2: u32 = input
        .trim()
        .lines()
        .map(|line| {
            let (opponent, expected) = line.split_once(' ').unwrap();
            let opponent = Turn::from_str(opponent);
            let you = Turn::from_expected(&opponent, expected);
            you.match_score(&opponent) as u32
        })
        .sum();
    dbg!(part2);
}

#[derive(PartialEq, Eq, Clone)]
enum Turn {
    Rock,
    Paper,
    Scissors,
}
impl Turn {
    fn from_str(s: &str) -> Self {
        match s {
            "A" | "X" => Self::Rock,
            "B" | "Y" => Self::Paper,
            "C" | "Z" => Self::Scissors,
            _ => panic!(),
        }
    }

    fn winner_of(&self) -> Self {
        match self {
            Self::Rock => Self::Paper,
            Self::Paper => Self::Scissors,
            Self::Scissors => Self::Rock,
        }
    }

    fn match_score(&self, opponent: &Self) -> u8 {
        let win_score = if self == opponent {
            3
        } else if self == &opponent.winner_of() {
            6
        } else {
            0
        };
        win_score
            + match self {
                Self::Rock => 1,
                Self::Paper => 2,
                Self::Scissors => 3,
            }
    }

    pub(crate) fn from_expected(opponent: &Self, expected: &str) -> Self {
        match expected {
            // lose
            "X" => opponent.winner_of().winner_of(),
            // draw
            "Y" => opponent.clone(),
            // win
            "Z" => opponent.winner_of(),
            _ => panic!(),
        }
    }
}
