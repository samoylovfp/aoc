use std::{cmp::Ordering, collections::HashMap, fs::read_to_string};

fn main() {
    let input = read_to_string("input5.txt").unwrap();
    let lines = parse_lines(&input);
    dbg!(part_one(&lines, false));
    dbg!(part_two(&lines, false));
}

fn part_one(lines: &[Line], debug: bool) -> usize {
    let mut field = HashMap::new();
    for line in lines.iter().filter(|l| l.is_straight()) {
        for point in line.points() {
            *field.entry(point).or_insert(0) += 1;
        }
    }
    debug.then(||draw_board(&field));
    field.values().filter(|&&v| v >= 2).count()
}

fn part_two(lines: &[Line], debug: bool) -> usize {
    let mut field = HashMap::new();
    for line in lines.iter() {
        for point in line.points() {
            *field.entry(point).or_insert(0) += 1;
        }
    }
    debug.then(||draw_board(&field));
    field.values().filter(|&&v| v >= 2).count()
}

fn draw_board(field: &HashMap<(u32, u32), i32>) {
    for y in 0..20 {
        for x in 0..20 {
            print!(
                "{}",
                field
                    .get(&(x, y))
                    .map(|i| i.to_string())
                    .unwrap_or_else(|| ".".to_string())
            )
        }
        println!();
    }
}

#[derive(Debug)]
struct Line {
    from: (u32, u32),
    to: (u32, u32),
}

impl Line {
    fn is_straight(&self) -> bool {
        self.from.0 == self.to.0 || self.from.1 == self.to.1
    }
    fn points(&self) -> Vec<(u32, u32)> {
        let x_dir = match self.to.0.cmp(&self.from.0) {
            Ordering::Less => -1,
            Ordering::Equal => 0,
            Ordering::Greater => 1,
        };

        let y_dir = match self.to.1.cmp(&self.from.1) {
            Ordering::Less => -1,
            Ordering::Equal => 0,
            Ordering::Greater => 1,
        };

        let mut cursor = self.from;

        let mut result = vec![cursor];

        while cursor != self.to {
            cursor.0 = (cursor.0 as i32 + x_dir).try_into().unwrap();
            cursor.1 = (cursor.1 as i32 + y_dir).try_into().unwrap();
            result.push(cursor);
        }
        result
    }
}

fn parse_lines(input: &str) -> Vec<Line> {
    input
        .trim()
        .lines()
        .map(|line_str| {
            let (from, to) = line_str.trim().split_once(" -> ").unwrap();
            Line {
                from: parse_coords(from),
                to: parse_coords(to),
            }
        })
        .collect()
}

fn parse_coords(coord_str: &str) -> (u32, u32) {
    let (x, y) = coord_str.split_once(",").unwrap();
    (x.parse().unwrap(), y.parse().unwrap())
}

#[test]
fn test_provided() {
    let lines = parse_lines(
        r#"
0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
    "#,
    );
    assert_eq!(part_one(&lines, true), 5);
    assert_eq!(part_two(&lines, true), 12);
}
