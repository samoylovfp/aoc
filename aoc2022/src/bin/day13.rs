use std::{cmp::Ordering, fs::read_to_string};

use nom::{
    branch::alt,
    bytes::complete::{tag, take_while},
    character::complete::{line_ending, multispace1},
    combinator::fail,
    multi::{separated_list0, separated_list1},
    sequence::{separated_pair, tuple},
    Finish,
};

fn main() -> anyhow::Result<()> {
    let input = read_to_string("input13.txt").unwrap();
    let (i, packets) = separated_list1(
        tuple((line_ending, line_ending)),
        separated_pair(parse_list, line_ending, parse_list),
    )(&input)
    .finish()
    .unwrap();

    assert!(i.trim().is_empty());

    let part1: usize = packets
        .iter()
        .enumerate()
        .filter_map(|(i, (p1, p2))| p1.in_correct_order(p2).unwrap().then(|| i + 1))
        .sum();

    dbg!(part1);

    let (_i, mut all_packets_together) = separated_list1(multispace1, parse_list)(&input)
        .finish()
        .unwrap();

    all_packets_together.sort_by(Packet::cmp);
    let divider1 = Packet::List(vec![Packet::List(vec![Packet::Number(2)])]);
    let divider2 = Packet::List(vec![Packet::List(vec![Packet::Number(6)])]);

    // 1-indexed
    let div1_index = all_packets_together
        .binary_search_by(|item| item.cmp(&divider1))
        .unwrap_err()
        + 1;
    // 1-indexed, plus previous divider inserted
    let div2_index = all_packets_together
        .binary_search_by(|item| item.cmp(&divider2))
        .unwrap_err()
        + 2;

    let part2 = div1_index * div2_index;
    dbg!("part2", part2);

    Ok(())
}

#[derive(Debug)]
enum Packet {
    List(Vec<Packet>),
    Number(u32),
}

impl Packet {
    fn in_correct_order(&self, next_packet: &Packet) -> Option<bool> {
        match (self, next_packet) {
            (Packet::Number(left), Packet::Number(right)) => {
                if left == right {
                    None
                } else {
                    Some(left < right)
                }
            }
            (Packet::List(left), Packet::List(right)) => {
                let mut left_iter = left.iter();
                let mut right_iter = right.iter();
                loop {
                    match (left_iter.next(), right_iter.next()) {
                        (None, None) => break None,
                        (None, Some(..)) => break Some(true),
                        (Some(..), None) => break Some(false),
                        (Some(left), Some(right)) => {
                            if let Some(decision) = left.in_correct_order(right) {
                                break Some(decision);
                            }
                        }
                    }
                }
            }
            (Packet::List(..), Packet::Number(right)) => {
                self.in_correct_order(&Packet::List(vec![Packet::Number(*right)]))
            }
            (Packet::Number(left), Packet::List(..)) => {
                Packet::List(vec![Packet::Number(*left)]).in_correct_order(next_packet)
            }
        }
    }

    fn cmp(&self, next: &Packet) -> Ordering {
        match self.in_correct_order(next) {
            None => Ordering::Equal,
            Some(true) => Ordering::Less,
            Some(false) => Ordering::Greater,
        }
    }
}

fn parse_list(i: &str) -> nom::IResult<&str, Packet> {
    let (i, _) = tag("[")(i)?;
    let (i, sub_packets) = separated_list0(tag(","), alt((parse_list, parse_num)))(i)?;
    let (i, _) = tag("]")(i)?;

    Ok((i, Packet::List(sub_packets)))
}

fn parse_num(i: &str) -> nom::IResult<&str, Packet> {
    let (i, num) = take_while(|c: char| c.is_ascii_digit())(i)?;
    if num.is_empty() {
        fail::<&str, (), _>(i)?;
    }
    Ok((i, Packet::Number(num.parse().unwrap())))
}

#[cfg(test)]
mod test {
    use super::*;

    fn assert_right_order(left_pack: &str, right_pack: &str, expect: Option<bool>) {
        assert_eq!(
            parse_list(left_pack)
                .unwrap()
                .1
                .in_correct_order(&parse_list(right_pack).unwrap().1),
            expect
        );
    }

    #[test]
    fn test_provided() {
        assert_right_order("[1,1,3,1,1]", "[1,1,5,1,1]", Some(true));
        assert_right_order("[[1],[2,3,4]]", "[[1],4]", Some(true));
        // assert_right_order("[1,1,3,1,1]", "[1,1,5,1,1]", Some(true));
    }
}
