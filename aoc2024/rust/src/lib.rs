pub use glam::{IVec2, UVec2};

pub struct Grid<T> {
    pub data: Vec<Vec<T>>,
}

impl Grid<char> {
    pub fn from_str_chars(input: &str) -> Self {
        Self {
            data: input.lines().map(|l| l.chars().collect()).collect(),
        }
    }
    pub fn from_file_chars(fname: &str) -> Self {
        Self::from_str_chars(&std::fs::read_to_string(fname).unwrap())
    }

    pub fn print(&self) {
        for line in &self.data {
            for symbol in line {
                print!("{symbol}");
            }
            println!()
        }
    }
}

impl<T> Grid<T> {
    pub fn new(data: Vec<Vec<T>>) -> Self {
        Grid { data }
    }

    pub fn iter_cells(&self) -> impl Iterator<Item = (IVec2, &T)> {
        self.data.iter().enumerate().flat_map(|(y, line)| {
            line.iter()
                .enumerate()
                .map(move |(x, val)| (IVec2::new(x as i32, y as i32), val))
        })
    }

    pub fn iter_offsets<'s, 'o: 's>(
        &'s self,
        center: IVec2,
        offsets: &'o [[i32; 2]],
    ) -> impl Iterator<Item = (IVec2, Option<&'s T>)> + 's {
        offsets.into_iter().filter_map(move |&dc| {
            let dc: IVec2 = dc.into();
            let new_coord = center + dc;
            Some((new_coord, self.get(new_coord)))
        })
    }

    pub fn cross_neighbours(&self, center: IVec2) -> impl Iterator<Item = (IVec2, Option<&T>)> {
        self.iter_offsets(center, &[[-1, 0], [1, 0], [0, 1], [0, -1]])
    }

    pub fn get(&self, v: IVec2) -> Option<&T> {
        if v.x < 0 || v.y < 0 {
            return None;
        }
        self.data.get(v.y as usize)?.get(v.x as usize)
    }

    pub fn get_mut(&mut self, v: IVec2) -> Option<&mut T> {
        self.data.get_mut(v.y as usize)?.get_mut(v.x as usize)
    }

    pub fn map<O, F: FnMut(T) -> O + Copy>(self, f: F) -> Grid<O> {
        Grid {
            data: self
                .data
                .into_iter()
                .map(|line| line.into_iter().map(f).collect())
                .collect(),
        }
    }
}

impl<T: Clone> Grid<T> {
    pub fn swap(&mut self, p1: IVec2, p2: IVec2) {
        let mut tmp = self.get(p1).unwrap().clone();
        let v2 = self.get_mut(p2).unwrap();
        std::mem::swap(v2, &mut tmp);
        let v1 = self.get_mut(p1).unwrap();
        std::mem::swap(v1, &mut tmp);
    }
}

pub trait Turn {
    fn left(self) -> Self;
    fn right(self) -> Self;
}

impl Turn for IVec2 {
    fn left(self) -> Self {
        IVec2::new(self.y, -self.x)
    }

    fn right(self) -> Self {
        IVec2::new(-self.y, self.x)
    }
}
