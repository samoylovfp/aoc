use std::collections::VecDeque;

struct Mem {
    layout: Vec<i32>,
    instructions_pointer: usize
}

impl Mem {
    fn new(layout: Vec<i32>) -> Mem {
        Mem {
            layout, instructions_pointer: 0
        }
    }
    fn next(&mut self) -> i32 {
        let res = self.layout[self.instructions_pointer];
        self.instructions_pointer += 1;
        res
    }
    fn get_val(&self, addr: usize) -> i32 {
        self.layout[addr]
    }
    fn set_val(&mut self, addr: usize, val: i32) {
        self.layout[addr] = val
    }
    fn jump(&mut self, val: usize) {
        self.instructions_pointer = val;
        dbg!("Jumping to", val);
    }
}

fn execute(
    mut mem: Mem,
    mut input: VecDeque<i32>,
    output: &mut Vec<i32>) {
    loop {
        let op_encoded = mem.next();
        let op = op_encoded % 100;
        let first_operand_mode = op_encoded / 100 % 10;
        let second_operand_mode = op_encoded / 1000 % 10;
        dbg!("Executing", op_encoded);
        match op {
            99 => break,
            1 | 2 | 7 | 8 => {
                let mut lhs = mem.next();
                let mut rhs = mem.next();
                let target = mem.next();
                if first_operand_mode == 0 {
                    lhs = mem.get_val(lhs as usize);
                }
                if second_operand_mode == 0 {
                    rhs = mem.get_val(rhs as usize);
                }
                let val = match op {
                    1 => lhs + rhs,
                    2 => lhs * rhs,
                    7 => if lhs < rhs {1} else {0},
                    8 => if lhs == rhs {1} else {0},
                    _ => unreachable!()
                };
                mem.set_val(target as usize, val)
            },
            3 => {
                let pos = mem.next();
                mem.set_val(pos as usize, input.pop_front().expect("input"))
            },
            4 => {
                let mut val = mem.next();
                if first_operand_mode == 0 {
                    val = mem.get_val(val as usize);
                }
                println!("Diagnostic is {}", val);
                output.push(val);
            },
            5 | 6 => {
                let mut op1 = mem.next();
                let mut op2 = mem.next();
                if first_operand_mode == 0 {
                    op1 = mem.get_val(op1 as usize);
                }
                if second_operand_mode == 0 {
                    op2 = mem.get_val(op2 as usize);
                }
                match op {
                    5 => if op1 != 0 {mem.jump(op2 as usize)},
                    6 => if op1 == 0 {mem.jump(op2 as usize)},
                    _ => unreachable!()
                }
            }
            o => panic!("Unknwon operation {}", o)
        }
    }
}

fn main() {
    let instructions: Vec<i32> = std::fs::read_to_string("input.txt")
    .expect("input")
    .split(",")
    .filter_map(|n| n.trim().parse().ok())
    .collect();
    let input = VecDeque::from(vec![ 5 ]);
    let mut output = Vec::new();

    execute(Mem::new(instructions), input, &mut output);
    
}

#[test]
fn test_jump() {
    let layout = vec![3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9];
    let mut output = vec![];
    execute(Mem::new(layout.clone()), VecDeque::from(vec![1]), &mut output);
    assert_eq!(output, vec![1]);
    output.clear();

    execute(Mem::new(layout.clone()), VecDeque::from(vec![0]), &mut output);
    assert_eq!(output, vec![0]);

    let run = |input| {
        let mut output = vec![];
        execute(Mem::new(vec![3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99]),
            VecDeque::from(vec![input]), &mut output);
        output
    };

    assert_eq!(run(6), vec![999]);
    assert_eq!(run(8), vec![1000]);
    assert_eq!(run(888), vec![1001]);

}