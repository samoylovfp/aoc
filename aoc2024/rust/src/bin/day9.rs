use std::collections::{BTreeMap, BTreeSet, VecDeque};

use itertools::Itertools;

#[derive(Debug, Clone)]
enum Block {
    File { id: u32, length: usize },
    Empty { len: usize },
}

fn main() {
    let fname = "../day9_full.txt";
    let input = std::fs::read_to_string(fname).unwrap();
    let mut disk = Vec::new();
    let mut file_id = 0;
    let mut is_file = true;

    for n in input.trim().chars() {
        let length: u8 = n.to_string().parse().unwrap();
        for _ in 0..length {
            disk.push(if is_file {
                Block::File {
                    id: file_id,
                    length: length as usize,
                }
            } else {
                Block::Empty {
                    len: length as usize,
                }
            });
        }
        if is_file {
            file_id += 1;
        }
        is_file = !is_file;
    }
    dbg!(part1(disk));
    dbg!(part2(input));
}

fn part1(mut disk: Vec<Block>) -> usize {
    let mut empty_pointer = 0;
    'compacting: loop {
        'find_free: loop {
            let Some(v) = disk.get(empty_pointer) else {
                break 'compacting;
            };
            if matches!(v, Block::Empty { .. }) {
                break 'find_free;
            }
            empty_pointer += 1;
        }
        let val = 'find_val: loop {
            if empty_pointer == disk.len() - 1 {
                break 'compacting;
            }
            let v = disk.pop().unwrap();
            if matches!(v, Block::File { .. }) {
                break 'find_val v;
            }
        };
        disk[empty_pointer] = val;
    }
    let mut res = 0;
    for (i, v) in disk.iter().enumerate() {
        if let Block::File { id, .. } = v {
            res += i * (*id as usize);
        }
    }
    res
}

fn part2(input: String) -> u64 {
    let mut disk_contents = BTreeMap::new();

    // parse
    {
        let mut is_file = true;
        let mut file_n = 0;
        let mut idx = 0;
        for n in input.trim().chars() {
            let length = n.to_digit(10).unwrap() as usize;
            if is_file {
                disk_contents.insert(idx, Block::File { id: file_n, length });
                file_n += 1;
            } else {
                if length > 0 {
                    disk_contents.insert(idx, Block::Empty { len: length });
                }
            }
            idx += length;
            is_file = !is_file;
        }
    }

    for file_block in disk_contents.clone().iter().rev() {
        let (
            &file_idx,
            &Block::File {
                id: file_id,
                length: file_length,
            },
        ) = file_block
        else {
            continue;
        };
        // println!("Considering {file_id}");

        let fitting_empty = 'find_empty: {
            for empty_block_idx in disk_contents.keys().copied() {
                let block = disk_contents.get(&empty_block_idx).and_then(|b| match b {
                    Block::Empty { len } => Some(*len),
                    _ => None,
                });
                if let Some(empty_len) = block {
                    // println!("Checking empty at {empty_block_idx} of size {empty_len}");
                    if empty_len >= file_length {
                        if empty_block_idx > file_idx {
                            break 'find_empty None;
                        }
                        break 'find_empty Some((empty_block_idx, empty_len));
                    }
                }
            }
            None
        };

        if let Some((empty_idx, empty_len)) = fitting_empty {
            // println!("Moving {file_id} to {empty_block_idx}");
            disk_contents.remove(&file_idx);
            disk_contents.remove(&empty_idx);
            disk_contents.insert(
                empty_idx,
                Block::File {
                    id: file_id,
                    length: file_length,
                },
            );

            let new_empty_len = empty_len - file_length;
            let new_idx = empty_idx + file_length;
            // println!("New empty is at {new_idx} of length {new_empty_len}");
            if new_empty_len > 0 {
                disk_contents.insert(new_idx, Block::Empty { len: new_empty_len });
            }
        }
    }

    disk_contents
        .iter()
        .filter_map(|(&disk_idx, block)| match block {
            &Block::File { id, length } => Some(
                (disk_idx..(disk_idx + length))
                    .map(|i| (i * id as usize) as u64)
                    .sum::<u64>(),
            ),
            _ => None,
        })
        .sum()
}
