use glam::U64Vec2;
use scan_fmt::scan_fmt;

fn main() {
    let input = std::fs::read_to_string("day13_full.txt").unwrap();
    let machines = parse_machines(&input);
    dbg!(part1(&machines));
    dbg!(part2(&machines));
}

fn part1(machines: &[Machine]) -> u64 {
    machines
        .iter()
        .filter_map(|m| {
            let mut min = None;
            for b_presses in 0..100 {
                for a_presses in 0..100 {
                    let moved = a_presses * m.a + b_presses * m.b;
                    if moved == m.prize {
                        let tokens = a_presses * 3 + b_presses;
                        min = match min {
                            None => Some(a_presses * 3 + b_presses),
                            Some(m) => Some(m.min(tokens)),
                        };
                    }
                }
            }

            min
        })
        .sum()
}

fn part2(machines: &[Machine]) -> u64 {
    machines
        .iter()
        .filter_map(|m| {
            let px = (m.prize.x + 10000000000000) as i64;
            let py = (m.prize.y + 10000000000000) as i64;
            let ax = m.a.x as i64;
            let ay = m.a.y as i64;
            let bx = m.b.x as i64;
            let by = m.b.y as i64;
            let div = by * ax - bx * ay;
            let nom = py * ax - px * ay;
            if div == 0 || nom % div != 0 {
                return None;
            }
            let x2 = nom / div;
            let x1_nom = px - x2 * bx;
            if x1_nom % ax != 0 {
                return None;
            }
            let x1 = x1_nom / ax;
            assert_eq!(
                x1 as u64 * m.a + x2 as u64 * m.b,
                U64Vec2::new(px as u64, py as u64)
            );

            Some(x1 as u64 * 3 + x2 as u64)
        })
        .sum()
}

struct Machine {
    a: U64Vec2,
    b: U64Vec2,
    prize: U64Vec2,
}

fn parse_machines(input: &str) -> Vec<Machine> {
    let mut lines = input.lines();
    let mut res = vec![];
    loop {
        let Some(ba) = lines.next() else {
            break;
        };
        let (ax, ay) = scan_fmt!(ba, "Button A: X+{d}, Y+{d}", u64, u64).unwrap();
        let (bx, by) =
            scan_fmt!(lines.next().unwrap(), "Button B: X+{d}, Y+{d}", u64, u64).unwrap();
        let (px, py) = scan_fmt!(lines.next().unwrap(), "Prize: X={d}, Y={d}", u64, u64).unwrap();
        let _empty = lines.next();
        res.push(Machine {
            a: U64Vec2::new(ax, ay),
            b: U64Vec2::new(bx, by),
            prize: U64Vec2::new(px, py),
        })
    }

    res
}
