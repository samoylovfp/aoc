use super::AocSolver;

pub struct Day2;

impl AocSolver for Day2 {
    fn title(&self) -> &str {
        "Day2: Rock Paper Scissors"
    }

    fn show(&mut self, input: &str, ui: &mut eframe::egui::Ui) {
        ui.label("LIZARD SPOCK");
    }
}