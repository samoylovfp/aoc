fn main() {
    let input = std::fs::read_to_string("day08.txt").unwrap();
    dbg!(input
        .lines()
        .map(|l| l.chars().count() - count_unescaped(l))
        .sum::<usize>());
    dbg!(input
        .lines()
        .map(|l| count_escaped(l) - l.chars().count())
        .sum::<usize>());
}

fn count_unescaped(l: &str) -> usize {
    let mut total = 0;
    let mut escaping = false;
    let mut hex = 0;
    for c in (&l[1..l.len() - 1]).chars() {
        if hex == 2 {
            hex -= 1;
            continue;
        }
        if hex == 1 {
            hex -= 1;
            total += 1;
            escaping = false;
            continue;
        }
        if c == 'x' && escaping {
            hex = 2;
            continue;
        }

        if escaping {
            escaping = false;
            total += 1;
            continue;
        }
        if c == '\\' {
            escaping = true;
            continue;
        }
        total += 1;
    }
    total
}

fn count_escaped(l: &str) -> usize {
    let mut res = 2;
    for c in l.chars() {
        res += match c {
            '"' => 2,
            '\\' => 2,
            _ => 1,
        }
    }
    res
}
