pub mod day1;
pub mod day2;

use self::{day1::Day1, day2::Day2};
use eframe::egui::Ui;

pub trait AocSolver {
    fn title(&self) -> &str;
    fn show(&mut self, input: &str, ui: &mut Ui);
}

pub fn get_solvers() -> Vec<Box<dyn AocSolver>> {
    vec![Box::new(Day1), Box::new(Day2)]
}
