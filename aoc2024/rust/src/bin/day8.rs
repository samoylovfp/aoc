use std::collections::{BTreeMap, BTreeSet, HashSet};

use glam::{IVec2, UVec2};
use itertools::Itertools;

fn main() {
    let fname = "../day8_full.txt";
    let grid: Vec<Vec<char>> = std::fs::read_to_string(fname)
        .unwrap()
        .lines()
        .map(|l| l.chars().collect())
        .collect();
    dbg!(solve(&grid, false));
    dbg!(solve(&grid, true));
}

fn solve(grid: &[Vec<char>], part2: bool) -> usize {
    let mut anntennae: BTreeMap<char, Vec<_>> = BTreeMap::new();
    for (y, line) in grid.iter().enumerate() {
        for (x, ch) in line.iter().copied().enumerate() {
            if ch != '.' {
                anntennae
                    .entry(ch)
                    .or_default()
                    .push(IVec2::new(x as i32, y as i32));
            }
        }
    }
    let mut res = HashSet::new();

    for (_freq, locs) in anntennae {
        for pair in locs.iter().permutations(2) {
            let n1 = pair[0];
            let n2 = pair[1];
            if part2 {
                for i in 0..100 {
                    res.insert(n1 + (n2 - n1) * i);
                }
            } else {
                res.insert(n1 + (n2 - n1) * 2);
            }
        }
    }
    res.iter()
        .filter(|v| {
            (0..grid[0].len() as i32).contains(&v.x) && (0..grid.len() as i32).contains(&v.y)
        })
        .count()
}
