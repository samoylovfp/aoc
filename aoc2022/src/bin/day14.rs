use std::{collections::HashMap, fs::read_to_string};

use ndarray::Array2;

fn main() {
    let input = read_to_string("input14.txt").unwrap();
    let mut space = HashMap::new();
    for rock_line in input.lines() {
        let mut prev: Option<(i32, i32)> = None;
        for point in rock_line.split(" -> ") {
            let (x, y) = point.split_once(",").unwrap();
            let x: i32 = x.parse().unwrap();
            let y: i32 = y.parse().unwrap();
            if let Some((prev_x, prev_y)) = prev {
                let start_x = prev_x.min(x);
                let start_y = prev_y.min(y);
                let end_x = prev_x.max(x);
                let end_y = prev_y.max(y);
                for iy in start_y..=end_y {
                    for ix in start_x..=end_x {
                        space.insert((ix, iy), Cell::Rock);
                    }
                }
            }
            prev = Some((x, y));
        }
    }
    let mut sandy_space = space.clone();
    dbg!(part1(&mut sandy_space));
    dbg!(part2(&mut sandy_space));
}

fn part1(space: &mut HashMap<(i32, i32), Cell>) -> usize {
    let lowest_rock = space.keys().map(|(_x, y)| y).max().copied().unwrap();
    assert!(lowest_rock > 0);
    'cave: loop {
        let mut sand_unit = (500, 0);
        loop {
            let mut next_pos = sand_unit;
            if next_pos.1 > lowest_rock {
                break 'cave;
            }
            next_pos.1 += 1;
            if !space.contains_key(&next_pos) {
                sand_unit = next_pos;
                continue;
            }
            next_pos.0 -= 1;
            if !space.contains_key(&next_pos) {
                sand_unit = next_pos;
                continue;
            }
            next_pos.0 += 2;
            if !space.contains_key(&next_pos) {
                sand_unit = next_pos;
                continue;
            }
            space.insert(sand_unit, Cell::Sand);
            break;
        }
    }

    space.values().filter(|c| matches!(c, Cell::Sand)).count()
}

fn part2(space: &mut HashMap<(i32, i32), Cell>) -> usize {
    let lowest_rock = space.keys().map(|(_x, y)| y).max().copied().unwrap();
    let floor = lowest_rock + 2;
    assert!(lowest_rock > 0);
    'cave: loop {
        let mut sand_unit = (500, 0);
        if space.contains_key(&sand_unit) {
            break 'cave;
        }
        loop {
            let mut next_pos = sand_unit;
            next_pos.1 += 1;
            if next_pos.1 >= floor {
                space.insert(sand_unit, Cell::Sand);
                break;
            }
            if !space.contains_key(&next_pos) {
                sand_unit = next_pos;
                continue;
            }
            next_pos.0 -= 1;
            if !space.contains_key(&next_pos) {
                sand_unit = next_pos;
                continue;
            }
            next_pos.0 += 2;
            if !space.contains_key(&next_pos) {
                sand_unit = next_pos;
                continue;
            }
            space.insert(sand_unit, Cell::Sand);
            break;
        }
    }

    space.values().filter(|c| matches!(c, Cell::Sand)).count()
}

fn print_space(space: &HashMap<(i32, i32), Cell>) {
    for y in 0..15 {
        for x in 490..510 {
            match space.get(&(x, y)) {
                Some(Cell::Rock) => print!("#"),
                Some(Cell::Sand) => print!("o"),
                _ => print!("."),
            }
        }
        println!();
    }
}

#[derive(Clone, Copy)]
enum Cell {
    Rock,
    Sand,
}
