mod tasks;

use eframe::egui::{self, ScrollArea};
use tasks::{get_solvers, AocSolver};

fn main() {
    #[cfg(target_arch = "wasm32")]
    {
        console_error_panic_hook::set_once();
        tracing_wasm::set_as_global_default();

        eframe::start_web(
            "egui",
            eframe::WebOptions::default(),
            Box::new(|cc| Box::new(MyEguiApp::new(cc))),
        )
        .unwrap();
    }

    #[cfg(not(target_arch = "wasm32"))]
    {
        tracing_subscriber::fmt::init();
        eframe::run_native(
            "AoC",
            eframe::NativeOptions::default(),
            Box::new(|cc| Box::new(MyEguiApp::new(cc))),
        )
    }
}

#[derive(Default)]
struct MyEguiApp {
    solvers: Vec<Box<dyn AocSolver>>,
    selected_solver: usize,
    input: String,
}

impl MyEguiApp {
    fn new(cc: &eframe::CreationContext<'_>) -> Self {
        Self {
            solvers: get_solvers(),
            selected_solver: 0,
            input: String::new(),
        }
    }
}

impl eframe::App for MyEguiApp {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        egui::TopBottomPanel::top("select task").show(ctx, |ui| {
            egui::menu::menu_button(ui, "Select day", |ui| {
                for (i, solver) in self.solvers.iter().enumerate() {
                    if ui
                        .selectable_label(self.selected_solver == i, solver.title())
                        .clicked()
                    {
                        self.selected_solver = i;
                    }
                }
            })
        });

        egui::SidePanel::left("input").show(ctx, |ui| {
            ui.label("Type or paste your input here");
            ScrollArea::vertical().show(ui, |ui| {
                ui.text_edit_multiline(&mut self.input);
            })
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            let solver = self.solvers.get_mut(self.selected_solver).unwrap();
            ui.heading(solver.title());
            solver.show(&self.input, ui);
        });
    }
}
