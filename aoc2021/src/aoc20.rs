use std::{
    collections::{HashMap, HashSet},
    fs::read_to_string,
    mem::swap,
};

fn main() {
    let input = read_to_string("input20.txt").unwrap();
    let mut lines = input.lines();
    let enhancement: HashSet<usize> = lines
        .next()
        .unwrap()
        .trim()
        .chars()
        .enumerate()
        .filter_map(|(i, c)| (c == '#').then(|| i))
        .collect();

    let _empty_line = lines.next();
    let mut other_pixels = false;
    let mut image: HashMap<(i32, i32), bool> = lines
        .enumerate()
        .flat_map(|(y, line)| {
            line.trim()
                .chars()
                .enumerate()
                .map(move |(x, char)| ((x as i32, y as i32), char == '#'))
        })
        .collect();

    for iteration in 0..50 {
        let mut new_image = HashMap::new();

        let top_left = image.keys().min().unwrap();
        let bottom_right = image.keys().max().unwrap();

        if iteration == 2 {
            println!("First part {}", image.values().filter(|x| **x).count());
        }

        for y in (top_left.1 - 3)..=(bottom_right.1 + 3) {
            for x in (top_left.0 - 3)..=(bottom_right.0 + 3) {
                let bits = get_bit_mask(&image, other_pixels, (x, y));
                let new_bit = enhancement.contains(&number_from_bits(bits));
                new_image.insert((x, y), new_bit);
                // if new_bit {
                //     print!("#");
                // } else {
                //     print!(".");
                // }
            }
            // println!();
        }
        other_pixels = enhancement.contains(&number_from_bits([other_pixels; 9]));
        swap(&mut new_image, &mut image);
    }
    println!("Second part {}", image.values().filter(|x| **x).count());
}

fn get_bit_mask(
    image: &HashMap<(i32, i32), bool>,
    outer_space_bit: bool,
    (cx, cy): (i32, i32),
) -> [bool; 9] {
    let values: Vec<bool> = (-1..=1)
        .flat_map(|y| {
            (-1..=1).map(move |x| *image.get(&(cx + x, cy + y)).unwrap_or(&outer_space_bit))
        })
        .collect();

    values.try_into().unwrap()
}

fn number_from_bits(bits: [bool; 9]) -> usize {
    bits.into_iter()
        .rev()
        .enumerate()
        .map(|(i, bit)| if bit { 1 << i } else { 0 })
        .sum()
}

#[test]
fn test_number_from_bits() {
    assert_eq!(number_from_bits([false; 9]), 0);
    let mut bits = [false; 9];
    bits[8 - 2] = true;
    bits[8 - 1] = true;
    assert_eq!(number_from_bits(bits), 6)
}
