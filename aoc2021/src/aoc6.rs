use std::{collections::VecDeque, fs::read_to_string};

fn main() {
    let input = read_to_string("input6.txt").unwrap();
    let mut fish = FishCounts::parse(&input);
    (0..80).for_each(|_| fish.advance());
    println!("First part is {}", fish.0.iter().sum::<u128>());
    (80..256).for_each(|_| fish.advance());
    println!("Second part is {}", fish.0.iter().sum::<u128>())
}

struct FishCounts(VecDeque<u128>);

impl FishCounts {
    fn parse(input: &str) -> Self {
        let mut result = VecDeque::from(vec![0; 9]);
        for fish_age in input.trim().split(',').map(|s| s.parse::<usize>().unwrap()) {
            result[fish_age] += 1;
        }
        FishCounts(result)
    }

    fn advance(&mut self) {
        let births = self.0.pop_front().unwrap();
        self.0[6] += births;
        self.0.push_back(births);
    }
}

#[test]
fn test_provided() {
    let mut fish_counts = FishCounts::parse("3,4,3,1,2");
    assert_eq!(fish_counts.0, [0, 1, 1, 2, 1, 0, 0, 0, 0]);
    fish_counts.advance();
    assert_eq!(fish_counts.0, [1, 1, 2, 1, 0, 0, 0, 0, 0]);
    fish_counts.advance();
    assert_eq!(fish_counts.0, [1, 2, 1, 0, 0, 0, 1, 0, 1]);
    fish_counts.advance();
    assert_eq!(fish_counts.0, [2, 1, 0, 0, 0, 1, 1, 1, 1]);
    fish_counts.advance();
    assert_eq!(fish_counts.0, [1, 0, 0, 0, 1, 1, 3, 1, 2]);
}
