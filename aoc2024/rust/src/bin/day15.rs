use aoc_rust::Grid;
use glam::IVec2;

fn main() {
    let mut grid = Grid::from_file_chars("day15_map.txt");
    let mut robot_pos = grid
        .iter_cells()
        .find_map(|(pos, c)| (*c == '@').then_some(pos))
        .unwrap();
    *grid.get_mut(robot_pos).unwrap() = '.';

    for instr in std::fs::read_to_string("day15_instr.txt").unwrap().chars() {
        if instr.is_whitespace() {
            continue;
        }
        let vector: IVec2 = match instr {
            '>' => [1, 0],
            '<' => [-1, 0],
            '^' => [0, -1],
            'v' => [0, 1],
            _ => panic!("Unknown instruction"),
        }
        .into();

        let robot_destination = robot_pos + vector;

        let empty_space = 'empty: {
            for i in 1.. {
                let pos = robot_pos + vector * i;
                match grid.get(pos) {
                    Some('.') => break 'empty Some(pos),
                    Some('#') | None => break,
                    _ => {}
                }
            }
            None
        };

        if let Some(pos) = empty_space {
            grid.swap(robot_destination, pos);
            robot_pos = robot_destination;
        }
    }

    dbg!(grid
        .iter_cells()
        .filter_map(|(coord, cell)| (*cell == 'O').then_some(coord.y * 100 + coord.x))
        .sum::<i32>());
}
