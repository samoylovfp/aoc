use std::{fs::read_to_string, time::Instant};

#[derive(Debug)]
struct Range {
    dest_start: u64,
    source_start: u64,
    length: u64,
}

#[derive(Debug)]
struct SeedMap(Vec<Range>);

fn main() {
    let input = read_to_string("input05.txt").unwrap();
    // let input = read_to_string("sample05.txt").unwrap();
    let seeds: Vec<u64> = input
        .lines()
        .filter_map(|l| l.strip_prefix("seeds: "))
        .next()
        .unwrap()
        .split_ascii_whitespace()
        .map(|s| s.parse().unwrap())
        .collect();

    let maps: Vec<SeedMap> = input
        .match_indices("map:\n")
        .map(|(i, _)| {
            input[i..]
                .lines()
                .skip(1)
                .take_while(|l| !l.is_empty())
                .map(|l| {
                    let numbers: Vec<u64> = l
                        .split_ascii_whitespace()
                        .map(|n| n.parse().unwrap())
                        .collect();
                    Range {
                        dest_start: numbers[0],
                        source_start: numbers[1],
                        length: numbers[2],
                    }
                })
                .collect()
        })
        .map(SeedMap)
        .collect();

    let mut transformed_seeds = seeds.clone();
    for seed in &mut transformed_seeds {
        print!("{seed}");
        for map in &maps {
            *seed = 'seed: {
                for range in &map.0 {
                    if (range.source_start..(range.source_start + range.length)).contains(seed) {
                        break 'seed *seed + range.dest_start - range.source_start;
                    }
                }
                *seed
            };
            print!(" -> {seed}");
        }
        println!();
    }

    println!("Part1: {}", transformed_seeds.into_iter().min().unwrap());

    let mut global_min = u64::MAX;

    let mut start = Instant::now();

    for ch in seeds.chunks_exact(2) {
        let min = (ch[0]..(ch[0] + ch[1]))
            .map(|mut seed| {
                for map in &maps {
                    seed = 'seed: {
                        for range in &map.0 {
                            // if (range.source_start..(range.source_start + range.length))
                            // .contains(&seed)
                            if seed >= range.source_start
                                && seed < range.source_start + range.length
                            {
                                break 'seed seed + range.dest_start - range.source_start;
                            }
                        }
                        seed
                    };
                    // print!(" -> {seed}");
                }
                seed
            })
            .min()
            .unwrap();
        if start.elapsed().as_secs() > 0 {
            println!("{} done", ch[0]);
            start = Instant::now();
        }
        global_min = global_min.min(min);
    }

    println!("Part2: {global_min}")
}
