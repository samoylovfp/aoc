#![allow(clippy::needless_collect)]
// I need to report this to the clippy team, the collect is needed to avoid ownership issues

use std::fs::read_to_string;

fn main() {
    let input = read_to_string("input16.txt").unwrap();

    dbg!(part_one(&input));
    dbg!(part_two(&input));
}

#[derive(Debug, PartialEq)]
enum PacketContents {
    SubPackets(Vec<Packet>),
    Literal(u128),
}

#[derive(Debug, PartialEq)]
struct Packet {
    version: u8,
    type_id: u8,
    contents: PacketContents,
}

impl Packet {
    fn parse(bit_stream: &mut impl Iterator<Item = u8>) -> Self {
        let version = bit_stream.take_bits(3) as u8;
        let type_id = bit_stream.take_bits(3) as u8;

        let contents = if type_id == 4 {
            let mut continuation = 1;
            let mut result = 0;
            while continuation == 1 {
                continuation = bit_stream.take_bits(1);
                result = (result << 4) + bit_stream.take_bits(4);
            }
            PacketContents::Literal(result)
        } else {
            let mut sub_packets = vec![];
            let length_mode_bit = bit_stream.take_bits(1);

            if length_mode_bit == 0 {
                let total_length = bit_stream.take_bits(15);
                let sub_packet_bits: Vec<u8> = (0..total_length)
                    .map(|_| bit_stream.next().unwrap())
                    .collect();
                let mut sub_packets_bits_iterator = sub_packet_bits.into_iter().peekable();
                while sub_packets_bits_iterator.peek().is_some() {
                    sub_packets.push(Packet::parse(&mut sub_packets_bits_iterator));
                }
            } else {
                let sub_packet_amount = bit_stream.take_bits(11);
                for _p in 0..sub_packet_amount {
                    sub_packets.push(Packet::parse(bit_stream));
                }
            }
            PacketContents::SubPackets(sub_packets)
        };

        Packet {
            version,
            type_id,
            contents,
        }
    }

    fn sum_versions(&self) -> u32 {
        self.version as u32
            + if let PacketContents::SubPackets(ref p) = self.contents {
                p.iter().map(|p| p.sum_versions()).sum()
            } else {
                0
            }
    }

    fn calculate_value(&self) -> u128 {
        let bool_to_int = |v| if v { 1 } else { 0 };
        match self.contents {
            PacketContents::SubPackets(ref p) => match self.type_id {
                0 => p.iter().map(|p| p.calculate_value()).sum(),
                1 => p.iter().map(|p| p.calculate_value()).product(),
                2 => p.iter().map(|p| p.calculate_value()).min().unwrap(),
                3 => p.iter().map(|p| p.calculate_value()).max().unwrap(),
                5 => bool_to_int(p[0].calculate_value() > p[1].calculate_value()),
                6 => bool_to_int(p[0].calculate_value() < p[1].calculate_value()),
                7 => bool_to_int(p[0].calculate_value() == p[1].calculate_value()),

                tid => panic!("Unknown type id {:?}", tid),
            },
            PacketContents::Literal(n) => n,
        }
    }
}

fn part_one(input: &str) -> u32 {
    let mut bit_stream = split_to_bits(input);
    let root_packet = Packet::parse(&mut bit_stream);

    root_packet.sum_versions()
}

fn part_two(input: &str) -> u128 {
    let mut bit_stream = split_to_bits(input);
    let root_packet = Packet::parse(&mut bit_stream);
    root_packet.calculate_value()
}

fn split_to_bits(input: &str) -> impl Iterator<Item = u8> {
    let chars: Vec<char> = input.chars().collect();
    chars
        .into_iter()
        .flat_map(|c| c.to_digit(16).unwrap().to_bits())
}

trait ToBits {
    fn to_bits(&self) -> [u8; 4];
}

impl ToBits for u32 {
    #[allow(clippy::identity_op)]
    fn to_bits(&self) -> [u8; 4] {
        [
            ((self >> 3) % 2) as u8,
            ((self >> 2) % 2) as u8,
            ((self >> 1) % 2) as u8,
            ((self >> 0) % 2) as u8,
        ]
    }
}

trait FromBits {
    fn take_bits(&mut self, bits: usize) -> u128;
}

impl<T: Iterator<Item = u8>> FromBits for T {
    fn take_bits(&mut self, bits: usize) -> u128 {
        let mut result = 0;
        for bit_n in (0..bits).rev() {
            result += self.next().unwrap() as u128 * (1_u128 << bit_n as u128)
        }

        result
    }
}

#[test]
fn test_to_bits() {
    assert_eq!(3u32.to_bits(), [0, 0, 1, 1]);
    assert_eq!(8u32.to_bits(), [1, 0, 0, 0]);
    assert_eq!(15u32.to_bits(), [1, 1, 1, 1]);
}

#[test]
fn test_from_bits() {
    assert_eq!([1, 0, 0].into_iter().take_bits(3), 4);
    assert_eq!([1, 1, 0, 1, 0, 1, 0].into_iter().take_bits(2), 3);
}

#[test]
fn test_literal() {
    assert_eq!(
        Packet::parse(&mut split_to_bits("D2FE28")),
        Packet {
            version: 6,
            type_id: 4,
            contents: PacketContents::Literal(2021)
        }
    );
}

#[test]
fn test_operator() {
    use PacketContents::*;
    assert_eq!(
        Packet::parse(&mut split_to_bits("38006F45291200")),
        Packet {
            version: 1,
            type_id: 6,
            contents: SubPackets(vec![
                Packet {
                    version: 6,
                    type_id: 4,
                    contents: Literal(10)
                },
                Packet {
                    version: 2,
                    type_id: 4,
                    contents: Literal(20)
                }
            ])
        }
    )
}

#[test]
fn test_operator2() {
    use PacketContents::*;
    assert_eq!(
        Packet::parse(&mut split_to_bits("EE00D40C823060")),
        Packet {
            version: 7,
            type_id: 3,
            contents: SubPackets(vec![
                Packet {
                    version: 2,
                    type_id: 4,
                    contents: Literal(1)
                },
                Packet {
                    version: 4,
                    type_id: 4,
                    contents: Literal(2)
                },
                Packet {
                    version: 1,
                    type_id: 4,
                    contents: Literal(3)
                }
            ])
        }
    )
}

#[test]
fn test_smoke_operators() {
    assert_eq!(part_one("8A004A801A8002F478"), 16);
    assert_eq!(part_one("620080001611562C8802118E34"), 12);
    assert_eq!(part_one("C0015000016115A2E0802F182340"), 23);
    assert_eq!(part_one("A0016C880162017C3686B18A3D4780"), 31);
}

#[test]
fn test_smoke_p2() {
    assert_eq!(part_two("C200B40A82"), 3);
}
