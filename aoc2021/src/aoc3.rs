#![allow(mixed_script_confusables)]

use std::fs::read_to_string;

fn main() {
    let input = read_to_string("input3.txt").unwrap();
    let input_bits = calculate_bits(&input);

    let (γ, ε) = ints_from_most_and_least_frequent_bits(&input_bits);
    println!("First part is {}", γ * ε);
    let (oxygen_gen, co2_scrub) = filter_by_bits(&input_bits);
    println!("Second part is {}", oxygen_gen * co2_scrub)
}

fn calculate_bits(input: &str) -> Vec<Vec<u8>> {
    input
        .trim()
        .lines()
        .map(|line| {
            line.trim()
                .chars()
                .map(|c| match c {
                    '0' => 0,
                    '1' => 1,
                    _ => panic!(),
                })
                .collect()
        })
        .collect()
}

fn ints_from_most_and_least_frequent_bits(input_bits: &[Vec<u8>]) -> (u32, u32) {
    let line_counts = input_bits.len();

    let one_bit_counts =
        input_bits
            .iter()
            .fold(vec![0u32; input_bits[0].len()], |mut accum, number| {
                for (accum_count, bit) in accum.iter_mut().zip(number) {
                    *accum_count += *bit as u32;
                }
                accum
            });

    // Reverse to have least significant bit first
    let from_most_frequent = one_bit_counts
        .iter()
        .rev()
        .enumerate()
        .map(|(i, one_count)| {
            if *one_count as usize > line_counts / 2 {
                1 << i
            } else {
                0
            }
        })
        .sum();

    // Bit inversion
    let mask = (1 << input_bits[0].len()) - 1;
    let from_least_frequent = mask - from_most_frequent;

    (from_most_frequent, from_least_frequent)
}

fn filter_by_bits(numbers_bits: &[Vec<u8>]) -> (u32, u32) {
    let number_bit_len = numbers_bits[0].len();
    let mut numbers_with_most_frequent_bits = numbers_bits.to_vec();

    for bit_number in 0..number_bit_len {
        let number_count = numbers_with_most_frequent_bits.len();
        let one_count: usize = numbers_with_most_frequent_bits
            .iter()
            .map(|line| line[bit_number] as usize)
            .sum();

        let half = number_count / 2 + number_count % 2;

        let retain_with = if one_count >= half { 1 } else { 0 };
        numbers_with_most_frequent_bits.retain(|line| line[bit_number] == retain_with);
        if numbers_with_most_frequent_bits.len() == 1 {
            break;
        }
    }

    let mut numbers_with_least_frequent_bits = numbers_bits.to_vec();

    for bit_number in 0..number_bit_len {
        let number_count = numbers_with_least_frequent_bits.len();
        let one_count: usize = numbers_with_least_frequent_bits
            .iter()
            .map(|line| line[bit_number] as usize)
            .sum();
        let half = number_count / 2 + number_count % 2;

        let retain_with = if one_count < half { 1 } else { 0 };
        numbers_with_least_frequent_bits.retain(|line| line[bit_number] == retain_with);
        if numbers_with_least_frequent_bits.len() == 1 {
            break;
        }
    }

    assert!(numbers_with_most_frequent_bits.len() == 1);
    assert!(numbers_with_least_frequent_bits.len() == 1);

    (
        bits_to_num(numbers_with_most_frequent_bits.pop().unwrap()),
        bits_to_num(numbers_with_least_frequent_bits.pop().unwrap()),
    )
}

fn bits_to_num(b: Vec<u8>) -> u32 {
    b.iter()
        .rev()
        .enumerate()
        .map(|(i, b)| (*b as u32) << i)
        .sum()
}

#[test]
fn test_most_frequent_bits() {
    let bits = calculate_bits(
        r#"
            00100
            11110
            10110
            10111
            10101
            01111
            00111
            11100
            10000
            11001
            00010
            01010
        "#,
    );
    assert_eq!(ints_from_most_and_least_frequent_bits(&bits), (22, 9));

    assert_eq!(filter_by_bits(&bits), (23, 10))
}

#[test]
fn test_bits_to_num() {
    assert_eq!(bits_to_num(vec![1, 0, 1, 1, 1]), 23)
}
