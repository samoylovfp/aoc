use nom::{
    branch::alt,
    bytes::complete::*,
    sequence::{delimited, separated_pair},
    IResult, Parser,
};

fn main() {
    dbg!(parse_only_mul(
        &std::fs::read_to_string("day3.txt").unwrap()
    ));
    dbg!(parse_do_mul(&std::fs::read_to_string("day3.txt").unwrap()));
}

fn parse_only_mul(input: &str) -> u64 {
    let dec = || take_while(|c: char| c.is_digit(10));
    let mut mul = delimited(
        tag::<&str, &str, ()>("mul("),
        separated_pair(dec(), tag(","), dec()),
        tag(")"),
    );

    let mut res = 0;
    for idx in 0..input.len() {
        if let Ok((_i, (val1, val2))) = mul(&input[idx..]) {
            let val1: u64 = val1.parse().unwrap();
            let val2: u64 = val2.parse().unwrap();
            res += val1 * val2;
        }
    }
    res
}

enum Instr {
    Do,
    Dont,
    Mul(u64, u64),
}

fn parse_do_mul(input: &str) -> u64 {
    let dec = || take_while(|c: char| c.is_digit(10));
    let mul = delimited(
        tag::<&str, &str, ()>("mul("),
        separated_pair(dec(), tag(","), dec()),
        tag(")"),
    );
    let do_ = tag("do()");
    let dont = tag("don't()");
    let mut res = 0;
    let mut enabled = true;
    let mut combined_parser = alt((
        do_.map(|_| Instr::Do),
        dont.map(|_| Instr::Dont),
        mul.map(|(v1, v2)| Instr::Mul(v1.parse().unwrap(), v2.parse().unwrap())),
    ));
    for idx in (0..input.len()) {
        if let Ok((_, intstr)) = combined_parser(&input[idx..]) {
            match intstr {
                Instr::Do => enabled = true,
                Instr::Dont => enabled = false,
                Instr::Mul(v1, v2) => {
                    if enabled {
                        res += v1 * v2;
                    }
                }
            }
        }
    }
    res
}
