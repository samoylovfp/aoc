use std::fs::read_to_string;

fn main() {
    let input = read_to_string("input2.txt").unwrap();

    dbg!(part_one(&input));
    dbg!(part_two(&input));
}

fn part_one(input: &str) -> u32 {
    let mut pos = (0, 0);
    for line in input.lines() {
        let mut words = line.split_ascii_whitespace();
        let verb = words.next().unwrap();
        let value: u32 = words.next().unwrap().parse().unwrap();

        match verb {
            "forward" => pos.0 += value,
            "down" => pos.1 += value,
            "up" => pos.1 -= value,
            _ => panic!(),
        }
    }
    pos.0 * pos.1
}

fn part_two(input: &str) -> u32 {
    let mut pos = (0, 0);
    let mut aim = 0;

    for line in input.lines() {
        let mut words = line.split_ascii_whitespace();
        let verb = words.next().unwrap();
        let value: u32 = words.next().unwrap().parse().unwrap();

        match verb {
            "down" => aim += value,
            "up" => aim -= value,

            "forward" => {
                pos.0 += value;
                pos.1 += value * aim;
            }
            _ => panic!(),
        }
    }

    pos.0 * pos.1
}
