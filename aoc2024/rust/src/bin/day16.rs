use std::collections::{BTreeSet, HashMap, HashSet};

use aoc_rust::Grid;
use glam::IVec2;

fn main() {
    let grid = Grid::from_file_chars("day16.txt");
    dbg!(p1(&grid));
}

enum Action {
    Forward,
    TurnCw,
    TurnCcw,
}

#[derive(PartialEq, PartialOrd, Eq, Ord)]
enum Direction {
    East,
    South,
    North,
    West,
}

struct Node {
    score: u64,
    pos: IVec2,
    dir: Direction,
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.score.eq(&other.score)
    }
}

impl Eq for Node {}
impl Ord for Node {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.score.partial_cmp(&other.score)
    }
}

fn p1(g: &Grid<char>) -> String {
    let start = g
        .iter_cells()
        .find_map(|(crd, cell)| (*cell == 'S').then_some(crd))
        .unwrap();

    // let mut path = HashMap::new();

    // let mut known_node_costs = HashMap::new();
    // let mut heuristic_costs = HashMap::new();
    let mut open_set = BTreeSet::new();
    open_set.insert(Node {
        score: 0,
        pos: start,
        dir: Direction::East,
    });

    while let Some(node) = open_set.pop_first() {
        todo!()
    }

    todo!()
}
