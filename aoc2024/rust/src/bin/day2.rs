fn main() {
    let data = std::fs::read_to_string("day2.txt").unwrap();

    let reports: Vec<Vec<u16>> = data
        .lines()
        .map(|l| l.split_whitespace().map(|v| v.parse().unwrap()).collect())
        .collect();

    dbg!(reports.iter().filter(|r| safe(&[], r)).count());
    dbg!(reports.iter().filter(|r| safe_damp(r)).count());
}

fn safe(p1: &[u16], p2: &[u16]) -> bool {
    let mut ascending = None;
    let combined = p1.iter().chain(p2);
    for (val1, val2) in combined.clone().zip(combined.skip(1)) {
        match ascending {
            None => ascending = Some(val1 < val2),
            Some(a) => {
                if a != (val1 < val2) {
                    return false;
                }
            }
        }
        let diff = val1.abs_diff(*val2);
        if diff > 3 || diff < 1 {
            return false;
        }
    }

    true
}

fn safe_damp(rep: &[u16]) -> bool {
    safe(&[], &rep) || (0..rep.len()).any(|i| safe(&rep[..i], &rep[i + 1..]))
}
