open Printf

let parse_reports ch =
  ch |> In_channel.input_lines
  |> List.map (fun l -> String.split_on_char ' ' l |> List.map int_of_string)

let is_safe (report : int list) =
  let rec inner (increasing : bool) = function
    | e1 :: e2 :: rest ->
        let diff = abs (e2 - e1) in
        e1 < e2 == increasing
        && diff >= 1 && diff <= 3
        && inner increasing (e2 :: rest)
    | _ -> true
  in
  match report with e1 :: e2 :: rest -> inner (e1 < e2) report | _ -> false

let is_damp_safe (report : int list) : bool =
  let rec iter_skips pre = function
    | e1 :: rest -> is_safe (pre @ rest) || iter_skips (pre @ [ e1 ]) rest
    | [] -> is_safe pre
  in
  is_safe report || iter_skips [] report

let part1 reports = List.filter is_safe reports |> List.length
let part2 reports = List.filter is_damp_safe reports |> List.length

let () =
  let reports = "day2.txt" |> open_in |> parse_reports in
  reports |> part1 |> printf "Part1: %d\n";
  reports |> part2 |> printf "Part2: %d\n"
