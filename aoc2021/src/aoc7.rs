use std::{fs::read_to_string, time::Instant};

fn main() {
    let start = Instant::now();
    let input = read_to_string("input7.txt").unwrap();
    let initial_positions: Vec<i64> = input
        .trim()
        .split(',')
        .map(|p| p.parse().unwrap())
        .collect();
    println!("Read and parsed input in {:?}", start.elapsed());
    let start = Instant::now();
    dbg!(first_part(&initial_positions));
    println!("First part in {:?}", start.elapsed());
    let start = Instant::now();
    dbg!(second_part(&initial_positions));
    println!("Second part in {:?}", start.elapsed());
}

fn first_part(positions: &[i64]) -> i64 {
    let start = *positions.iter().min().unwrap();
    let end = *positions.iter().max().unwrap();
    (start..=end)
        .map(|target| positions.iter().map(|p| (p - target).abs()).sum::<i64>())
        .min()
        .unwrap()
}

fn second_part(positions: &[i64]) -> i64 {
    let start = *positions.iter().min().unwrap();
    let end = *positions.iter().max().unwrap();
    (start..=end)
        .map(|target| {
            positions
                .iter()
                .map(|p| {
                    let total_steps = (p - target).abs();
                    (0..=total_steps).sum::<i64>()
                })
                .sum::<i64>()
        })
        .min()
        .unwrap()
}

#[test]
fn test_provided() {
    assert_eq!(first_part(&[16, 1, 2, 0, 4, 2, 7, 1, 2, 14]), 37);
    assert_eq!(second_part(&[16, 1, 2, 0, 4, 2, 7, 1, 2, 14]), 168)
}
