use std::collections::BTreeMap;

fn main() {
    let input: Vec<u64> = std::fs::read_to_string("../day11_full.txt")
        .unwrap()
        .split_whitespace()
        .map(|v| v.parse().unwrap())
        .collect();

    dbg!(count_stones_dedup(&input, 25));
    dbg!(count_stones_dedup(&input, 75));
}

fn step(stone: u64) -> (u64, Option<u64>) {
    if stone == 0 {
        (1, None)
    } else {
        let number_of_digits = stone.ilog10() + 1;
        if number_of_digits % 2 == 0 {
            let mask = 10_u64.pow(number_of_digits / 2);
            (stone / mask, Some(stone % mask))
        } else {
            (stone * 2024, None)
        }
    }
}

fn count_stones_dedup(stones: &[u64], steps: u64) -> u64 {
    let mut stone_counts = BTreeMap::<u64, u64>::new();
    let mut second_buffer = BTreeMap::new();
    for s in stones {
        *stone_counts.entry(*s).or_default() += 1;
    }
    for i in 0..steps {
        second_buffer.clear();

        for (s, count) in &stone_counts {
            let (s, new_s) = step(*s);
            *second_buffer.entry(s).or_default() += count;
            if let Some(s) = new_s {
                *second_buffer.entry(s).or_default() += count;
            }
        }
        std::mem::swap(&mut second_buffer, &mut stone_counts);
    }
    stone_counts.values().sum()
}
