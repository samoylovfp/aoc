use std::fs::read_to_string;

fn main() {
    let (numbers, boards) = parse_boards(&read_to_string("input4.txt").unwrap());
    dbg!(part_one(&numbers, &boards));
    dbg!(part_two(&numbers, &boards));
}

#[derive(Clone)]
struct Cell {
    number: u8,
    marked: bool,
}

impl Cell {
    fn new(n: u8) -> Self {
        Self {
            number: n,
            marked: false,
        }
    }
}

#[derive(Clone)]
struct Board(Vec<Vec<Cell>>);

impl Board {

    #[cfg(test)]
    fn draw(&self) -> String {
        self.0
            .iter()
            .map(|line| {
                line.iter()
                    .map(|c| c.number.to_string())
                    .collect::<Vec<String>>()
                    .join(" ")
            })
            .collect::<Vec<String>>()
            .join("\n")
    }

    fn mark(&mut self, number: u8) {
        self.0.iter_mut().for_each(|line| {
            line.iter_mut().for_each(|c| {
                if c.number == number {
                    c.marked = true
                }
            })
        })
    }

    fn won(&self) -> bool {
        let max_x = self.0[0].len();
        let max_y = self.0.len();

        // any rows won
        if (0..max_y).any(|y| (0..max_x).all(|x| self.0[y][x].marked)) {
            return true;
        }
        // any columns won
        if (0..max_x).any(|x| (0..max_y).all(|y| self.0[y][x].marked)) {
            return true;
        }

        false
    }

    #[cfg(test)]
    fn reset(&mut self) {
        self.0
            .iter_mut()
            .for_each(|row| row.iter_mut().for_each(|c| c.marked = false))
    }

    fn sum_unmarked(&self) -> u32 {
        self.0
            .iter()
            .map(|row| {
                row.iter()
                    .filter_map(|c| (!c.marked).then(|| c.number as u32))
                    .sum::<u32>()
            })
            .sum()
    }
}

fn parse_boards(input: &str) -> (Vec<u8>, Vec<Board>) {
    let mut lines = input.trim().lines();
    let numbers = lines
        .next()
        .unwrap()
        .split(',')
        .map(|n| n.parse().unwrap())
        .collect();

    let _empty_line = lines.next();

    let mut boards = vec![];
    let mut board = vec![];

    for line in lines {
        if line.is_empty() {
            boards.push(Board(board));
            board = vec![];
        } else {
            let line = line
                .split_whitespace()
                .map(|n| Cell::new(n.parse().unwrap()))
                .collect();
            board.push(line);
        }
    }

    boards.push(Board(board));

    (numbers, boards)
}

fn part_one(numbers: &[u8], boards: &[Board]) -> u32 {
    let mut boards = boards.to_vec();
    for n in numbers {
        for b in &mut boards {
            b.mark(*n);
            if b.won() {
                return b.sum_unmarked() * (*n as u32);
            }
        }
    }

    panic!()
}

fn part_two(numbers: &[u8], boards: &[Board]) -> u32 {
    let mut boards = boards.to_vec();

    for n in numbers {
        let mut last_won = None;
        for b in boards.as_mut_slice() {
            if b.won() {
                continue;
            }
            b.mark(*n);
            if b.won() && last_won.is_none() {
                last_won = Some(b.clone());
            }
        }
        if boards.iter().all(|b| b.won()) {
            return last_won.unwrap().sum_unmarked() * (*n as u32);
        }
    }

    panic!()
}

#[test]
fn test_provided() {
    let (numbers, mut boards) = parse_boards(
        r#"
7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
    "#,
    );

    assert_eq!(numbers.first(), Some(&7_u8));
    assert_eq!(numbers.last(), Some(&1_u8));

    assert_eq!(boards.len(), 3);
    let mut b = boards.remove(0);
    assert_eq!(
        b.draw(),
        r#"
22 13 17 11 0
8 2 23 4 24
21 9 14 16 7
6 10 3 18 5
1 12 20 15 19
    "#
        .trim()
    );

    assert!(!b.won());
    [22, 13, 17, 11, 0].iter().for_each(|n| b.mark(*n));
    assert!(b.won());
    b.reset();
    assert!(!b.won());
    [22, 8, 21, 6, 1].iter().for_each(|n| b.mark(*n));
    assert!(b.won());
    b.reset();
    assert_eq!(part_one(&numbers, &boards), 4512);

    assert_eq!(part_two(&numbers, &boards), 1924);
}
