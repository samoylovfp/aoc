fn main() {
    dbg!(part_one(-57, -105));
    dbg!(part_two((206, 250, -105, -57)));
}

fn part_one(y_max: i32, y_min: i32) -> i32 {
    let mut max_speed = 0;
    for y_initial_vel in 0..1000 {
        let mut y_vel = y_initial_vel;
        let mut y = 0;
        let mut prev_y = 0;

        while y >= y_min {
            prev_y = y;
            y += y_vel;
            y_vel -= 1;
        }
        if prev_y <= y_max {
            max_speed = y_initial_vel;
        }
    }
    get_highest_y(max_speed)
}

fn part_two(zone: (i32, i32, i32, i32)) -> usize {
    let mut trajectory_count = 0;
    for y_initial_vel in -200..200 {
        print!("{:04}", y_initial_vel);
        for x_initial_vel in 0..400 {
            if hits((x_initial_vel, y_initial_vel), zone) {
                print!("#");
                trajectory_count += 1;
            } else {
                print!(" ")
            }
        }
        println!();
    }
    trajectory_count
}

fn hits(
    (x_init_vel, y_init_vel): (i32, i32),
    (x_min, x_max, y_min, y_max): (i32, i32, i32, i32),
) -> bool {
    let mut position = (0, 0);
    let mut vel = (x_init_vel, y_init_vel);
    let mut prev_position = position;

    while position.0 <= x_max && position.1 >= y_min {
        prev_position = position;
        position.0 += vel.0;
        position.1 += vel.1;

        vel.0 = (vel.0 - 1).max(0);
        vel.1 -= 1;
    }

    (x_min..=x_max).contains(&prev_position.0) && (y_min..=y_max).contains(&prev_position.1)
}

fn get_highest_y(speed: i32) -> i32 {
    let mut y = 0;
    for speed in (0..=speed).rev() {
        y += speed;
    }

    y
}

#[test]
fn test_provided() {
    assert_eq!(part_two((20, 30, -10, -5)), 112);
    panic!();
}

#[test]
fn test_hits() {
    assert!(hits((8, 1), (20, 30, -10, -5)))
}
/*
23,-10  25,-9   27,-5   29,-6   22,-6   21,-7   9,0     27,-7   24,-5
25,-7   26,-6   25,-5   6,8     11,-2   20,-5   29,-10  6,3     28,-7
8,0     30,-6   29,-8   20,-10  6,7     6,4     6,1     14,-4   21,-6
26,-10  7,-1    7,7     8,-1    21,-9   6,2     20,-7   30,-10  14,-3
20,-8   13,-2   7,3     28,-8   29,-9   15,-3   22,-5   26,-8   25,-8
25,-6   15,-4   9,-2    15,-2   12,-2   28,-9   12,-3   24,-6   23,-7
25,-10  7,8     11,-3   26,-7   7,1     23,-9   6,0     22,-10  27,-6
8,1     22,-8   13,-4   7,6     28,-6   11,-4   12,-4   26,-9   7,4
24,-10  23,-8   30,-8   7,0     9,-1    10,-1   26,-5   22,-9   6,5
7,5     23,-6   28,-10  10,-2   11,-1   20,-9   14,-2   29,-7   13,-3
23,-5   24,-8   27,-9   30,-7   28,-5   21,-10  7,9     6,6     21,-5
27,-10  7,2     30,-9   21,-8   22,-7   24,-9   20,-6   6,9     29,-5
8,-2    27,-8   30,-5   24,-7
*/
