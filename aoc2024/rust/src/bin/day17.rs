use std::{
    collections::{BTreeSet, HashSet},
    time::Instant,
};

use itertools::Itertools;

type MachineWord = u64;

fn main() {
    #[rustfmt::skip]
    let mut m = Machine {
        reg_a: 30553366,
        reg_b: 0,
        reg_c: 0,
        instr_pointer: 0,
        output: vec![],
        program: vec![
            // b = a % 8
            2, 4,
            // b = b ^ 1
            1, 1,
            // c = a / 2.pow(b)
            7, 5,
            // b = b ^ c
            4, 7,
            // b = b ^ a
            1, 4,
            // a = a / 2.pow(3)
            0, 3,
            // print(b % 8)
            5, 5,
            // if a != 0 { goto 0}
            3, 0
        ],
    };
    m.run();
    println!(
        "part1: {}",
        m.output
            .into_iter()
            .map(|i| i.to_string())
            .collect_vec()
            .join(",")
    );
    let mut possible_a_values: BTreeSet<MachineWord> = BTreeSet::from_iter(0..7);

    let check = |a: MachineWord| {
        let mut m = Machine {
            reg_a: a,
            reg_b: 0,
            reg_c: 0,
            instr_pointer: 0,
            output: vec![],
            program: m.program.clone(),
        };
        m.run();

        m.output
    };

    while let Some(a_candidate) = possible_a_values.pop_first() {
        let res = check(a_candidate);
        if res == m.program {
            println!("Part2: {a_candidate}");
            return;
        }
        if res.len() > m.program.len() {
            continue;
        }
        let common_idx = m.program.len() - res.len();

        if res == &m.program[common_idx..] {
            possible_a_values.extend((0..8).map(|i| a_candidate * 8 + i));
        }
    }
}

struct Machine {
    reg_a: MachineWord,
    reg_b: MachineWord,
    reg_c: MachineWord,
    instr_pointer: usize,
    output: Vec<u8>,
    program: Vec<u8>,
}

impl Machine {
    fn adv(&mut self, op: Operand) {
        let two: MachineWord = 2;
        self.reg_a = self.reg_a / two.pow(op.get(self) as u32)
    }
    fn bxl(&mut self, op: u8) {
        self.reg_b = self.reg_b ^ (op as MachineWord);
    }
    fn bst(&mut self, op: Operand) {
        self.reg_b = op.get(self) % 8;
    }

    fn jnz(&mut self, op: u8) -> bool {
        if self.reg_a != 0 {
            self.instr_pointer = op as usize;
            true
        } else {
            false
        }
    }

    fn bxc(&mut self) {
        self.reg_b = self.reg_b ^ self.reg_c;
    }
    fn out(&mut self, op: Operand) {
        let res = op.get(self) % 8;
        self.output.push(res as u8);
    }
    fn bdv(&mut self, op: Operand) {
        self.reg_b = self.reg_a / (2u8 as MachineWord).pow(op.get(self) as u32)
    }
    fn cdv(&mut self, op: Operand) {
        self.reg_c = self.reg_a / (2u8 as MachineWord).pow(op.get(self) as u32)
    }

    fn exec_instr(&mut self, instr: u8, op: u8) {
        let combo_op = Operand::from_num(op);
        let mut increase_instruction_pointer = true;
        match instr {
            0 => self.adv(combo_op.unwrap()),
            1 => self.bxl(op),
            2 => self.bst(combo_op.unwrap()),
            3 => {
                if self.jnz(op) {
                    increase_instruction_pointer = false;
                }
            }
            4 => self.bxc(),
            5 => self.out(combo_op.unwrap()),
            6 => self.bdv(combo_op.unwrap()),
            7 => self.cdv(combo_op.unwrap()),
            _ => panic!("Unknown instruction"),
        }
        if increase_instruction_pointer {
            self.instr_pointer += 2;
        }
    }
    fn iter(&mut self) -> bool {
        self.exec_instr(
            self.program[self.instr_pointer],
            self.program[self.instr_pointer + 1],
        );
        return self.instr_pointer < self.program.len();
    }
    fn run(&mut self) {
        while self.iter() {}
    }
}

enum Operand {
    Literal(u8),
    RegA,
    RegB,
    RegC,
}

impl Operand {
    fn get(&self, m: &Machine) -> MachineWord {
        match self {
            Operand::Literal(v) => *v as _,
            Operand::RegA => m.reg_a,
            Operand::RegB => m.reg_b,
            Operand::RegC => m.reg_c,
        }
    }

    fn from_num(n: u8) -> Option<Self> {
        Some(match n {
            0 | 1 | 2 | 3 => Self::Literal(n),
            4 => Self::RegA,
            5 => Self::RegB,
            6 => Self::RegC,
            _ => return None,
        })
    }
}

#[test]
fn provided1() {
    let mut m = Machine {
        reg_a: 0,
        reg_b: 0,
        reg_c: 9,
        instr_pointer: 0,
        output: vec![],
        program: vec![2, 6],
    };
    m.run();
    assert_eq!(m.reg_b, 1);
}

#[test]
fn provided2() {
    let mut m = Machine {
        reg_a: 10,
        reg_b: 0,
        reg_c: 0,
        instr_pointer: 0,
        output: vec![],
        program: vec![5, 0, 5, 1, 5, 4],
    };
    m.run();
    assert_eq!(m.output, &[0, 1, 2]);
}

#[test]
fn provided3() {
    let mut m = Machine {
        reg_a: 2024,
        reg_b: 0,
        reg_c: 0,
        instr_pointer: 0,
        output: vec![],
        program: vec![0, 1, 5, 4, 3, 0],
    };
    m.run();
    assert_eq!(m.output, &[4, 2, 5, 6, 7, 7, 7, 7, 3, 1, 0]);
    assert_eq!(m.reg_a, 0);
}

#[test]
fn provided4() {
    let mut m = Machine {
        reg_a: 0,
        reg_b: 29,
        reg_c: 0,
        instr_pointer: 0,
        output: vec![],
        program: vec![1, 7],
    };
    m.run();
    assert_eq!(m.reg_b, 26);
}

#[test]
fn provided5() {
    let mut m = Machine {
        reg_a: 0,
        reg_b: 2024,
        reg_c: 43690,
        instr_pointer: 0,
        output: vec![],
        program: vec![4, 0],
    };
    m.run();
    assert_eq!(m.reg_b, 44354);
}

#[test]
fn provided_big() {
    let mut m = Machine {
        reg_a: 117440,
        reg_b: 0,
        reg_c: 0,
        instr_pointer: 0,
        output: vec![],
        program: vec![0, 3, 5, 4, 3, 0],
    };

    m.run();
    assert_eq!(m.output, m.program);
}
