use std::{
    collections::{BinaryHeap, HashMap},
    fs::read_to_string,
    time::Instant,
};

fn main() {
    let input = read_to_string("input17.txt").unwrap();
    // let input = read_to_string("sample17.txt").unwrap();
    let map: Vec<Vec<u8>> = input
        .lines()
        .map(|l| l.chars().map(|c| c.to_digit(10).unwrap() as u8).collect())
        .collect();

    dbg!(part1(&map));
}

type Cost = u32;

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
enum Dir {
    Right,
    Down,
    Left,
    Up,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
struct Pos {
    x: usize,
    y: usize,
    streak: usize,
    dir: Dir,
}

#[derive(Debug, Hash, PartialEq, Clone, Eq)]
struct Candidate {
    pos: Pos,
    cost: Cost,
}

impl Ord for Candidate {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.cost.cmp(&self.cost)
    }
}

impl PartialOrd for Candidate {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

fn part1(map: &Vec<Vec<u8>>) -> Cost {
    let mut candidates = BinaryHeap::new();
    // updated when visiting nodes from open set
    let mut pos_to_min_heat_lost = HashMap::<Pos, Cost>::new();
    let mut min_heat_lost = u32::MAX;
    // let mut min_heat_lost_path = None;

    let mut start = Instant::now();
    for dir in [Dir::Right, Dir::Down] {
        let pos = Pos {
            x: 0,
            y: 0,
            streak: 1,
            dir,
        };
        let candidate = Candidate { pos, cost: 0 };
        candidates.push(candidate);
        pos_to_min_heat_lost.insert(pos, 0);
    }

    let width = map[0].len();
    let height = map.len();

    while let Some(candidate) = candidates.pop() {
        let cpos = candidate.pos;
        if candidate.cost > pos_to_min_heat_lost[&cpos] {
            continue;
        }

        if start.elapsed().as_secs() > 0 {
            println!(
                "open set {} current ({},{}), current_cost: {} min_heat: {}",
                candidates.len(),
                cpos.x,
                cpos.y,
                candidate.cost,
                min_heat_lost
            );
            start = Instant::now();
        }

        let mut neighbors = Vec::with_capacity(4);
        // left
        if cpos.x > 0 && (cpos.streak < 3 || cpos.dir != Dir::Left) && cpos.dir != Dir::Right {
            neighbors.push(Pos {
                x: cpos.x - 1,
                y: cpos.y,
                streak: if cpos.dir == Dir::Left {
                    cpos.streak + 1
                } else {
                    1
                },
                dir: Dir::Left,
            });
        }
        // right
        if cpos.x < (width - 1)
            && (cpos.streak < 3 || cpos.dir != Dir::Right)
            && cpos.dir != Dir::Left
        {
            neighbors.push(Pos {
                x: cpos.x + 1,
                y: cpos.y,
                streak: if cpos.dir == Dir::Right {
                    cpos.streak + 1
                } else {
                    1
                },
                dir: Dir::Right,
            });
        }
        // down
        if cpos.y < (height - 1)
            && (cpos.streak < 3 || cpos.dir != Dir::Down)
            && cpos.dir != Dir::Up
        {
            neighbors.push(Pos {
                x: cpos.x,
                y: cpos.y + 1,
                streak: if cpos.dir == Dir::Down {
                    cpos.streak + 1
                } else {
                    1
                },
                dir: Dir::Down,
            });
        }
        // up
        if cpos.y > 0 && (cpos.streak < 3 || cpos.dir != Dir::Up) && cpos.dir != Dir::Down {
            neighbors.push(Pos {
                x: cpos.x,
                y: cpos.y - 1,
                streak: if cpos.dir == Dir::Up {
                    cpos.streak + 1
                } else {
                    1
                },
                dir: Dir::Up,
            });
        }
        for n in neighbors {
            let neighbor_heat_lost = pos_to_min_heat_lost[&cpos] + map[n.y][n.x] as u32;

            if !pos_to_min_heat_lost
                .get(&n)
                .is_some_and(|p| *p <= neighbor_heat_lost)
            {
                let new_candidate = Candidate {
                    pos: n,
                    cost: neighbor_heat_lost,
                };
                candidates.push(new_candidate);
            }

            let cost = pos_to_min_heat_lost.entry(n).or_insert_with(||neighbor_heat_lost);
            *cost = u32::min(*cost, neighbor_heat_lost);

            if n.x == width - 1 && n.y == height - 1 {
                if neighbor_heat_lost < min_heat_lost {
                    min_heat_lost = neighbor_heat_lost;
                    continue;
                    // min_heat_lost_path = Some(path);
                }
            }
        }
    }

    min_heat_lost
}
