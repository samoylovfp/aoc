use std::fs::read_to_string;

fn main() {
    let input = read_to_string("input1.txt").unwrap();
    let num_iterator = input.chars().map(|c| match c {
        '(' => 1,
        ')' => -1,
        _ => panic!(),
    });
    println!("First part is {}", num_iterator.clone().sum::<i32>());

    println!(
        "Second part is {:?}",
        num_iterator
            .enumerate()
            .scan(0, |sum, (i, n)| {
                *sum += n;
                Some((*sum, i))
            })
            .find(|(sum, _i)| *sum < 0)
            .unwrap()
            .1
            + 1
    )
}
