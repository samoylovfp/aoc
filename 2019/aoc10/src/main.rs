use std::collections::HashSet;
use std::{cell::Cell, fs::read_to_string};

use fehler::throws;

type Point = (isize, isize);

/// (quadrant, angle, distance)
const ANGLE_PREC: isize = 10000;

fn gcd(a: usize, b: usize) -> usize {
    let min_num = a.min(b);
    if min_num == 0 {
        return a.max(b);
    }
    for i in (1..=min_num).rev() {
        if (a % i == 0) && (b % i == 0) {
            return i;
        }
    }
    return 1;
}

fn iter_points_between(
    start: impl Into<Point>,
    end: impl Into<Point>,
) -> impl Iterator<Item = Point> {
    let start = start.into();
    let end = end.into();
    let dx = end.0 as isize - start.0 as isize;
    let dy = end.1 as isize - start.1 as isize;
    let points_num = gcd(dx.abs() as usize, dy.abs() as usize) as isize;
    (1..points_num).map(move |i| {
        (
            (start.0 as isize + dx * i / points_num),
            (start.1 as isize + dy * i / points_num),
        )
    })
}


struct Map {
    asteroids: HashSet<Point>,
    station: Cell<Option<Point>>
}

impl Map {

    fn from_str(d: &str) -> Map {
        let mut asteroids = HashSet::new();
        for (y, line) in d.trim().lines().enumerate() {
            for (x, char) in line.trim().chars().enumerate() {
                if char == '#' {
                    asteroids.insert((x as isize, y as isize));
                }
            }
        }
        Map {
            asteroids,
            station: Cell::new(None)
        }
    }

    fn has_asteroid(&self, p: Point) -> bool {
        self.asteroids.contains(&p)
    }

    fn get_visible_from(&self, from: Point) -> HashSet<Point> {
        self.asteroids
            .iter()
            .cloned()
            .filter(|&point| {
                point != from && iter_points_between(from, point).all(|p| !self.has_asteroid(p))
            })
            .collect()
    }

    fn total_visible_from(&self, p: Point) -> usize {
        self.get_visible_from(p).len()
    }

    fn get_station(&self) -> Option<Point> {
        if let None = self.station.get() {
            self.station.set(self.asteroids
            .iter()
            .max_by_key(|&&p| self.total_visible_from(p))
            .cloned())
        }
        self.station.get()
    }
    /// (order, quadrant, ratio, distance)
    fn order(&self, p1: Point, p2: Point) -> (u8, u8, usize) {
        let dx = p2.0 - p1.0;
        let dy = p2.1 - p1.1;
        let quadrant;
        match (dx >= 0, dy >= 0) {
            (true, false) => {
                quadrant = 1;
            }
            (true, true) => {
                quadrant = 2;
            },
            (false, true) => {
                quadrant=3;
            },
            (false, false) => {
                quadrant=4;
            }
        };
        let mut base = dy.abs() * ANGLE_PREC;
        if base == 0 {
            base = 1
        }
        let mut ratio = dx.abs() * ANGLE_PREC * ANGLE_PREC / base;
        let order = iter_points_between(p1, p2).map(
            |p|
            if self.has_asteroid(p) {1} else {0}
        ).sum();
        if quadrant == 2 || quadrant == 4 {
            ratio = -ratio;
        }
        return (order, quadrant, ratio as usize);
    }

    fn get_firing_plan(&self, from: Point) -> Vec<Point> {
        let mut clockwise_asteroids = self.asteroids.iter().cloned()
        .collect::<Vec<_>>();
        clockwise_asteroids.sort_by_key(|&c| self.order(from ,c));
        for c in &clockwise_asteroids {
            println!("{:?} polar is {:?}", c, self.order(from, *c));
        }
        clockwise_asteroids
    }
}

#[throws(std::io::Error)]
fn main() {
    let d = read_to_string("input.txt").expect("need an input.txt file");
    let field = Map::from_str(&d);
    let station = field.get_station().expect("No asteroids?");
    println!("First part is {:?}", field.total_visible_from(station));
    let firing_plan = field.get_firing_plan(station);
    let a200 = firing_plan[200];
    println!("Second part is {}", a200.0 * 100 + a200.1);

}

#[cfg(test)]
mod tests {
    use super::*;

    fn iter_equal(start: (isize, isize), end: (isize, isize), expect: Vec<(isize, isize)>) {
        assert_eq!(
            iter_points_between(start, end).collect::<Vec<Point>>(),
            expect
        )
    }

    #[test]
    fn test_max_visible() {
        let m = Map::from_str(
            r#"
            .#..#
            .....
            #####
            ....#
            ...##
        "#,
        );

        assert_eq!(
            m.get_visible_from((2, 2)),
            vec![(1, 2), (1, 0), (4, 0), (3, 2), (4, 3), (4, 4), (3, 4)]
                .into_iter()
                .map(|t| t.into())
                .collect()
        );
        assert_eq!(m.total_visible_from((2, 2)), 7);
    }

    #[test]
    fn test_provided1() {
        let m = Map::from_str(
            r#"
            .#..#
            .....
            #####
            ....#
            ...##
            "#
        );
        let s = m.get_station();

        assert_eq!(
            m.total_visible_from(s.unwrap()),
            8
        );
    }

    #[test]
    fn test_between_diag() {
        iter_equal((4, 4), (1, 1), vec![(3, 3), (2, 2)])
    }

    #[test]
    fn test_between_horizontal() {
        iter_equal((5, 0), (0, 0), vec![(4, 0), (3, 0), (2, 0), (1, 0)])
    }

    #[test]
    fn test_between_vertical() {
        iter_equal((0, 5), (0, 0), vec![(0, 4), (0, 3), (0, 2), (0, 1)])
    }

    #[test]
    fn test_gcd() {
        assert_eq!(gcd(0, 6), 6);
        assert_eq!(gcd(6, 10), 2);
        assert_eq!(gcd(4, 8), 4);
    }

    #[test]
    fn test_clockwise() {
        let m = Map::from_str(&r#"
            .#....###24...#..
            ##...##.13#67..9#
            ##...#...5.8####.
            ..#.....X...###..
            ..#.#.....#....##
        "#.replace(|c| ('1'..='9').contains(&c), "#"));
        let mut sorted_asteroids = m.asteroids.iter().cloned().collect::<Vec<_>>();
        sorted_asteroids.sort();
        for a in sorted_asteroids {
            println!("Asteroid {:?}", a);
        }
        let f = m.get_firing_plan((8, 3));
        assert_eq!(&f[..9], &[
            (8, 1), (9, 0), (9, 1),
            (10, 0), (9, 2), (11, 1),
            (12, 1), (11, 2), (15 ,1),
        ]);
        assert_eq!(&f[9..18], &[
            (12, 2), (13, 2), (14, 2),
            (15 ,2), (12, 3), (16, 4),
            (15, 4), (10, 4), (4, 4),
        ]);
        assert_eq!(&f[18..27], &[
            (2, 4), (2, 3), (0, 2),
            (1, 2), (0, 1), (1, 1),
            (5, 2), (1, 0), (5, 1),
        ]);
        assert_eq!(&f[27..36], &[
            (6, 1), (6, 0), (7, 0),
            (8, 0), (10, 1), (14, 0),
            (16, 1), (13, 3), (14, 3)
        ]);
    }
}
